<style>
    .form_control{
        width: 80%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
    }
</style>

<body class="skin-blue sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

        <?= show_notification(); ?>
        <div class="content-wrapper" style="min-height: 1068px;">

            <section class="content">
                <div class="row">
                    <div class="col-md-12">

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">
                                    <i class="fa fa-fw fa-plus-circle"></i>Student Registration
                                </h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <form role="form" method="post" action="">
                                    <table class="table">
                                        <tr>
                                            <td style="width: 20%">First Name</td>
                                            <td><input required type="text" name="first_name" id="first_name" class="form_control" title="First name" /></td>
                                        </tr>
                                        <tr>
                                            <td>Last Name</td>
                                            <td><input required type="text" name="last_name" id="last_name" class="form_control" title="Last name" /></td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td><input required type="tel" name="phone" id="phone" class="form_control" /></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><input required type="email" name="email" id="email" class="form_control" title="Email" /></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><input type="text" name="address" id="address" class="form_control" title="Address" /></td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td><input type="text" name="dateof_birth" id="dateof_birth" class="form_control" style="cursor: pointer" /></td>
                                        </tr>
                                        <tr>
                                            <td>Education fair</td>
                                            <td>
                                                <select required name="education_fair" id="education_fair" class="form_control">
                                                    <option value="" selected>Select fair</option>
                                                    <option value="Abuja, Nigeria">Abuja, Nigeria</option>
                                                    <option value="Lagos, Nigeria">Lagos, Nigeria</option>
                                                    <option value="Edo, Nigeria">Edo, Nigeria</option>
                                                    <option value="Nairobi, Kenya">Nairobi, Kenya</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Do you have a Travel Passport</td>
                                            <td>
                                                <input type="radio" name="passport" value="1"/> Yes
                                                <input type="radio" name="passport" value="0" checked/> No
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Education Interest</td>
                                            <td>
                                                <select required name="interest" id="interest" class="form_control">
                                                    <option value="" selected>Select Interest</option>
                                                    <option value="Master">Master</option>
                                                    <option value="Undergraduate">Undergraduate</option>
                                                    <option value="Diploma & Advance Diploma">Diploma &amp; Advance Diploma </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>English language test</td>
                                            <td>
                                                <select required name="english_test" id="english_test" class="form_control">
                                                    <option value="None" selected>None</option>
                                                    <option value="IELTS">IELTS</option>
                                                    <option value="TOEF iBT">TOEF iBT</option>
                                                    <option value="PTE">PTE</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Highest Qualification</td>
                                            <td>
                                                <select required name="highest_qual" id="highest_qual" class="form_control">
                                                    <option value="" selected>Select</option>
                                                    <option value="Master">Master</option>
                                                    <option value="Bachelor">Bachelor</option>
                                                    <option value="Diploma/Advance Diploma">Diploma/Advance Diploma</option>
                                                    <option value="SSCE">SSCE</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Financial Capacity – Tuition per Semester</td>
                                            <td>
                                                <select required name="financial_capacity" id="financial_capacity" class="form_control">
                                                    <option value="" selected>Select</option>
                                                    <option value="$3000">$3,000</option>
                                                    <option value="$7000">$7,000</option>
                                                    <option value="$14000">$14,000</option>
                                                    <option value="$18000">$18,000</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Course Choice</td>
                                            <td>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>Course 1</td>
                                                        <td>
                                                            <input type="text" name="course_choice_1" id="course_choice_1" class="form_control" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Course 2</td>
                                                        <td>
                                                            <input type="text" name="course_choice_2" id="course_choice_2" class="form_control" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Course 3</td>
                                                        <td>
                                                            <input type="text" name="course_choice_3" id="course_choice_3" class="form_control" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Names of other persons/guardian attending the Fair with you</td> 
                                            <td>
                                                <input type="text" name="attendee" id="attendee" class="form_control" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <button type="reset" class="btn btn-warning btn-flat">Reset</button>
                                                <button class="btn btn-primary btn-flat" type="submit">Register</button>

                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
</body>

<!-- datepicker -->
<script src="/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script>

    $(function () {
        $("#dateof_birth").datepicker();
    });
</script>

