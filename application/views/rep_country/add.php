<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Add Representing Country
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Representing Countries</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row" ng-app="country" ng-controller="addCtrl">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form role="form" method="post" action="">
                        <fieldset>
                            <legend>Country</legend>
                            <table class="table">
                                <tr>
                                    <td>
                                        Country Name
                                    </td>
                                    <td>
                                        <select class="form-control" id="country_id" name="country_id">
                                            <option value="">select Country</option>
                                            <?php
                                            if (!empty($countries)):
                                                $sel = '';
                                                foreach ($countries as $country):
                                                    ?>
                                                    <option value="<?= $country->country_id ?>" <?= $sel ?>><?= $country->country ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>

                        <fieldset>
                            <legend>Add Application Process</legend>
                            <table class="table">
                                <tr ng-repeat="status in data.statuses">
                                    <td>Step {{$index + 1}}</td>
                                    <td>
                                        <input type="text" class="form-control" name="status[{{$index}}]" value="">
                                    </td>
                                    <td>
                                        <a href="#" title="Remove this step" onclick="return false;" ng-click="removeItem($index)">
                                            <i class="fa fa-fw fa-trash-o"></i> remove
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <a title="Click here to add" class="btn btn-warning btn-xs pull-right" onclick="return false;" ng-click="addItem()">
                                            <i class="fa fa-plus-circle"></i>
                                            Click here to add
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <table class="table">
                            <tr>
                                <td>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="reset" class="btn btn-warning">Reset Form</button>
                                </td
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>


    var countryApp = angular.module('country', []);

    countryApp.controller('addCtrl', function ($scope) {

        $scope.data = {statuses: []};

        $scope.blankResult = {status: ''};

        $scope.addItem = function () {
            $scope.data.statuses.push(angular.copy($scope.blankResult));
        };

        $scope.getSittingNo = function () {
            return $scope.data.sittings.length;
        };

        $scope.removeItem = function (idx) {
            if ($scope.data.statuses.length > 0) {
                $scope.data.statuses.splice(idx, 1);
            }
            $scope.check();
        };

        $scope.check = function () {
            if ($scope.data.statuses.length == 0) {
                $scope.addItem();
            }
        };

        $scope.check();



    });

</script>