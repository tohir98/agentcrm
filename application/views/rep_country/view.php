<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Representing Countries <span class="badge badge-success"><?= count($countries) ?></span>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Representing Countries</a></li>
        <li class="active">View</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php if ($this->user_auth_lib->have_perm('rep_countries:add')): ?>
                            <a class="btn btn-block btn-primary pull-right" href="<?= site_url('rep_countries/add'); ?>">
                                Add Representing Country
                            </a>
                        <?php endif; ?>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($countries)):
                        foreach ($countries as $rc):
                            ?>
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?= $rc['country'] ?> | Added on <?= date('d-M-Y', strtotime($rc['created_at'])) ?></h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                    </div><!-- /.box-tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <?php
                                    if (!empty($rc['statuses'])):
                                        $cnt = 0;
                                        ?>
                                        <table class="table table-condensed table-striped table-bordered">
                                            <tr>
                                                <th>S/N</th>
                                                <th>Steps</th>
                                                <th>Status Name</th>
                                                <th>Add/Edit Auto Email</th>
                                                <th>Action</th>
                                            </tr>
                                            <?php 
//                                                                                    var_dump($rc['statuses']);
                                            foreach ($rc['statuses'] as $status): ?>
                                                <tr>
                                                    <td><?= ++$cnt; ?></td>
                                                    <td><?= 'Step ' . $cnt; ?></td>
                                                    <td><?= $status['status_name'] ?></td>
                                                    <td>
                                                        <h5>
                                                            <i class="fa fa-fw fa-code"></i>
                                                        </h5>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" <?php if ($status['status']): ?> checked <?php endif; ?> class="input-block-level bswitch" data-enabled="<?= intval($status['status']) ?>" data-id="<?= $status['id']?>">
                                                                     
                                                    </td>
                                                </tr>
                                            <?php endforeach;
                                            ?>
                                        </table>
                                    <?php endif;
                                    ?>


                                </div><!-- /.box-body -->
                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal" id="modal-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Status Change</h4>
            </div>
            <div class="modal-body">
                <p id="statusMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" class="btn btn-primary closeme">Ok</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    $(".bswitch").bootstrapSwitch();
    $('.bswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        var id_ = $(this).data('id');

        var url_ = '<?php echo site_url('rep_countries/switchStatus'); ?>';
        $.post(
                url_,
                {
                    'state': state,
                    'id': id_,
                },
                function (res) {
                    if (res === 'OK') {
                        toastr["success"]("Operation was successful");
                    } else {
                        toastr["error"]("Sorry, Unable complete your request.");
                    }
                }
        );

        return false;

    });

    $('body').delegate('.tagToggle', 'click', function (evt) {
        evt.preventDefault();
        var status_ = parseInt($(this).data('enabled')) > 0 ? 'disable' : 'enable';
        $('#statusMsg').text('Are you sure you want to ' + status_ + ' this step?');
        $('#modal-status').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
    });
</script>
