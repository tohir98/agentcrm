<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Add Agent
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Agents</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
               
                <div class="box-body">
                    <form method="post">
                        <?php include '_form.php'; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>