<?php
//var_dump($agent_details);
$first_name = $agent_details->first_name;
$last_name = $agent_details->last_name;
$phone = $agent_details->phone;
$download_csv = (int) $agent_details->download_csv;
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Agent</h4>
        </div>
        <form name="frmEditAgent" method="post" action="<?= site_url('setup/agent/edit/' . $agent_details->agent_id) ?>">
            <div class="modal-body">
<?php include '_agent_detail_form.php'; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->