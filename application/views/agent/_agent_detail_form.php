<table class="table">
    <tr>
        <td>
            First Name
        </td>
        <td>
            <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?= isset($first_name) ? $first_name : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Last Name
        </td>
        <td>
            <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?= isset($last_name) ? $last_name : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Phone
        </td>
        <td>
            <input required type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?= isset($phone) ? $phone : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Download CSV
        </td>
        <td>
            <select class="form-control" name="download_csv" id="download_csv">
                <option value="1" <?php if (isset($download_csv)): ?> <?= $download_csv == 1 ? 'selected' : '' ?> <?php endif; ?>>Yes</option>
                <option value="0" <?php if (isset($download_csv)): ?> <?= $download_csv == 0 ? 'selected' : '' ?> <?php endif; ?>>No</option>
            </select>
        </td>
    </tr>
</table>