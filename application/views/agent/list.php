<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Agents
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Agents</a></li>
        <li class="active">List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <?php if ($this->user_auth_lib->have_perm('setup:add_agent')): ?>
                        <h3 class="box-title">
                            <a class="btn btn-block btn-primary pull-right" href="<?= site_url('setup/agent/add_agent'); ?>">
                                Add Agent
                            </a>
                        </h3>
                    <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($agents)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Agent</th>
                                    <th>Contact Details.</th>
                                    <th>Manager</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($agents as $agent): ?>
                                    <tr>
                                        <td><?= ucfirst($agent->first_name) . ' ' . ucfirst($agent->last_name) ?></td>
                                        <td> 
                                            <i class="fa fa-fw fa-envelope"></i><?= $agent->c_email ?> <br>
                                            <i class="fa fa-fw fa-phone"></i><?= $agent->phone ?> 
                                        </td>
                                        <td><?= ucfirst($agent->c_first_name) . ' ' . ucfirst($agent->c_last_name) ?></td>

                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('setup/agent/edit/' . $agent->agent_id) ?>" onclick="return false;" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('setup/agent/delete/' . $agent->agent_id) ?>" onclick="return false;" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else:
                        $msg = "No agent has been created. <a href=" . site_url('/setup/agent/add_agent'). ">Click here to create one.</a>";
                        echo show_no_data($msg);
                        endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<div class="modal" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete Agent</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Agent?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="#" class="btn btn-primary closeme">Yes, delete</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal" id="modal_edit_agent">
</div>

<script>
    $('body').delegate('.delete', 'click', function (evt) {
        console.log('deleting..');
        evt.preventDefault();
        $('#modal-delete').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_agent').modal('show');
        $('#modal_edit_agent').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_agent').html('');
            $('#modal_edit_agent').html(html);
            $('#modal_edit_agent').modal('show').fadeIn();
        });
        return false;
    });
</script>