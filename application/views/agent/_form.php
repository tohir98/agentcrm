<fieldset>
    <legend>Agent Information</legend>
    <?php include '_agent_detail_form.php'; ?>
</fieldset>


<fieldset>
    <legend>Login Information</legend>
    <table class="table">
        <tr>
            <td>
                Email
            </td>
            <td>
                <input required type="text" class="form-control" id="email" name="email" placeholder="Email" value="">
            </td>
        </tr>
    </table>
</fieldset>


<table class="table">
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-warning">Reset Form</button>
        </td>
    </tr>
</table>