<?= show_notification(); ?>
<section class="content-header">
    <?php if (isset($branch_details)) : ?>
        <a href="<?= site_url('brach_office/view') ?>" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-chevron-left"></i> Back</a> <br><br>
    <?php endif; ?>
    <h1>
        <?= $page_title; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Branch Office</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <form method="post" <?php if (isset($branch_details)) : ?> action="<?= site_url('brach_office/edit/' . $branch_details->branch_id)?>" <?php endif; ?>>
                        <?php include '_branch_form.php'; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>