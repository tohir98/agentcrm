<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Branch Offices
        <?php
        if (count($branches) > 0):
            ?>
            <span class="badge"><?= count($branches); ?></span>
        <?php endif; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Branch Office</a></li>
        <li class="active">View</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <?php if ($this->user_auth_lib->have_perm('brach_office:add')): ?>
                        <h3 class="box-title">
                            <a class="btn btn-block btn-primary pull-right" href="<?= site_url('brach_office/add'); ?>">
                                <i class="fa fa-fw fa-plus-circle"></i>New Branch Office
                            </a>
                        </h3>
                    <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($branches)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Branch</th>
                                    <th>Contact Person</th>
                                    <!--<th>Date Added</th>-->
                                    <th>Last Login</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($branches as $branch): ?>
                                    <tr>
                                        <td>
                                            <b><?= $branch->branch_name ?></b><br>
                                            <?= $branch->address ?><br>
                                            <?= $branch->city ?> | <?= $branch->state ?><br>
                                            <?= $branch->email ?> | <i class="fa fa-fw fa-phone"></i> <?= $branch->phone ?><br>

                                        </td>
                                        <td>
                                            <?= ucfirst($branch->contact_person) ?> <label class="label label-success"><?= $branch->designation ?></label><br>
                                            <i class="fa fa-fw fa-phone"></i> <?= $branch->contact_phone ?><br>
                                            <i class="fa fa-fw fa-skype"></i> <?= $branch->skype_id ?>
                                        </td>
                                        <!--<td><?= date('d-M-Y', strtotime($branch->date_created)); ?></td>-->
                                        <td>NA</td>
                                        <td> 
                                            <span class="label label-<?= $branch->c_status > 0 ? 'success' : 'warning' ?>">
                                                <?= $statuses[$branch->c_status]; ?>
                                            </span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('brach_office/viewBranch/'. $branch->branch_id) ?>">View</a></li>
                                                    <li><a href="<?= site_url('brach_office/edit/'. $branch->branch_id)?>">Edit</a></li>
                                                    <li>
                                                        <a href="<?= site_url('brach_office/delete/' . $branch->branch_id) ?>" onclick="return false;" class="delete">Delete</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php else:
                        
                        $msg = "No branch office has been created. <a href=" . site_url('brach_office/add'). ">Click here to create one.</a>";
                        echo show_no_data($msg);
                        endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<div class="modal" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Delete Branch Office</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Branch Office?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
                <a href="#" class="btn btn-primary closeme">Yes, delete</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal" id="modal_edit_branch_office">
</div>

<script>
    $('body').delegate('.delete', 'click', function (evt) {
        console.log('deleting..');
        evt.preventDefault();
        $('#modal-delete').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
    });

    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_agent').modal('show');
        $('#modal_edit_agent').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_agent').html('');
            $('#modal_edit_agent').html(html);
            $('#modal_edit_agent').modal('show').fadeIn();
        });
        return false;
    });
</script>