<table class="table">
    <tr>
        <td>
            Branch Name
        </td>
        <td>
            <input required type="text" class="form-control" id="branch_name" name="branch_name" placeholder="Enter Branch Name" value="<?= isset($branch_details->branch_name) ? $branch_details->branch_name : '' ?>">
        </td>
        <td>
            Address
        </td>
        <td>
            <input required type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?= isset($branch_details->address) ? $branch_details->address : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            City
        </td>
        <td>
            <input required type="text" class="form-control" id="city" name="city" placeholder="City" value="<?= isset($branch_details->city) ? $branch_details->city : '' ?>">
        </td>
        <td>
            State
        </td>
        <td>
            <input type="text" class="form-control" id="state" name="state" placeholder="State" value="<?= isset($branch_details->state) ? $branch_details->state : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Country
        </td>
        <td>
            <select required class="form-control" id="country_id" name="country_id">
                <option value="">select Country</option>
                <?php
                if (!empty($countries)):
                    $sel = '';
                    foreach ($countries as $country):
                        if ($branch_details->country_id == $country->country_id):
                            $sel = 'selected';
                        else:
                            $sel = '';
                        endif;
                        ?>
                        <option value="<?= $country->country_id ?>" <?= $sel ?>><?= trim($country->country) ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </td>
        <td>
            Email
        </td>
        <td>
            <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= isset($branch_details->email) ? $branch_details->email : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Phone
        </td>
        <td>
            <input required type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?= isset($branch_details->phone) ? $branch_details->phone : '' ?>">
        </td>
        <td>
            Website
        </td>
        <td>
            <input type="text" class="form-control" id="website" name="website" placeholder="Website" value="<?= isset($branch_details->website) ? $branch_details->website : '' ?>">
        </td>
    </tr>
</table>

<fieldset>
    <legend>Point of Contact</legend>
    <table class="table">
        <tr>
            <td>
                First Name
            </td>
            <td>
                <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?= isset($branch_details->contact_person) ? explode(" ", $branch_details->contact_person)[0] : '' ?>">

            </td>
            <td>
                Last Name
            </td>
            <td>
                <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?= isset($branch_details->contact_person) ? explode(" ", $branch_details->contact_person)[1] : '' ?>">
            </td>
        </tr>
        <tr>
            <td>
                Contact Phone
            </td>
            <td>
                <input required type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="Contact Phone" value="<?= isset($branch_details->contact_phone) ? $branch_details->contact_phone : '' ?>">
            </td>
            <td>
                Skype ID
            </td>
            <td>
                <input type="text" class="form-control" id="skype_id" name="skype_id" placeholder="Skype ID" value="<?= isset($branch_details->skype_id) ? $branch_details->skype_id : '' ?>">
            </td>
        </tr>
        <tr>
            <td>
                Designation
            </td>
            <td>
                <input type="text" class="form-control" id="designation" name="designation" placeholder="Designation">
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</fieldset>
<?php if ($mode !== 'edit'): ?>
<fieldset>
    <legend>Login Details</legend>
    <table class="table">
        <tr>
            <td>Email Address (Username)</td>
            <td>
                <input type="email" class="form-control" id="username" name="username" value="">
            </td>
            <td>Password</td>
            <td>
                <input type="password" class="form-control" id="password" name="password" value="">
            </td>
        </tr>
    </table>
</fieldset>
<?php endif; ?>

<?php if ($mode !== 'view'): ?>
    <table class="table">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <button type="submit" class="btn btn-primary"><?= isset($branch_details) ? 'Update' : 'Save' ?></button>
                <button type="reset" class="btn btn-warning">Reset Form</button>
            </td
        </tr>
    <?php endif; ?>
</table>