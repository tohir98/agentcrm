<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Tuition Fees
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Tuition Fees</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <?php if ($this->user_auth_lib->have_perm('setup:add_agent')): ?>
                        <h3 class="box-title">
                            <a class="btn btn-block btn-primary pull-right" href="#add_tuition" data-toggle="modal">
                                Add Tuition Fee
                            </a>
                        </h3>
                    <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($fees)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Fee</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sn = 0;
                                foreach ($fees as $fee): ?>
                                    <tr>
                                        <td><?= ++$sn; ?></td>
                                        <td><?= ucfirst($fee->fee) ?></td>
                                        <td><?= ucfirst($fee->description) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('f_setup/edit_tution_fee/' . $fee->tution_fee_id) ?> " class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('f_setup/delete_tution_fee/' . $fee->tution_fee_id) ?>" onclick="return false;" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
    <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No fee has been added. <a href=#add_tuition data-toggle=modal>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<?php include 'overlay/_add_tuition.php'; ?>
<div class="modal" id="modal_edit_tuition_fees">
</div>


<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this fee ?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    
    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_tuition_fees').modal('show');
        $('#modal_edit_tuition_fees').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_tuition_fees').html('');
            $('#modal_edit_tuition_fees').html(html);
            $('#modal_edit_tuition_fees').modal('show');
        });
        return false;
    });
</script>