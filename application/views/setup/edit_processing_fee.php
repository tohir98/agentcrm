<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Fee</h4>
        </div>
        <form role="form" method="post" action="<?= site_url('/f_setup/processing_fee/'. $fee_details->processing_fee_id)?>">
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <td>
                            Fee
                        </td>
                        <td>
                            <input required type="text" class="form-control" id="fee_name" name="fee_name" placeholder="Fee Name" value="<?= $fee_details->fee_name; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description
                        </td>
                        <td>
                            <input type="text" class="form-control" id="description" name="description" value="<?= $fee_details->description; ?>" placeholder="Description">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Amount
                        </td>
                        <td>
                            <input required type="text" class="form-control" id="amount" name="amount" value="<?= $fee_details->amount; ?>" placeholder="Amount" >
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->