<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Commission Payables
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Commission Payables</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <?php if ($this->user_auth_lib->have_perm('setup:add_agent')): ?>
                        <h3 class="box-title">
                            <a class="btn btn-block btn-primary pull-right" href="#add_comm" data-toggle="modal">
                                Add Commission
                            </a>
                        </h3>
                    <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($commission_payable)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Commission</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sn = 0;
                                foreach ($commission_payable as $commission):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn; ?></td>
                                        <td><?= ucfirst($commission->commission_payable) ?></td>
                                        <td><?= ucfirst($commission->description) ?></td>
                                        <td><?= number_format($commission->amount, 2) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('/f_setup/edit_comm_payable/'.$commission->commission_payable_id) ?>" class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('f_setup/delete_comm_payables/' . $commission->commission_payable_id) ?>" onclick="return false;" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
    <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No payable commission has been added. <a href=#add_comm data-toggle=modal>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<?php include 'overlay/_add_comm.php'; ?>

<div class="modal" id="modal_edit_comm_payable">
</div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this commission ?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    
      $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_comm_payable').modal('show');
        $('#modal_edit_comm_payable').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_comm_payable').html('');
            $('#modal_edit_comm_payable').html(html);
            $('#modal_edit_comm_payable').modal('show');
        });
        return false;
    });
</script>