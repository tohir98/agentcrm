<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Edit Tution</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/f_setup/edit_tution_fee/'.$tution_details->tution_fee_id) ?>">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                Fee
                            </td>
                            <td>
                                <input required type="text" class="form-control" id="fee" name="fee" placeholder="Fee" value="<?= $tution_details->fee ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                            <td>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?= $tution_details->description ?>">
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->