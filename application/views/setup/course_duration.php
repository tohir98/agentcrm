<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Course Duration
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Course Duration</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a class="btn btn-block btn-primary pull-right" href="#add_course_duration" data-toggle="modal">
                            Add Course Duration
                        </a>
                    </h3>
                </div>
                <div class="box-body">
                    <?php
                    if (!empty($course_durations)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead> 
                                <tr>
                                    <th>SN</th>
                                    <th>Course Duration</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sn = 0;
                                foreach ($course_durations as $course_duration):
                                    ?>
                                    <tr>
                                        <td><?= ++$sn; ?></td>
                                        <td><?= ucfirst($course_duration->course_duration) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('f_setup/edit_course_duration/' . $course_duration->course_duration_id) ?> " class="edit">Edit</a></li>
                                                    <li><a href="<?= site_url('f_setup/delete_course_duration/' . $course_duration->course_duration_id) ?>" onclick="return false;" class="delete">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
    <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    else:
                        $msg = "No course duration has been added. <a href=#add_course_duration data-toggle=modal>Click here to add one.</a>";
                        echo show_no_data($msg);
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'overlay/_add_course_duration.php'; ?>
<div class="modal" id="modal_edit_course_duration">
</div>

<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = 'Are you sure you want to delete this course duration ?';
            OaaStudy.doConfirm({
                title: 'Confirm Delete',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
    
    $('body').delegate('.edit', 'click', function (evt) {
        evt.preventDefault();

        $('#modal_edit_course_duration').modal('show');
        $('#modal_edit_course_duration').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#modal_edit_course_duration').html('');
            $('#modal_edit_course_duration').html(html);
            $('#modal_edit_course_duration').modal('show');
        });
        return false;
    });

</script>