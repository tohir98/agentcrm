<div class="modal" id="add_tuition">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Tuition</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                Fee
                            </td>
                            <td>
                                <input required type="text" class="form-control" id="fee" name="fee" placeholder="Fee" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                            <td>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>