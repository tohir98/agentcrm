<div class="modal" id="add_fee">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Fee</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                Fee
                            </td>
                            <td>
                                <input required type="text" class="form-control" id="fee_name" name="fee_name" placeholder="Fee Name" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description
                            </td>
                            <td>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Amount
                            </td>
                            <td>
                                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" >
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>