
<div class="modal-body">
    <table class="table">
        <tr>
            <td>
                Commission
            </td>
            <td>
                <input required type="text" class="form-control" id="commission" name="commission" placeholder="Commission" value="<?php if (isset($type)) { echo $type == 'payable' ? $comm_details->commission_payable : $comm_details->commission_receivable ;} ?>">
            </td>
        </tr>
        <tr>
            <td>
                Description
            </td>
            <td>
                <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php if (isset($comm_details->description)) { echo $comm_details->description ;} ?>">
            </td>
        </tr>
        <tr>
            <td>
                Amount
            </td>
            <td>
                <input required type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?php if (isset($comm_details->amount)) { echo $comm_details->amount ;} ?>">
            </td>
        </tr>
    </table>
</div>