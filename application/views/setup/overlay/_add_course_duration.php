<div class="modal" id="add_course_duration">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Course Duration</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <td>
                                Course Duration
                            </td>
                            <td>
                                <input required type="text" class="form-control" id="course_duration" name="course_duration" placeholder="Course Duration" value="">
                            </td>
                        </tr>                        
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>