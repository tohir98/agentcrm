<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Representing Institutions <span class="badge badge-success"><?= count($institutions) ?></span>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Representing Institutions</a></li>
        <li class="active">View</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a class="btn btn-block btn-primary pull-right" href="<?= site_url('rep_institutions/add'); ?>">
                            <i class="fa fa-fw fa-plus-circle"></i>Add Representing Institution
                        </a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($institutions)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Institution</th>
                                    <th>Contact Person</th>
                                    <th>Website</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($institutions as $institution): ?>
                                    <tr>
                                        <td style="width: 70px">
                                            <img src="<?= $institution->logo ? "/files/school_logo/" . $institution->logo : '/img/logo_def.jpg' ?>" style="width: 60px; height: 60px" />
                                        </td>
                                        <td>
                                            <b><?= $institution->institution_name ?></b><br>
                                            <?= $institution->campus ?><br>
                                            <?= $institution->country ?>
                                        </td>
                                        <td>
                                            <?= ucfirst($institution->intl_contact_person) ?> <br>
                                            <i class="fa fa-fw fa-phone"></i> <?= $institution->phone ?><br>
                                            <i class="fa fa-fw fa-envelope"></i> <?= $institution->email ?>
                                        </td>
                                        <td><?= $institution->website ?></td>
                                        <td> 
                                            <span class="label label-<?= $institution->status ? 'success' : 'warning' ?>">
                                                <?= $institution->status ? 'Active' : 'Inactive' ?>
                                            </span>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a class="statusToggle" data-enabled="<?= intval($institution->status) ?>" href="<?= site_url('/rep_institutions/toggleStatus/' . $institution->rep_institution_id); ?>">
                                                            <?= $institution->status ? 'Disable' : 'Enable' ?>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= site_url('/rep_institutions/edit/' . $institution->rep_institution_id); ?>">
                                                            Edit
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="<?= site_url('/rep_institutions/viewInstitution/' . $institution->rep_institution_id); ?>">
                                                            View
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(function () {
        $('.statusToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this Institution, applicants \'ll no longer be able to apply to it, Are you sure you want to continue? ' : 'Are you sure you want to enable selected institution ?';
            OaaStudy.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>