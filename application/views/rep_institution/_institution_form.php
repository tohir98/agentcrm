<table class="table">
    <tr>
        <td>
            Country
        </td>
        <td>
            <select class="form-control" id="rep_country_id" name="rep_country_id">
                <option value="">select Country</option>
                <?php
                if (!empty($countries)):
                    $sel = '';
                    foreach ($countries as $country):
                        if ($institution_details->rep_country_id == $country->rep_country_id):
                            $sel = 'selected';
                        else:
                            $sel = '';
                        endif;
                        ?>
                        <option value="<?= $country->rep_country_id ?>" <?= $sel ?>><?= $country->country ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </td>
        <td>
            Institution Name
        </td>
        <td>
            <input required type="text" class="form-control" id="institution_name" name="institution_name" placeholder="Institution Name" value="<?= isset($institution_details->institution_name) ? $institution_details->institution_name : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Campus
        </td>
        <td>
            <input required type="text" class="form-control" id="campus" name="campus" placeholder="City" value="<?= isset($institution_details->campus) ? $institution_details->campus : '' ?>">
        </td>
        <td>
            International Contact Person
        </td>
        <td>
            <input type="text" class="form-control" id="intl_contact_person" name="intl_contact_person" placeholder="International Contact Person" value="<?= isset($institution_details->intl_contact_person) ? $institution_details->intl_contact_person : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Email
        </td>
        <td>
            <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= isset($institution_details->email) ? $institution_details->email : '' ?>">
        </td>
        <td>
            Contact No
        </td>
        <td>
            <input required type="text" class="form-control" id="phone" name="phone" placeholder="Contact No" value="<?= isset($institution_details->phone) ? $institution_details->phone : '' ?>">
        </td>
    </tr>
    <tr>
        <td>
            Website
        </td>
        <td>
            <input required type="text" class="form-control" id="website" name="website" placeholder="Address" value="<?= isset($institution_details->website) ? $institution_details->website : '' ?>">
        </td>
        <td>
            Logo
        </td>
        <td>
            <input type="file" class="form-control" id="logo" name="userfile" placeholder="Phone">
        </td>
    </tr>
    <?php if ($mode !== 'view'): ?>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <button type="submit" class="btn btn-primary"><?= isset($institution_details) ? 'Update' : 'Save' ?></button>
                <button type="reset" class="btn btn-warning">Reset Form</button>
            </td>
        </tr>
    <?php endif; ?>
</table>