<?= show_notification(); 
?>
<section class="content-header">
    <h1>
        <?php if ($mode !== 'adds') : ?>
        <a href="<?= site_url('rep_institutions/view') ?>" class="btn btn-warning btn-flat btn-sm"> <i class="fa fa-chevron-left"></i> Back</a> <br><br>
        <?php endif; ?>
        <?= $page_title ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Representing Institution</a></li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title" style="padding: 5px">
                    <?php if ($mode === 'add') : ?>
                    <a href="<?= site_url('rep_institutions/view')?>" class="btn btn-warning">
                        View Representing Institutions
                    </a>
                    <?php endif; ?>
                </div>
                <div class="box-body">
                    <form role="form" method="post" enctype="multipart/form-data" <?php if (isset($institution_details)) : ?> action="<?= site_url('rep_institutions/edit/' . $institution_details->rep_institution_id)?>" <?php endif; ?> >
                        <?php include '_institution_form.php'; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>