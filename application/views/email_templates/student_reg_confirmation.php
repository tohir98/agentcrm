<?php include '_header.php'; ?>
<p><strong>Hi <?php echo ucfirst($first_name); ?>,</strong></p>
<p>
    Thanks for registering with us. Your student account has been created successfully.
</p>


<h4>Find your login details below </h4>
<b>Username :</b> <?= $username ?><br>

<p>
    <a href="<?= $verify_link; ?>">Click here to verify your email and set your password</a>
</p>
<p>
    <a href="<?= $pdf_guide; ?>">Click here to download OAA Study -- Student participation guide</a>
</p>

Your student account can also be used to apply for admission from our website.
<br/>

Thanks<br/><br/>

Regards,<br/>
<?= BUSINESS_NAME; ?>

<br/>
<br/>

<?php include '_footer.php'; ?>