<?php include '_header.php'; ?>
<p><strong>Hi <?php echo ucfirst($first_name); ?>,</strong></p>
<p>
    Thanks for registering with us
</p>

<p>
    <a href="<?= $verify_link; ?>">Click here to verify your email</a>
</p>
<p>
<h4>Find your login details below </h4>
<b>Username :</b> <?= $username ?><br>
<b>Password :</b> <?= $password ?><br>
</p>


<br/>

Thanks<br/><br/>

Regards,<br/>
<?= BUSINESS_NAME; ?>

<br/>
<br/>

<?php include '_footer.php'; ?>