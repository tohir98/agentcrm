<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Reports
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active">Branch Office</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">

                    <h3 class="box-title">
                        <i class="fa fa-fw fa-bars"></i> Branch Office
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php 
                    if (!empty($branches)): ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Branch Office</th>
                                    <th>New</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cnt = 0;
                                foreach ($branches as $branch):
                                    ?>
                                    <tr>
                                        <td><?= ++$cnt; ?></td>
                                        <td><?= $branch->branch_name; ?></td>
                                        <td><?= $branch->app_count; ?></td>
                                        <td><?= $branch->app_count; ?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>