<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Leads Sources
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Leads Management</a></li>
        <li class="active">Leads Sources</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <?php if ($this->user_auth_lib->have_perm('lead_mgt:add_leads_source')): ?>
                        <a href="#leadSource" data-toggle="modal" class="btn btn-success">
                            Add Lead Source
                        </a>
                    <?php endif; ?>

                </div>
                <div class="box-body">
                    <?php if (!empty($sources)): ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Lead Source</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <?php
                            $count = 0;
                            foreach ($sources as $source):
                                ?>
                                <tr>
                                    <td><?= ++$count ?></td>
                                    <td><?= $source->lead_source ?></td>
                                    <td>
                                        <input type="checkbox" <?php if ($source->status == 1): ?> checked <?php endif; ?> class="input-block-level bswitch" data-marchant="<?= $source->lead_source ?>" data-id="<?= $source->lead_source_id ?>">
                                    </td>
                                    <td>
                                        <a class="editLeadSource" data-lead_source="<?= $source->lead_source ?>" data-id="<?= $source->lead_source_id ?>" href="#" onclick="return false;">Edit</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <?php
                    else:
                        echo "<h3>Lead source has not been added yet,</h3>";
                    endif;
                    ?>
                </div>
            </div>


        </div>
    </div>
</section>

<div class="modal" id="leadSource">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Lead Source</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Source Name:</label>
                        <div class="input-group col-xs-9">
                            <input type="text" class="form-control" id="lead_source" name="lead_source" placeholder="Source Name">
                        </div><!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- Edit Modal -->
<div class="modal" id="editleadSource">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add New Lead Source</h4>
            </div>
            <form method="post" action=<?= site_url('/lead_mgt/edit_lead_source') ?>>
				<div class="modal-body">
                    <div class="form-group">
                        <label>Source Name:</label>
                        <div class="input-group col-xs-9">
                            <input type="text" class="form-control" id="e_lead_source" name="lead_source" placeholder="Source Name">
                        </div><!-- /.input group -->
                    </div>
                </div>
                <div class="modal-footer">
					<input type="hidden" name="lead_source_id" id="lead_source_id" />
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
			
			</form>
		</div>
	</div>
</div>


<script>
    $(".bswitch").bootstrapSwitch();
    $('.bswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        var id_ = $(this).data('id');

        var url_ = '<?php echo site_url('lead_mgt/switchStatus'); ?>';
        $.post(
                url_,
                {
                    'state': state,
                    'id': id_,
                },
                function (res) {
                    if (res === 'OK') {
                        toastr["success"]("Operation was successful");
                    } else {
                        toastr["error"]("Sorry, Unable complete your request.");
                    }
                }
        );

        return false;

    });
	
	 $('.editLeadSource').click(function () {
        var leadSource_ = $(this).data('lead_source');
        var id_ = $(this).data('id');
        $("#e_lead_source").val(leadSource_);
        $("#lead_source_id").val(id_);

        $("#editleadSource").modal();
    });
</script>