<a href="<?= $this->input->server('HTTP_REFERER') ?>" class="btn btn-warning btn-flat"><i class="fa fa-chevron-left"></i> Back</a>
<form method="post" action="" enctype="multipart/form-data" ng-app="visa" ng-controller="visaCtrl">
    <div class="modal-body ">
        <div class="form-group">
            <table class="table table-bordered table-striped ">
                <tr>
                    <td style="width: 25%">
                        Issued CoE?
                    </td>
                    <td>
                        <div class="col-xs-4">
                            <input type="radio" name="coe_issued" id="coe_yes" value="1" title="Yes" <?php if (isset($visa_info->coe_issued) && $visa_info->coe_issued == 1): ?> checked <?php endif; ?> />
                            <label for="coe_yes">Yes</label>
                        </div>
                        <div class="col-xs-4">
                            <input type="radio" name="coe_issued" id="coe_no" value="0" title="No"  />
                            <label for="coe_no">No</label>
                        </div>

                    </td>
                </tr>
                <tr id="coe_date_container" <?php if (!isset($visa_info->coe_issued) || $visa_info->coe_issued == 0): ?> style="display: none" <?php endif; ?>>
                    <td>
                        Date CoE Received
                    </td>
                    <td>
                        <input type="text" name="date_coe_rec" id="date_coe_rec" class="form-control" style="cursor: pointer" value="<?php
                        if (isset($visa_info->date_coe_rec)) :
                            echo date('m/d/Y', strtotime($visa_info->date_coe_rec));
                        endif;
                        ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Medical Examination
                    </td>
                    <td>
                        <select required name="medical_exam" id="medical_exam" class="form-control">
                            <option value="" selected>Select</option>
                            <option value="Prior visa">Prior visa</option>
                            <option value="During visa">During visa</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        Visa Lodge?
                    </td>
                    <td>
                        <div class="col-xs-4">
                            <input type="radio" name="visa_lodge" id="visa_lodge_yes" value="1" title="Yes" <?php if (isset($visa_info->visa_lodge) && $visa_info->visa_lodge == 1): ?> checked <?php endif; ?> /> 
                            <label for="visa_lodge_yes">Yes</label>
                        </div>
                        <div class="col-xs-4">
                            <input type="radio" name="visa_lodge" id="visa_lodge_no" value="0" title="No" <?php if (isset($visa_info->visa_lodge) && $visa_info->visa_lodge == 0): ?> checked <?php endif; ?> /> 
                            <label for="visa_lodge_no">No</label>
                        </div>

                    </td>
                </tr>
                <tr id="visa_lodge_container" <?php if (!isset($visa_info->visa_lodge) || $visa_info->visa_lodge == 0): ?> style="display: none" <?php endif; ?>>
                    <td>
                        Date Visa Lodge?
                    </td>
                    <td>
                        <input type="text" name="date_visa_lodge" id="date_visa_lodge" class="form-control" style="cursor: pointer" value="<?php
                        if (isset($visa_info->date_visa_lodge)) :
                            echo date('m/d/Y', strtotime($visa_info->date_visa_lodge));
                        endif;
                        ?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Visa Type
                    </td>
                    <td>
                        <select class="form-control" id="visa_type_id" name="visa_type_id">
                            <option value="" selected>Select Visa Type</option>
                            <?php
                            if (!empty($visa_types)):
                                $sel = '';
                                foreach ($visa_types as $visa_type):

                                    if (isset($visa_info->visa_type_id)) :
                                        if ($visa_info->visa_type_id == $visa_type->visa_type_id):
                                            $sel = 'selected';
                                        else:
                                            $sel = '';
                                        endif;
                                    endif;
                                    ?>
                                    <option value="<?= $visa_type->visa_type_id ?>" <?= $sel ?>><?= $visa_type->visa_type; ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Expiry Date
                    </td>
                    <td>

                        <div class="col-xs-3">
                            <select class="form-control" name="exp_day">
                                <?php for ($cc = 1; $cc <= 31; $cc++): ?>
                                    <option value="<?= $cc; ?>"><?= $cc; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <select class="form-control" name="exp_month">
                                <?php for ($cc = 1; $cc <= 12; $cc++): ?>
                                    <option value="<?= $cc; ?>"><?=
                                        date('M', strtotime(date("Y-{$cc}-d")));
                                        ;
                                        ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-xs-5">
                            <select class="form-control" name="exp_year">
                                <?php for ($cc = date('Y'); $cc <= date('Y') + 5; $cc++): ?>
                                    <option value="<?= $cc; ?>"><?= $cc; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        Passport Number:
                    </td>
                    <td>
                        <input required type="text" class="form-control" id="passport_no" name="passport_no" placeholder="Passport No" value="<?php
                        if (isset($visa_info->passport_no)) {
                            echo $visa_info->passport_no;
                        }
                        ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Visa Status
                    </td>
                    <td>
                        <select required name="visa_status" id="visa_status" class="form-control">
                            <option value="" selected>Select</option>
                            <option value="Pending">Pending</option>
                            <option value="Granted">Granted</option>
                            <option value="Refused">Refused</option>
                            <option value="Withdraw">Withdraw</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        PIC 4020:
                    </td>
                    <td>
                        <div class="col-xs-4">
                            <input type="radio" name="pic" value="1" title="Yes" <?php if (isset($visa_info->pic4020) && $visa_info->pic4020): ?> checked <?php endif; ?> /> Yes
                        </div>
                        <div class="col-xs-4">
                            <input type="radio" name="pic" value="0" title="No" <?php if (isset($visa_info->pic4020) && !$visa_info->pic4020): ?> checked <?php endif; ?> /> No
                        </div>
                    </td>
                </tr>
            </table>
            <fieldset>
                <legend>Upload Visa Documents
                    <a class="pull-right btn btn-warning btn-flat btn-sm" href="#" onclick="return false;" ng-click="addItem()">
                                <i class="fa fa-plus"></i> add more document</a>
                </legend>
                <table class="table">
                    <tr ng-repeat="doc in data.docs">
                        <td>
                            <select name="visa_doc_id[{{$index}}]" class="form-control">
                                <option value="">Select Visa Document</option>
                                <?php
                                if (!empty($visa_docs)):
                                    foreach ($visa_docs as $doc):
                                        ?>
                                        <option value="<?= $doc->visa_doc_id ?>"><?= $doc->visa_doc ?></option>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="file" name="userfile[{{$index}}]" /> 
                        </td>
                        <td> 
                            <a href="#" onclick="return false;" ng-click="removeItem($index)">remove</a>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <table class="table">
                <tr>
                    <td colspan="5" align="right">
                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
