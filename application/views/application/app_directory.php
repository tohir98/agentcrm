<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Application Directory
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Applications</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a href="<?= site_url('applications/new_application') ?>" class="btn btn-primary btn-flat">New Application</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($applicants)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" ng-click="toggleAll()" ></th>
                                    <th>Applicant</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Date of Birth</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($applicants as $applicant): ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" value="" name="employees[]" >
                                        </td>
                                        <td>
                                            <a href="<?= site_url('applications/view_application/' . $applicant->ref_id . '/' . $applicant->first_name . '-' . $applicant->last_name); ?>">
                                                <?= ucfirst($applicant->first_name) . ' ' . ucfirst($applicant->last_name) ?>
                                            </a>
                                        </td>
                                        <td><?= $applicant->gender ?></td>
                                        <td><?= $applicant->email ?></td>
                                        <td><?= date('d M Y', strtotime($applicant->dateof_birth)); ?></td>
                                        
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('applications/view_application/' . $applicant->ref_id . '/' . $applicant->first_name . '-' . $applicant->last_name); ?>">View Record</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    <?php endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

