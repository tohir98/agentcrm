<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Student Enrolment Form
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Student Enrollment Form</a></li>
        <li class="active">Course Selection</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="box">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10">
                            <fieldset>
                                <legend>Course Selection</legend>
                                <form method="post">
                                    <div class="modal-body ">
                                        <div class="form-group">
                                            <table class="table table-bordered table-striped ">
                                                <tr>
                                                    <td style="width: 25%">
                                                        Country
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="rep_country_id" name="rep_country_id">
                                                            <option value="" selected>Select Country</option>
                                                            <?php
                                                            if (!empty($rep_countries)):
                                                                $sel = '';
                                                                foreach ($rep_countries as $rep_country):

                                                                    if (isset($visa_info->visa_type_id)) :
                                                                        if ($visa_info->visa_type_id == $visa_type->visa_type_id):
                                                                            $sel = 'selected';
                                                                        else:
                                                                            $sel = '';
                                                                        endif;
                                                                    endif;
                                                                    ?>
                                                                    <option value="<?= $rep_country->rep_country_id ?>" <?= $sel ?>><?= $rep_country->country; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Rep Institution
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="rep_institution_id" name="rep_institution_id">
Institution                                                            <?php
                                                            if (!empty($rep_institutions)):
                                                                $sel = '';
                                                                foreach ($rep_institutions as $rep_institution):
                                                                    ?>
                                                                    <option value="<?= $rep_institution->rep_institution_id ?>" <?= $sel ?>><?= $rep_institution->institution_name; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Proposed Course
                                                    </td>
                                                    <td>
                                                        <input required type="text" class="form-control" name="propose_course" id="propose_course" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Intake</td>
                                                    <td>
                                                        <div class="col-xs-4">
                                                            <select required class="form-control" name="intake_month_id">
                                                                <?php for ($cc = 1; $cc <= 12; $cc++): ?>
                                                                    <option value="<?= $cc; ?>"><?=
                                                                        date('M', strtotime(date("Y-{$cc}-d")));
                                                                        ?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <select required class="form-control" name="intake_year">
                                                                <?php for ($cc = date('Y'); $cc <= date('Y') + 5; $cc++): ?>
                                                                    <option value="<?= $cc; ?>"><?= $cc; ?></option>
                                                                <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td colspan="5" align="right">
                                                        <a href="<?= $this->input->server('HTTP_REFERER'); ?>" class="btn btn-warning btn-flat"><i class="fa fa-chevron-left"></i> Back</a>
                                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>