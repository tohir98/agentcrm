Your Agent Id is: <span class="label label-success flat"><?= user_type_status($this->user_auth_lib->get('access_level')) ?></span>
<br>
<br>
<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs" id="myTab">
    <li class="<?= strstr($path, 'applications/new_application') ? 'active' : '' ?>">
        <a href="#" >Personal Details</a>
    </li>
    <li class="<?= strstr($path, 'applications/contact_details') ? 'active' : '' ?>">
        <a href="#" >Contact Details</a>
    </li>
    <li class="<?= strstr($path, 'applications/course_selection') ? 'active' : '' ?>">
        <a href="#" >Course Selection</a>
    </li>
    <li class="<?= strstr($path, 'applications/fee_detail') ? 'active' : '' ?>">
        <a href="#" >Fee Detail</a>
    </li>
<!--    <li class="<?= strstr($path, 'applications/document_upload') ? 'active' : '' ?>">
        <a href="#" >Document Upload</a>
    </li>-->
    <li class="<?= strstr($path, 'applications/visa_info') ? 'active' : '' ?>">
        <a href="" >Visa Info</a>
    </li>
</ul>