<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('/applications/app_directory') ?>" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-print"></i> Print
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-file"></i> PDF
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Records</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <select class="form-control students" name="cboStudents" id="cboStudents">
                <option></option>
                <?php
                if (!empty($applicants)):
                    $sel = '';
                    foreach ($applicants as $applicant):
                        $sel = $applicant->ref_id == $record->ref_id ? 'selected' : '';
                        ?>
                        <option value="<?= $applicant->ref_id ?>" data-first_name="<?= ucfirst($applicant->first_name) ?>" data-last_name="<?= ucfirst($applicant->last_name) ?>" <?= $sel ?>><?= ucfirst($applicant->first_name) . ' ' . ucfirst($applicant->last_name); ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                    <div class="row-fluid" style="height:180px;overflow:hidden">

                        <?php
                        $pic_src = "/img/profile-default.jpg";
                        if (!empty($profile_picture)):
                            $pic_src = $profile_picture->profile_picture_url;
                        endif;
                        ?>
                        <img src="<?= $pic_src ?>" style="max-width: 150px">
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <div class="btn-group" style="width: 100%">
                        <button type="button" style="width: 100%" class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit
                            <span class="caret"></span> 
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?= site_url('applications/change_profile_picture/' . $applicant->ref_id . '/' . $applicant->first_name . '-' . $applicant->last_name) ?>">Change Picture</a></li>
                            <?php if ($profile_picture): ?>
                                <li><a class="remove_pic" href="<?= site_url('applications/remove_profile_picture/' . $applicant->applicant_id) ?>">Remove Picture</a></li>
                            <?php endif; ?>

                        </ul>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body no-padding" style="margin-top: 10px">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a class="left_menu_button" href="#profile_container"> Profile </a></li>
                                <li> <a class="left_menu_button" href="#contact_details_container">Contact Details</a></li>
                                <li> <a class="left_menu_button" href="#course_selection_container">Course Selection</a></li>
                                <li> <a class="left_menu_button" href="#fee_detail_container">Fee Detail</a></li>
                                <!--<li> <a class="left_menu_button" href="#document_upload_container">Document Upload</a></li>-->
                                <li> <a class="left_menu_button" href="#visa_info_container">Visa Info</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?php
            echo $profile_content;
            echo $contact_details_content;
            echo $visa_info_content;
            echo $course_selection_content;
            echo $fee_detail_content;
            echo $document_upload_content;
            ?>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
            $('.remove_pic').click(function (e) {
                e.preventDefault();
                var h = this.href;
                var message = 'Are you sure you want to remove this profile picture ?';
                OaaStudy.doConfirm({
                    title: 'Confirm ',
                    message: message,
                    onAccept: function () {
                        window.location = h;
                    }
                });
            });
        });

        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.student_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.students').change(function () {
            var ref_id_ = $(this).val();
            var first_name = $(this).find(':selected').attr('data-first_name');
            var last_name = $(this).find(':selected').attr('data-last_name');
            window.location.href = '<?php echo site_url('applications/view_application'); ?>/' + ref_id_ + '/' + first_name + '-' + last_name;
        });




    });


</script>
