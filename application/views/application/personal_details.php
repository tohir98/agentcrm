<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Student Enrolment Form
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="box">
                <div class="box-header">
                    <?php if ($this->input->server('HTTP_REFERER')): ?>
                        <a href="<?= $this->input->server('HTTP_REFERER') ?>" class="btn btn-warning btn-flat"><i class="fa fa-chevron-left"></i> Back</a>
                    <?php endif; ?>
                </div>
                <form method="post" <?php if (isset($personal_detail)): ?> action="<?= site_url('applications/update_personal_details/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)) ?>" <?php endif; ?>>
                    <div class="box-body">

                        <div class="modal-body">
                            <div class="form-group">
                                <table id="example1" class="table table-bordered table-striped">
                                    <tr>
                                        <td>
                                            Title:
                                        </td>
                                        <td>
                                            <select required class="form-control" name="title_id" id="title_id">
                                                <option value="" >Select Title</option>
                                                <?php
                                                if (!empty($titles)):
                                                    $sel = '';
                                                    foreach ($titles as $title):
                                                        if (isset($personal_detail->title_id)):
                                                            if ($personal_detail->title_id == $title->title_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                        endif;
                                                        ?>
                                                        <option value="<?= $title->title_id ?>" <?= $sel; ?>><?= $title->title ?></option>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Passport Number:
                                        </td>
                                        <td>
                                            <input required type="text" class="form-control" id="passport_no" name="passport_no" placeholder="Passport No" value="<?php
                                            if (isset($personal_detail->passport_no)) {
                                                echo $personal_detail->passport_no;
                                            }
                                            ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        <td>
                                            <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php
                                            if (isset($personal_detail->first_name)) {
                                                echo $personal_detail->first_name;
                                            }
                                            ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Middle Name:
                                        </td>
                                        <td>
                                            <input required type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" value="<?php
                                            if (isset($personal_detail->middle_name)) {
                                                echo $personal_detail->middle_name;
                                            }
                                            ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Surname:
                                        </td>
                                        <td>
                                            <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Surname" value="<?php
                                            if (isset($personal_detail->last_name)) {
                                                echo $personal_detail->last_name;
                                            }
                                            ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gender:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <input type="radio" name="gender_id" value="<?= MALE ?>" <?php
                                                    if (isset($personal_detail->gender_id) && $personal_detail->gender_id == MALE) {
                                                        echo 'checked';
                                                    }
                                                    ?>>&nbsp; Male  &nbsp;
                                                </div>
                                                <div class="col-xs-3">
                                                    <input type ="radio" name="gender_id" value="<?= FEMALE ?>" <?php
                                                    if (isset($personal_detail->gender_id) && $personal_detail->gender_id == FEMALE) {
                                                        echo 'checked';
                                                    }
                                                    ?>>&nbsp; Female
                                                </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control" id="dateof_birth" name="dateof_birth" placeholder="Date of birth" style="cursor: pointer" value="<?php
                                                    if (isset($personal_detail->dateof_birth)) {
                                                        echo date('m/d/Y', strtotime($personal_detail->dateof_birth));
                                                    }
                                                    ?>">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Marital Status:
                                        </td>
                                        <td>
                                            <select required class="form-control" name="marital_status_id" id="marital_status_id">
                                                <option selected>Select marital Status</option>
                                                <?php
                                                if (!empty($marital_statuses)):
                                                    $sel = '';
                                                    foreach ($marital_statuses as $marital_status):
                                                        if (isset($personal_detail->marital_status_id)):
                                                            if ($personal_detail->marital_status_id == $marital_status->marital_status_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                        endif;
                                                        ?>
                                                        <option value="<?= $marital_status->marital_status_id ?>" <?= $sel; ?>><?= $marital_status->marital_status; ?></option>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nationality:
                                        </td>
                                        <td>
                                            <select class="form-control" name="nationality_id" id="nationality_id">
                                                <option selected>Nationality</option>
                                                <?php
                                                if (!empty($countries)):
                                                    $sel = '';
                                                    foreach ($countries as $country):
                                                        if (isset($personal_detail->nationality_id)):
                                                            if ($personal_detail->nationality_id == $country->country_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                        endif;
                                                        ?>
                                                        <option value="<?= $country->country_id ?>" <?= $sel; ?>><?= $country->country; ?></option>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Country of Birth:
                                        </td>
                                        <td>
                                            <select class="form-control" id="country_id" name="country_id">
                                                <option selected>Country</option>
                                                <?php
                                                if (!empty($countries)):
                                                    $sel = '';
                                                    foreach ($countries as $country):
                                                        if (isset($personal_detail->country_id)):
                                                            if ($personal_detail->country_id == $country->country_id):
                                                                $sel = 'selected';
                                                            else:
                                                                $sel = '';
                                                            endif;
                                                        endif;
                                                        ?>
                                                        <option value="<?= $country->country_id ?>" <?= $sel; ?>><?= $country->country; ?></option>
                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>

                                            <div class="col-xs-4">
                                                <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php
                                                if (isset($personal_detail->email)) {
                                                    echo $personal_detail->email;
                                                }
                                                ?>">
                                            </div>
                                            <div class="col-xs-3">
                                                Lead Source:
                                            </div>
                                            <div class="col-xs-5">
                                                <select class="form-control" id="lead_source_id" name="lead_source_id">
                                                    <option value="0">Select Lead Source</option>
                                                    <?php
                                                    if (!empty($lead_sources)):
                                                        $sel = '';
                                                        foreach ($lead_sources as $lead_source):
                                                            if (isset($personal_detail->lead_source_id)):
                                                                if ($personal_detail->lead_source_id == $lead_source->lead_source_id):
                                                                    $sel = 'selected';
                                                                else:
                                                                    $sel = '';
                                                                endif;
                                                            endif;
                                                            ?>
                                                            <option value="<?= $lead_source->lead_source_id ?>"  <?= $sel; ?>><?= $lead_source->lead_source; ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <?php if (user_type_status($this->user_auth_lib->get('access_level')) === 'Agent'): ?>
                        <div class="box-footer pull-right">
                            <button type="reset" class="btn btn-warning">Reset</button>
                            <button type="submit" class="btn btn-primary"><?= isset($personal_detail) ? 'Update' : 'Save'; ?></button>
                        </div>
                    <?php endif; ?>
                </form>


            </div>
        </div>
    </div>
</section>


<script>
    $('document').ready(function () {
        $('#dateof_birth').datepicker();
    });
</script>