<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Student Enrolment Form
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Student Enrolment Form</a></li>
        <li class="active">Visa Info</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="box">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Visa Information</legend>
                                <?php include '_visa_form.php'; ?>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('document').ready(function () {
        $('#date_coe_rec').datepicker();
        $('#date_visa_lodge').datepicker();

        $("#coe_yes").click(function () {
            $("#coe_date_container").show();
        });

        $("#coe_no").click(function () {
            $("#coe_date_container").hide();
        });

        $("#visa_lodge_yes").click(function () {
            $("#visa_lodge_container").show();
        });

        $("#visa_lodge_no").click(function () {
            $("#visa_lodge_container").hide();
        });
    });


    var visaApp = angular.module('visa', []);

    visaApp.controller('visaCtrl', function ($scope) {

        var getSiteUrl = function () {
            return window.location.protocol + '//' + window.location.host + '/';
        };

        $scope.data = {docs: []};

        $scope.blankResult = {};

        $scope.addItem = function () {
            $scope.data.docs.push(angular.copy($scope.blankResult));
        };

        $scope.removeItem = function (idx) {
            if ($scope.data.docs.length > 0) {
                $scope.data.docs.splice(idx, 1);
            }
            $scope.check();
        };

        $scope.check = function () {
            if ($scope.data.docs.length == 0) {
                $scope.addItem();
            }
        };

        $scope.showSubCategory = function (category_id) {
            console.log(category_id);
            $scope.category_id = category_id;

            $scope.myPromise = $http.get(getSiteUrl() + "setup/json_get_sub_sources/" + category_id)
                    .success(function (response) {
                        $scope.sub_sources = response;
                    });

        };

        $scope.check();

    });
</script>