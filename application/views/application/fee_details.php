<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Student Enrolment Form
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Student Enrollment Form</a></li>
        <li class="active">Course Selection</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="box">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <!--                                <legend>Fee Details</legend>-->
                                <form method="post">
                                    <div class="modal-body ">
                                        <div class="form-group">
                                            <table class="table table-bordered table-striped ">
                                                <tr>
                                                    <td style="width: 25%">
                                                        Total Fee
                                                    </td>
                                                    <td>
                                                        <input required type="text" class="form-control" name="total_fee" id="total_fee" value="<?php if (isset($fee_detail->total_fee)): echo $fee_detail->total_fee;
                                                            endif; ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Tuition Fee
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="tuition_fee_id" name="tuition_fee_id">
                                                            <option value="" selected>Select</option>                                                            <?php
                                                            if (!empty($per_tuitions)):
                                                                $sel = '';
                                                                foreach ($per_tuitions as $id => $value):
                                                                    if ($fee_detail->tuition_fee_id == $id):
                                                                        $sel = 'selected';
                                                                    else:
                                                                        $sel = '';
                                                                    endif;
                                                                    ?>
                                                                    <option value="<?= $id ?>" <?= $sel ?>><?= $value; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Initial Payment By:
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="initial_fee_payment_id" name="initial_fee_payment_id">
                                                            <option value="" selected>Select</option>                                                            <?php
                                                            if (!empty($payment_methods)):
                                                                $sel = '';
                                                                foreach ($payment_methods as $id => $value):
                                                                     if ($fee_detail->initial_fee_payment_id == $id):
                                                                        $sel = 'selected';
                                                                    else:
                                                                        $sel = '';
                                                                    endif;
                                                                    ?>
                                                                    <option value="<?= $id ?>" <?= $sel ?>><?= $value; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Agent Fee
                                                    </td>
                                                    <td>
                                                        <input required type="text" class="form-control" name="agent_fee" id="agent_fee" value="<?php if (isset($fee_detail->agent_fee)): echo $fee_detail->agent_fee;
                                                            endif; ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Enrollment Fee
                                                    </td>
                                                    <td>
                                                        <input required type="text" class="form-control" name="enrolment_fee" id="enrolment_fee" value="<?php if (isset($fee_detail->enrolment_fee)): echo $fee_detail->enrolment_fee;
                                                            endif; ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        OSHC:
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="oshc_id" name="oshc_id">
                                                            <option value="" selected>Select</option>                                                            <?php
                                                            if (!empty($oshcs)):
                                                                $sel = '';
                                                                foreach ($oshcs as $id => $value):
                                                                    if ($fee_detail->oshc_id == $id):
                                                                        $sel = 'selected';
                                                                    else:
                                                                        $sel = '';
                                                                    endif;
                                                                    ?>
                                                                    <option value="<?= $id ?>" <?= $sel ?>><?= $value; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        OSHC Amount
                                                    </td>
                                                    <td>
                                                        <input required type="text" class="form-control" name="oshc_amount" id="oshc_amount" value="<?php if (isset($fee_detail->oshc_amount)): echo $fee_detail->oshc_amount;
                                                            endif; ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Course Duration
                                                    </td>
                                                    <td>
                                                        <select required class="form-control" id="course_duration_id" name="course_duration_id">
                                                            <option value="" selected>Select Course Duration</option>
                                                            <?php
                                                            if (!empty($course_durations)):
                                                                $sel = '';
                                                                foreach ($course_durations as $course):

                                                                    if (isset($fee_detail->course_duration_id)) :
                                                                        if ($fee_detail->course_duration_id == $course->course_duration_id):
                                                                            $sel = 'selected';
                                                                        else:
                                                                            $sel = '';
                                                                        endif;
                                                                    endif;
                                                                    ?>
                                                                    <option value="<?= $course->course_duration_id ?>" <?= $sel ?>><?= $course->course_duration; ?></option>
                                                                    <?php
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Offer Accepted?
                                                    </td>
                                                    <td>
                                                        <div class="col-xs-4">
                                                            <input type="radio" name="offer_accepted" id="offer_accepted_yes" value="1" title="Yes" <?php if (isset($fee_detail->offer_accepted) && $fee_detail->offer_accepted == 1): ?> checked <?php endif; ?> />
                                                            <label for="offer_accepted_yes">Yes</label>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <input type="radio" name="offer_accepted" id="offer_accepted_no" value="0" title="No"  />
                                                            <label for="offer_accepted_no">No</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="offer_date_container" <?php if (!isset($fee_detail->offer_accepted) || $fee_detail->offer_accepted == 0): ?> style="display: none" <?php endif; ?>>
                                                    <td>
                                                        Date Offer Accepted
                                                    </td>
                                                    <td>
                                                        <input type="text" name="date_offer_accepted" id="date_offer_accepted" class="form-control" style="cursor: pointer" value="<?php
                                                               if (isset($fee_detail->date_offer_accepted)) :
                                                                   echo date('m/d/Y', strtotime($fee_detail->date_offer_accepted));
                                                               endif;
                                                               ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="right">
                                                        <a href="<?= $this->input->server('HTTP_REFERER'); ?>" class="btn btn-warning btn-flat"><i class="fa fa-chevron-left"></i> Back</a>
                                                        <button type="submit" class="btn btn-primary btn-flat">Update</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('document').ready(function () {
        $('#date_offer_accepted').datepicker();
        $("#offer_accepted_yes").click(function () {
            $("#offer_date_container").show();
        });

        $("#offer_accepted_no").click(function () {
            $("#offer_date_container").hide();
        });
    });
</script>