<div class="box box-solid box-primary student_info_box" id="profile_container">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Profile
            <a href="<?= $link = site_url('applications/new_application/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="pull-right btn btn-warning" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <?php if($record): ?>
        <table class="table table-striped">
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <?= ucfirst($record->first_name) . ' ' . ucfirst($record->middle_name) . ' ' . ucfirst($record->last_name) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Gender:
                </td>
                <td>
                    <?= $record->gender ?>
                </td>
            </tr>
            <tr>
                <td>
                    Marital Status:
                </td>
                <td>
                    <?= $record->marital_status ?>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Birth:
                </td>
                <td>
                    <?= $record->dateof_birth !== '0000-00-00' ? date('d-M-Y', strtotime($record->dateof_birth)) : 'NA' ?>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?= $record->email ?>
                </td>
            </tr>
            <tr>
                <td>
                    Lead Source:
                </td>
                <td>
                    <?= $record->lead_source ?>
                </td>
            </tr>
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <?= $record->country ?>
                </td>
            </tr>
            <tr>
                <td>
                    Nationality:
                </td>
                <td>
                    <?= $record->nationality ?>
                </td>
            </tr>
            <tr>
                <td>
                    Application Date:
                </td>
                <td>
                    <?= $record->date_created !== '0000-00-00' ? date('d-M-Y', strtotime($record->date_created)) : 'NA' ?>
                </td>
            </tr>
        </table>
        <?php endif; ?>
    </div>
</div>
