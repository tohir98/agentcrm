<div class="box box-solid box-primary student_info_box" id="fee_detail_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;Fee Details
            <a href="<?= $link = site_url('applications/fee_detail/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="pull-right btn btn-warning" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <table class="table">
                <tr>
                    <td>Total Fee</td>
                    <td><?= number_format($fee_detail->total_fee, 2); ?></td>
                </tr>
                <tr>
                    <td>Tuition Fee</td>
                    <td><?= $per_tuitions[$fee_detail->tuition_fee_id]; ?></td>
                </tr>
                <tr>
                    <td>Initial Payment By</td>
                    <td><?= $payment_methods[$fee_detail->initial_fee_payment_id]; ?></td>
                </tr>
                <tr>
                    <td>Agent Fee</td>
                    <td><?= number_format($fee_detail->agent_fee, 2); ?></td>
                </tr>
                <tr>
                    <td>Enrollment Fee</td>
                    <td><?= number_format($fee_detail->enrolment_fee, 2); ?></td>
                </tr>
                <tr>
                    <td>OSHC</td>
                    <td><?= $oshcs[$fee_detail->oshc_id]; ?></td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td><?= number_format($fee_detail->oshc_amount, 2); ?></td>
                </tr>
                <tr>
                    <td>Course Duration</td>
                    <td><?= $fee_detail->course_duration; ?></td>
                </tr>
                <tr>
                    <td>Offer Accepted</td>
                    <td><?= $fee_detail->offer_accepted > 0 ? 'Yes' : 'No'; ?></td>
                </tr>
                <tr>
                    <td>Date Offer Accepted</td>
                    <td><?= $fee_detail->offer_accepted > 0 ? date('d M Y', strtotime($fee_detail->date_offer_accepted)) : ''; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>