<div class="box box-solid box-primary student_info_box" id="visa_info_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;Visa Information
            <a href="<?= $link = site_url('applications/visa_info/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="pull-right btn btn-warning" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <?php
            if (!empty($visa_info)):
                $visa_info = $visa_info[0];
                ?>
                <table class="table">
                    <tr>
                        <td><b>Visa Type:</b></td>
                        <td>
                            <?= $visa_info->visa_type ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Expiry Date:</b></td>
                        <td>
                            <?= date('d M Y', strtotime($visa_info->expiry_date)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Passport No:</b></td>
                        <td>
                            <?= $visa_info->passport_no ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Health Cover:</b></td>
                        <td>
                            <?= $visa_info->health_cover ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Medical Exam:</b></td>
                        <td>
                            <?= $visa_info->medical_exam ?>
                        </td>
                    </tr>
                    <?php if ($visa_info->visa_lodge): ?>
                        <tr>
                            <td><b>Date Visa Lodge:</b></td>
                            <td>
                                <?= date('d M Y', strtotime($visa_info->date_visa_lodge)) ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($visa_info->coe_issued): ?>
                        <tr>
                            <td><b>Date COE Received:</b></td>
                            <td>
                                <?= date('d M Y', strtotime($visa_info->date_coe_rec)) ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </table>

                <?php
            else:
                echo show_no_data("Record not added yet.<a href=$link> Clich here to add Visa Information <a>");
            endif;
            ?>
            <?php if (!empty($visa_docs)): ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Document</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($visa_docs as $doc):
                            ?>
                            <tr>
                                <td><?= $doc->visa_doc; ?></td>
                                <td>
                                    <a href="<?= $doc->doc_image_url; ?>" target="_blank"> 
                                        <?= $doc->visa_doc; ?>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
            <?php endif; ?>

        </div>
    </div>
</div>