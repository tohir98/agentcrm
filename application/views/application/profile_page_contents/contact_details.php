<div class="box box-solid box-primary student_info_box" id="contact_details_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;Contact Details
            <a href="<?= $link = site_url('applications/contact_details/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="pull-right btn btn-warning" style="margin-right: 5px">Edit</a>
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <?php if (!empty($contact_details)):
                $contact_details = $contact_details[0];
                ?>
                <b>Address Line 1:</b> <?= $contact_details->address_1 ?> <br>
                <b>Address Line 2:</b> <?= $contact_details->address_2 ?> <br>
                <b>City:</b> <?= $contact_details->suburb ?> <br>
                <b>State:</b> <?= $contact_details->state_id ?> <br>
                <i class="fa fa-phone-square"></i> <?= $contact_details->telephone ?> <br>
                <i class="fa fa-phone-square"></i> <?= $contact_details->mobile ?>
                <?php
            else:
                echo show_no_data("Record not added yet.<a href=$link> Clich here to add Contact Detais <a>");
            endif;
            ?>

        </div>
    </div>
</div>