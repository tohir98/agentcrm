<div class="box box-solid box-primary student_info_box" id="course_selection_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;Course Selection
            <a href="<?= $link = site_url('applications/course_selection/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="pull-right btn btn-warning" style="margin-right: 5px">
                <?= !empty($course_selection) ? 'Edit' : 'Add'; ?> 
            </a>
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <?php
            if (!empty($course_selection)):
                $course_selection = $course_selection[0];
            $intake_month = $course_selection->intake_month_id;
                ?>
                <b>Country:</b> <?= $course_selection->country ?> <br>
                <b>Institution:</b> <?= $course_selection->institution_name ?> <br>
                <b>Proposed Course:</b> <?= $course_selection->propose_course ?> <br>
                <b>In Take:</b> <?= date('M', strtotime(date("Y-{$intake_month}-d"))) .' ' . $course_selection->intake_year ?> 
                <?php
            else:
                echo show_no_data("Record not added yet.<a href=$link> Clich here to add Course Selection <a>");
            endif;
            ?>

        </div>
    </div>
</div>