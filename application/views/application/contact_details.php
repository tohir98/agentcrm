<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Student Enrolment Form
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Student Enrolment Form</a></li>
        <li class="active">Contact Details</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php include '_tab.php'; ?>
            <div class="box">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Contact Address</legend>
                                <form method="post">
                                    <div class="modal-body ">
                                        <table class="table table-bordered table-striped ">
                                            <tr>
                                                <td> Address 1 </td>
                                                <td>
                                                    <input required type="text" class="form-control" id="address_1" name="address_1" placeholder="Address 1" value="<?php
                                                    if (isset($contact_details->address_1)) {
                                                        echo $contact_details->address_1;
                                                    }
                                                    ?>">
                                                </td>
                                                <td>  Address 2 </td>
                                                <td>
                                                    <input type="text" class="form-control" id="address_2" name="address_2" placeholder="Address 2" value="<?php
                                                    if (isset($contact_details->address_2)) {
                                                        echo $contact_details->address_2;
                                                    }
                                                    ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Suburb </td>
                                                <td>
                                                    <input required type="text" class="form-control" id="suburb" name="suburb" placeholder="Suburb" value="<?php
                                                    if (isset($contact_details->suburb)) {
                                                        echo $contact_details->suburb;
                                                    }
                                                    ?>">
                                                </td>
                                                <td> State </td>
                                                <td>
                                                    <input required type="text" class="form-control" id="state_id" name="state_id" placeholder="State" value="<?php
                                                    if (isset($contact_details->state_id)) {
                                                        echo $contact_details->state_id;
                                                    }
                                                    ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Postcode </td>
                                                <td>
                                                    <input required type="text" class="form-control" id="postcode_id" name="postcode_id" placeholder="Postcode" value="<?php
                                                    if (isset($contact_details->postcode_id)) {
                                                        echo $contact_details->postcode_id;
                                                    }
                                                    ?>">
                                                </td>
                                                <td>  Telephone Home  </td>
                                                <td>
                                                    <input required type="text" class="form-control" id="telephone" name="telephone" placeholder="Telephone" value="<?php
                                                    if (isset($contact_details->telephone)) {
                                                        echo $contact_details->telephone;
                                                    }
                                                    ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Mobile </td>
                                                <td>
                                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" value="<?php
                                                    if (isset($contact_details->mobile)) {
                                                        echo $contact_details->mobile;
                                                    }
                                                    ?>">
                                                </td>
                                                <td> Country </td>
                                                <td>
                                                    <select class="form-control" id="country_id" name="country_id">
                                                        <option value="">select Country</option>
                                                        <?php
                                                        if (!empty($countries)):
                                                            $sel = '';
                                                            foreach ($countries as $country):
                                                                ?>
                                                                <option value="<?= $country->country_id ?>" <?= $sel ?>><?= $country->country ?></option>
                                                                <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <a href="<?= site_url('applications/view_application/'.$this->uri->segment(3) . '/' . $this->uri->segment(4)) ?>" class="btn btn-warning"> <i class="fa fa-chevron-left"></i> Back </a>
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>