<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Eduportal
 * Company subdomain id lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage Subdomain
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright Â© 2015 EduPortal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Subdomain {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Detected subdomain 
     * 
     * @access private
     * @var string
     */
    private $subdomain;
    private $child_subdomain;
    private $school;
    private $except_ids = array(
        'testportal',
        'eduportal',
        'www',
        'portal',
    );

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        // Load CI object
        $this->CI = get_instance();

        $this->CI->load->helper('url');
//        $this->CI->load->library('uri');
        // Load school model
        $this->CI->load->model('school/school_model', 's_model');
        $this->init();
    }

// End func __construct

    /**
     * Initialize subdomain detection
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    private function init() {

        $this->init_subdomain();

        if (!$this->subdomain) {

            if ($this->CI->uri->segment(1) == '') {
                redirect(site_url('home'));
            }

            if ($this->CI->uri->segment(1) != 'home') {
                return true;
            }
            return;
        }

        $this->load_school();

        if (empty($this->school)) {
            show_error('The account you\'re looking for does not exist. Please check the address and try again. If you need further assistance, please contact our help desk via: ' . EDUPORTAL_SUPPORT_EMAIL . '<br/>', 404, 'Account not Found');
        }
    }

    private function init_subdomain() {

        $subdomain_arr = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain_name = $subdomain_arr[0];

        // Assume www.{subdomain}
        if (isset($subdomain_arr[1])) {
            if ($subdomain_arr[0] == 'www' and ! in_array($subdomain_arr[1], $this->except_ids)) {
                $this->subdomain = $subdomain_arr[1];
                return true;
            }
        }

        // Landing page.
        if (in_array($subdomain_name, $this->except_ids)) {
            $this->subdomain = false;
        } else {
            $this->subdomain = $subdomain_name;
        }
    }

// End func init_subdomain

    public function subdomain_name() {

        $subdomain_arr = explode('.', $_SERVER['HTTP_HOST']);
        return $subdomain_arr[1];
    }

// End func subdomain_name

    private function load_school() {

        if (!$this->subdomain) {
            $this->school = [];
            return false;
        }

        $this->school = $this->CI->s_model->load_school($this->subdomain) ? : array();

        if (!empty($this->school)) {
            //$this->child_subdomain = $this->school[0]->child_subdomain;
            $school = $this->school[0];
            //if company has parent and parent is not same as company, then go to parent.
            //this prevents users from logging in directly to sub-companies.

            return true;
        }

        return false;
    }

   

    public function get_school_id() {
        return $this->school[0]->school_id ? : -1;
    }

    public function get_subdomain() {
        return $this->subdomain;
    }

// End func get_subdomain

    public function get_child_subdomain() {
        return $this->child_subdomain;
    }

    public function company_subdomain() {
        return $this->subdomain;
    }

    public function school() {
        return $this->school;
    }

    public function get_school() {
        return current((array) $this->school);
    }

}

// End class subdomain
// End file user_auth.php
