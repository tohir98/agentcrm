<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * 
 * @property Basic_model $basic_model Description
 * @property Applicant_model $app_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property Rep_country_model $rc_model Description
 * @property Rep_institution_model $ri_model Description
 * @property User_nav_lib $user_nav_lib Description
 */
class Application_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('application/applicant_model', 'app_model');
        $this->load->model('setup/rep_country_model', 'rc_model');
        $this->load->model('setup/rep_institution_model', 'ri_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function new_application($ref_id = null, $full_name = null) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $ref_id = $this->app_model->saveApplicant(request_post_data());
            if ($ref_id && ($ref_id !== '')) {
                notify('success', 'Applicants details saved successfully');
            } else {
                notify('error', 'Unable to save applicant information, pls try again later');
            }
            redirect(site_url('applications/app_directory'));
        }

        $data = [
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES),
            'titles' => $this->basic_model->fetch_all_records(TBL_TITLE),
            'marital_statuses' => $this->basic_model->fetch_all_records('marital_status'),
            'lead_sources' => $this->basic_model->fetch_all_records(TBL_LEAD_SOURCE),
        ];

        if ($ref_id) {
            $data['personal_detail'] = $this->basic_model->fetch_all_records(TBL_APPLICANTS, ['applicant_id' => $this->app_model->getApplicantByRef($ref_id)])[0];
        }
        $this->user_nav_lib->run_page('application/personal_details', $data, 'Application | Application CRM');
    }

    public function contact_details($ref_id, $full_name = null) {
        $this->user_auth_lib->check_login();

        if (!isset($ref_id) || $ref_id == '') {
            trigger_error('Ref code missing');
        }

        if (request_is_post()) {
            if ($this->app_model->saveApplicantContact($ref_id, request_post_data())) {
                notify('success', 'Information successfully saved');
            } else {
                notify('error', 'Unable to save applicant information, pls try again later');
            }

            redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name . '#contact_details_container'));
        }

        $data = [
            'contact_details' => $this->basic_model->fetch_all_records(TBL_APPLICANT_CONTACTS, ['applicant_id' => $this->app_model->getApplicantByRef($ref_id)])[0],
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        ];
        $this->user_nav_lib->run_page('application/contact_details', $data, 'Application | Application CRM');
    }

    public function visa_info($ref_id, $full_name = NULL) {
        $this->user_auth_lib->check_login();

        if (!isset($ref_id) || $ref_id == '') {
            trigger_error('Ref code missing');
        }

        if (request_is_post()) {
            if ($this->app_model->saveVisaInfo($ref_id, request_post_data(), $_FILES['userfile'])) {
                notify('success', 'Applicant visa information saved successfully');
            } else {
                notify('error', 'Applicant visa information could not be saved at moment, pls try again');
            }

            redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name . '#visa_info_container'));
        }

        $data = [
            'visa_types' => $this->basic_model->fetch_all_records(TBL_VISA_TYPES),
            'visa_docs' => $this->basic_model->fetch_all_records(TBL_VISA_DOCS),
            'visa_info' => $this->basic_model->fetch_all_records(TBL_APPLICANT_VISA_INFO, ['applicant_id' => $this->app_model->getApplicantByRef($ref_id)])[0],
        ];

        $this->user_nav_lib->run_page('application/visa_info', $data, 'Application | Agent CRM');
    }

    public function fee_detail($ref_id, $full_name = NULL) {
        $this->user_auth_lib->check_login();
        if (!isset($ref_id) || $ref_id == '') {
            trigger_error('Ref code missing');
        }

        if (request_is_post()) {
            if ($this->app_model->saveFeeDetails($ref_id, request_post_data())) {
                notify('success', 'Applicant fee details saved successfully');
            } else {
                notify('error', 'Applicant fee details could not be saved at moment, pls try again');
            }

            redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name . '#fee_detail_container'));
        }

        $data = [
            'per_tuitions' => $this->basic_model->get_tuition_per(),
            'oshcs' => $this->basic_model->get_oshc(),
            'payment_methods' => $this->basic_model->get_paymemt_methods(),
            'course_durations' => $this->basic_model->fetch_all_records(TBL_COURSE_DURATION),
            'fee_detail' => $this->basic_model->fetch_all_records(TBL_APPLICANT_FEES, ['applicant_id' => $this->app_model->getApplicantByRef($ref_id)])[0],
        ];

        $this->user_nav_lib->run_page('application/fee_details', $data, 'Application | ' . BUSINESS_NAME);
    }

    public function app_directory() {
        $this->user_auth_lib->check_login();
        $data = array(
            'applicants' => $this->app_model->fetchApplicants($this->user_auth_lib->get('user_id'))
        );
        $this->user_nav_lib->run_page('application/app_directory', $data, 'Applications | Agent CRM');
    }

    public function view_application($ref_id, $full_name) {
        $this->user_auth_lib->check_login();

        $applicant_id = $this->app_model->getApplicantByRef($ref_id);

        $contact_details_content = $this->load->view('application/profile_page_contents/contact_details', array(
            'contact_details' => $this->basic_model->fetch_all_records(TBL_APPLICANT_CONTACTS, ['applicant_id' => $applicant_id])
            ), true);

        $visa_info_content = $this->load->view('application/profile_page_contents/visa_info', array(
            'visa_info' => $this->app_model->fetchApplicantVisaInfo($applicant_id),
            'visa_docs' => $this->app_model->fetchApplicantVisaDocs($applicant_id)
            ), true);

        $course_selection_content = $this->load->view('application/profile_page_contents/course_selection', array(
            'course_selection' => $this->app_model->fetchApplicantCourseSelection($applicant_id)
            ), true);

        $fee_detail_content = $this->load->view('application/profile_page_contents/fee_detail', array(
            'fee_detail' => $this->app_model->fetchApplicantFeeDetails($applicant_id),
            'per_tuitions' => $this->basic_model->get_tuition_per(),
            'oshcs' => $this->basic_model->get_oshc(),
            'payment_methods' => $this->basic_model->get_paymemt_methods(),
            ), true);

        $document_upload_content = $this->load->view('application/profile_page_contents/document_upload', array(
            'document_upload' => NULL
            ), true);

        $profile_content = $this->load->view('application/profile_page_contents/profile', array(
            'record' => $this->app_model->fetchApplicants($this->user_auth_lib->get('user_id'), $ref_id)[0]
            ), true);

        $data = array(
            'profile_content' => $profile_content,
            'contact_details_content' => $contact_details_content,
            'visa_info_content' => $visa_info_content,
            'course_selection_content' => $course_selection_content,
            'fee_detail_content' => $fee_detail_content,
            'document_upload_content' => $document_upload_content,
            'applicants' => $this->app_model->fetchApplicants($this->user_auth_lib->get('user_id')),
            'profile_picture' => $this->app_model->getApplicantProfilePic($applicant_id)
        );
        $this->user_nav_lib->run_page('application/profile_page', $data, 'Applicant Record | ' . BUSINESS_NAME);
    }

    public function course_selection($ref_id, $full_name = NULL) {
        $this->user_auth_lib->check_login();

        if (!isset($ref_id) || $ref_id == '') {
            trigger_error('Ref code missing');
        }

        if (request_is_post()) {
            if ($this->app_model->saveCourseSelection($ref_id, request_post_data())) {
                notify('success', 'Applicant course selection saved successfully');
            } else {
                notify('error', 'Applicant course selection could not be saved at moment, pls try again');
            }

            redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name . '#course_selection_container'));
        }

        $data = [
            'rep_countries' => $this->rc_model->get_rep_countries(),
            'rep_institutions' => $this->ri_model->fetchRepresentingInstitutions(),
        ];

        $this->user_nav_lib->run_page('application/course_selection', $data, 'Application | ' . BUSINESS_NAME);
    }

    public function update_personal_details($ref_id, $full_name = null) {
        if (request_is_post()) {
            if ($this->app_model->update_personal_details($ref_id, request_post_data())) {
                notify('success', "Personal details updated successfully");
            } else {
                notify('error', "Ops! Unable to update personal details, pls try again");
            }

            redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name . '#profile_container'));
        }
    }

    public function change_profile_picture($ref_id, $full_name) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if (request_is_post()) {
                if ($this->app_model->update_applicant_picture($this->input->post('applicant_id'), $_FILES['userfile'])) {
                    notify('success', 'Product image updated successfully');
                } else {
                    notify('error', 'Unable to update product image at moment, pls try again');
                }
                redirect(site_url('/applications/view_application/' . $ref_id . '/' . $full_name));
            }
        }

        $data = [
            'profile_picture' => $this->app_model->fetchApplicantPicture($ref_id),
            'applicant_id' => $this->app_model->getApplicantByRef($ref_id)
        ];

        $this->user_nav_lib->run_page('application/edit_profile_picture', $data, 'Change Profile Picture | ' . BUSINESS_NAME);
    }

    public function remove_profile_picture($applicant_id) {
        $this->user_auth_lib->check_login();

        if ($this->app_model->removeProfilePicture($applicant_id)) {
            notify('success', 'Profile picture removed successfully');
        } else {
            notify('error', 'Unable to remove profile picture, pls try again');
        }

        redirect($this->input->server('HTTP_REFERER'));
    }

}
