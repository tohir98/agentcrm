<?php

/**
 * Description of Reports_controller
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Branch_model $branch_model Description
 */
class Reports_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('application/applicant_model', 'app_model');
        $this->load->model('setup/branch_model', 'branch_model');
    }
    
    public function branch_office_report() {
        $this->user_auth_lib->check_login();
        
        $data = array(
            'branches' => $this->branch_model->fetchBranchReport()
        );
        
        $this->user_nav_lib->run_page('reports/branch_office', $data, 'Branch Offfice | '. BUSINESS_NAME);
    }

}
