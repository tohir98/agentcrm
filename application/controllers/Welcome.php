<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * @property Basic_model $basic_model Description
 * @property CI_Loader $load Description
 */
class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $this->load->view('welcome_message');
    }

    public function student_reg() {
        $hdata = array(
            'page_title' => 'Student Registration'
        );

        if (request_post_data()) {
            $this->load->model('basic_model');
            $this->load->helper('notification_helper');
            $this->load->library('mailer');

            if ($this->basic_model->saveReg(request_post_data())) {
                notify('success', "Registration was successful");
            } else {
                notify('error', "Ops! Someone has registered with the email you provided");
            }
        }

        $data = [];
        $this->load->view('templates/header', $hdata);
        $this->load->view('landing', $data);
    }

}
