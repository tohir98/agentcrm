<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of users_controller
 *
 * @category Controller
 * @author TOHIR Omoloye <otcleantech@gmail.com>
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 * @property Basic_model $basic_model Description
 * @property User_model $u_model Description
 */
class Users_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/User_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function index() {
        $this->login();
    }

    public function login() {
        $data = array(
            'email_message' => '',
            'password_message' => ''
        );
        $message = '';
        if (request_is_post()) {
            $response = $this->user_auth_lib->login(request_post_data());

            if (!$response ) {
                $message = 'Invalid E-mail/password.';
            } else {
                switch ((int) $response['access_level']) {
                    case USER_TYPE_SUPER_ADMIN:
                        redirect('admin/dashboard');
                        break;
                    case USER_TYPE_AGENT:
                        redirect('admin/dashboard');
                        break;
                    case USER_TYPE_BRANCH:
                        redirect('admin/dashboard');
                        break;
                    default:
                        break;
                }
            }
        }

        $data['email_message'] = $message;
        $this->load->view('user/login', $data);
    }

    public function super_admin() {
        $data = array(
            'email_message' => '',
            'password_message' => ''
        );

        if (request_is_post()) {
            $response = $this->user_auth_lib->loginAdmin(request_post_data());

            switch ((int) $response['access_level']) {
                case USER_TYPE_SUPER_ADMIN:
                    redirect('admin/dashboard');
                    break;
                case USER_TYPE_AGENT:
                    redirect('agent/dashboard');
                    break;
                case USER_TYPE_STUDENT:
                    redirect('student/dashboard');
                    break;
                default:
                    break;
            }
        }

        $this->load->view('user/login', $data);
    }

    public function forgot_password() {
        echo 'Coming soon...';
    }

    public function logout() {
        $this->user_auth_lib->logout();
        redirect(site_url('/login'));
    }

    public function verify_email($user_id, $key, $ac = null) {
        $userInfo = $this->basic_model->fetch_all_records(TBL_USERS, ['user_id' => $user_id]);

        if (!$userInfo) {
            show_404('page', false);
        }

        $db_key = sha1($userInfo[0]->username . $user_id);

        $keyValid = FALSE;
        if ($db_key == $key) {
            $keyValid = TRUE;
            $user = $userInfo;
        }

        if (!$keyValid) {
            show_404('page', false);
        }

        $login_url = site_url('login');
        $login_title = 'Reset Password';

        $success_message = '';
        $error_message = '';

        if (request_is_post()) {

            $new_password = $this->input->post("password", TRUE);
            $confirm_password = $this->input->post("confirm_password", TRUE);

            if (strlen(trim($new_password)) < 6) {
                $error_message = 'Enter valid password (minimum 6 chars).';
            } elseif ($new_password != $confirm_password) {
                $error_message = 'Invalid confirmation.';
            } else {

                $data_ = [ 'password' => $this->user_auth_lib->encrypt($new_password)];

                if ($ac == 'acc') {
                    $data_['status'] = USER_STATUS_ACTIVE;
                }


                $this->u_model->save(
                    $data_, array(
                    'user_id' => $user_id
                    )
                );

                redirect(site_url('login'));

                $success_message = 'Password changed successfully.';
            }
        }

        $bdata = array(
            'login_url' => $login_url,
            'login_title' => $login_title,
            'error_message' => $error_message,
            'success_message' => $success_message,
            'message' => $error_message
        );

        $this->load->view("user/reset_password", $bdata);

        return true;
    }
    
    public function edit_profile(){
        echo 'Coming soon';
    }

}
