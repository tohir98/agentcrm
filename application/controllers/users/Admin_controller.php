<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of admin_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property Agent_model $agent_model Description
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('basic_model', 'basic_model');
        $this->load->model('setup/agent_model', 'agent_model');
    }

    public function dashboard() {
        $this->user_auth_lib->check_login();

        $data = array(
            'rep_countries' => $this->basic_model->fetch_all_records(TBL_REP_COUNTRIES, ['status' => 1]), 
            'rep_institutions' => $this->basic_model->fetch_all_records(TBL_REP_INSTITUTION, ['status' => 1]), 
            'branch_offices' => $this->basic_model->fetch_all_records(TBL_BRANCHES, ['status' => 1]),
            'agents' => $this->agent_model->fetchAgents($this->user_auth_lib->get('user_id'))
        );
        $this->user_nav_lib->run_page('dashboard/dashboard', $data, 'Dashboard | Agent CRM');
    }

}
