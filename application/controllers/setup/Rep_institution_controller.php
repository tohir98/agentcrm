<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of rep_country Controller
 *
 * @author TOHIR
 * 
 * @property Rep_country_model $rc_model Description
 * @property Basic_model $basic_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 * @property Rep_institution_model $ri_model Description
 */
class Rep_institution_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/rep_country_model', 'rc_model');
        $this->load->model('setup/rep_institution_model', 'ri_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function add() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->ri_model->institutionExist(request_post_data()['institution_name'])) {
                notify('error', 'Institution name has already been added');
                redirect(site_url('rep_institutions/add'));
            }

            if ($this->ri_model->saveRepInstitution(request_post_data(), $_FILES['userfile'])) {
                notify('success', 'Representing Institution was added successfully');
                redirect(site_url('rep_institutions/view'));
            } else {
                notify('error', 'Unable to add Representing Institution at moment, pls try again');
            }
            exit;
        }
        $data = array(
            'countries' => $this->ri_model->getRepCountries(),
            'page_title' => 'Add Representing Institution',
            'mode' => 'add'
        );
        $this->user_nav_lib->run_page('rep_institution/add', $data, 'Representing Institution | Agent CRM');
    }

    public function view() {
        $this->user_auth_lib->check_login();
        $data = array(
            'institutions' => $this->ri_model->fetchRepresentingInstitutions()
        );
        $this->user_nav_lib->run_page('rep_institution/view', $data, 'Representing Institutions | ' . BUSINESS_NAME);
    }

    public function toggleStatus($institution_id) {
        $this->user_auth_lib->check_login();
        if ($this->ri_model->toggleStatus($institution_id)) {
            notify('success', 'Status change was successful.');
        } else {
            notify('error', 'Status change could not be completed. Pls try again');
        }

        redirect(site_url('/rep_institutions/view'));
    }

    public function edit($institution_id) {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()){
            if ($this->ri_model->updateInstitution($institution_id, request_post_data())){
                notify('success', 'Representing Institution info updated successfully.');
            }else{
                notify('error', 'Error! Information Update could not be completed. Pls try again');
            }
            redirect(site_url('/rep_institutions/view'));
        }
        
        $data = array(
            'institution_details' => $this->basic_model->fetch_all_records(TBL_REP_INSTITUTION, ['rep_institution_id' => $institution_id])[0],
            'countries' => $this->ri_model->getRepCountries(),
            'page_title' => 'Edit Representing Institution',
            'mode' => 'edit'
        );

        $this->user_nav_lib->run_page('rep_institution/add', $data, 'Representing Institution | ' . BUSINESS_NAME);
    }

    public function viewInstitution($institution_id) {
        $this->user_auth_lib->check_login();
        $data = array(
            'institution_details' => $this->basic_model->fetch_all_records(TBL_REP_INSTITUTION, ['rep_institution_id' => $institution_id])[0],
            'countries' => $this->ri_model->getRepCountries(),
            'page_title' => 'View Representing Institution',
            'mode' => 'view'
        );

        $this->user_nav_lib->run_page('rep_institution/add', $data, 'Representing Institution | ' . BUSINESS_NAME);
    }

}
