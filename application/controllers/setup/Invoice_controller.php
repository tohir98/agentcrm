<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Invoice_controller
 *
 * @author TOHIR
 * 
 * @property User_auth_lib $user_auth_lib Description
 * @property User_nav_lib $user_nav_lib Description
 */
class Invoice_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('basic_model', 'basic_model');
    }

    public function invoices() {
        $this->user_auth_lib->check_login();
        $data = array();
        $this->user_nav_lib->run_page('invoices/list', $data, 'Invoices ! ' . BUSINESS_NAME);
    }

}
