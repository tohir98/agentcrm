<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of setup_controller
 *
 * @author TOHIR
 * 
 * @property agent_model $a_model Description
 * @property Basic_model $basic_model Description
 * @property mailer $mailer Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Agent_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/agent_model', 'a_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function agents() {
        $this->user_auth_lib->check_login();
        if ($this->user_auth_lib->is_super_admin()) {
            $agents = $this->a_model->fetchAgents();
        } else {
            $agents = $this->a_model->fetchAgents($this->user_auth_lib->get('user_id'));
        }
        $data = [
            'agents' => $agents
        ];
        $this->user_nav_lib->run_page('agent/list', $data, 'Agents | Agent CRM');
    }

    public function add_agent() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->a_model->saveAgent(request_post_data())) {
                notify('success', 'Agent created successfully', site_url('setup/agents'));
            } else {
                notify('error', 'Unable to create agent, Pls try again', site_url('setup/agent/add_agent'));
            }
        }

        $data = array(
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES)
        );
        $this->user_nav_lib->run_page('agent/add', $data, 'Add Agent | Agent CRM');
    }

    public function delete($agent_id) {
        $redirectUrl = site_url('setup/agents');
        if (!is_numeric($agent_id) || !$agent_id || $agent_id === '') {
            notify('error', 'Invalid parameter passed for Agent ID', $redirectUrl);
        }

        if ($this->a_model->deleteAgent($agent_id)) {
            notify('success', 'Agent deleted successfully', $redirectUrl);
        } else {
            notify('error', 'An error occured while deleting Agent', $redirectUrl);
        }
    }

    public function edit($agent_id) {
        $redirectUrl = site_url('setup/agents');
        if (request_is_post()) {

            if ($this->a_model->updateAgent($agent_id, request_post_data())) {
                notify('success', 'Agent Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update agent information. Pls, try again.', $redirectUrl);
            }
        }

        if (!is_numeric($agent_id) || !$agent_id || $agent_id === '') {
            notify('error', 'Invalid parameter passed for Agent ID', $redirectUrl);
        }

        $data = array(
            'countries' => $this->basic_model->fetch_all_records(TBL_COUNTRIES),
            'agent_details' => $this->a_model->getAgentById($agent_id)
        );

        $this->load->view('agent/edit', $data);
    }

    public function test_mail() {
        $mail_data = array(
            'first_name' => 'Tohir Omoloye',
            'company_name' => 'OAAStudy'
        );

        $msg = $this->load->view('email_templates/test', $mail_data, true);

        $this->mailer
            ->sendMessage('Hello', $msg, 'otcleantech@gmail.com');
    }

}
