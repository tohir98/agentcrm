<?php

/**
 * Description of Fee_setup_controller
 *
 * @author TOHIR
 * 
 * @property setup_model $setup_model Description
 * @property Basic_model $basic_model Description
 * @property mailer $mailer Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Setup_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/setup_model', 'setup_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function processing_fee() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addFee(request_post_data())) {
                notify('success', 'Fee added successfully');
            } else {
                notify('error', 'Unable to add fee at moment, Pls try again');
            }
        }

        $data = [
            'fees' => $this->basic_model->fetch_all_records(TBL_PROCESSING_FEE)
        ];
        $this->user_nav_lib->run_page('setup/processing_fee', $data, 'Processing Fee | ' . BUSINESS_NAME);
    }

    public function comm_receivable() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addCommission('_receivable', request_post_data())) {
                notify('success', 'Commission added successfully');
            } else {
                notify('error', 'Unable to add commission at moment, Pls try again');
            }
        }

        $data = [
            'commissions' => $this->basic_model->fetch_all_records(TBL_COMM_RECEIVABLE)
        ];
        $this->user_nav_lib->run_page('setup/comm_receivables', $data, 'Commission Receivables | ' . BUSINESS_NAME);
    }

    public function tution_fees() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addTuition(request_post_data())) {
                notify('success', 'Tuition added successfully');
            } else {
                notify('error', 'Unable to add tuition at moment, Pls try again');
            }
        }

        $data = [
            'fees' => $this->basic_model->fetch_all_records(TBL_TUITION)
        ];
        $this->user_nav_lib->run_page('setup/tution_fees', $data, 'Tuition Fees | ' . BUSINESS_NAME);
    }

    public function visa_docs() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addVisaDoc(request_post_data())) {
                notify('success', 'Visa added successfully');
            } else {
                notify('error', 'Unable to add visa at moment, Pls try again');
            }
        }

        $data = [
            'visa_docs' => $this->basic_model->fetch_all_records(TBL_VISA_DOCS)
        ];
        $this->user_nav_lib->run_page('setup/visa_docs', $data, 'Visa Documents | ' . BUSINESS_NAME);
    }

    public function delete_visa_docs($visa_doc_id) {

        if ($this->basic_model->delete(TBL_VISA_DOCS, ['visa_doc_id' => $visa_doc_id])) {
            notify('success', 'Visa document deleted successfully');
        } else {
            notify('error', 'An error occured while deleting visa document');
        }

        redirect(site_url('f_setup/visa_docs'));
    }

    public function comm_payable() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addCommission('_payable', request_post_data())) {
                notify('success', 'Commission added successfully');
            } else {
                notify('error', 'Unable to add commission at moment, Pls try again');
            }
        }

        $data = [
            'commission_payable' => $this->basic_model->fetch_all_records(TBL_COMM_PAYABLE)
        ];
        $this->user_nav_lib->run_page('setup/comm_payable', $data, 'Commission Payable | ' . BUSINESS_NAME);
    }

    public function delete_comm_payables($comm_payable_id) {

        if ($this->basic_model->delete(TBL_COMM_PAYABLE, ['commission_payable_id' => $comm_payable_id])) {
            notify('success', 'Commission deleted successfully');
        } else {
            notify('error', 'An error occured while deleting commission');
        }

        redirect(site_url('f_setup/comm_payable'));
    }

    public function delete_comm_receivables($comm_receivable_id) {

        if ($this->basic_model->delete(TBL_COMM_RECEIVABLE, ['commission_receivable_id' => $comm_receivable_id])) {
            notify('success', 'Commission deleted successfully');
        } else {
            notify('error', 'An error occured while deleting commission');
        }

        redirect(site_url('f_setup/comm_receivable'));
    }

    public function delete_fee($processing_fee_id) {

        if ($this->basic_model->delete(TBL_PROCESSING_FEE, ['processing_fee_id' => $processing_fee_id])) {
            notify('success', 'Fee deleted successfully');
        } else {
            notify('error', 'An error occured while deleting fee');
        }

        redirect(site_url('f_setup/processing_fee'));
    }

    public function delete_tution_fee($tution_fee_id) {

        if ($this->basic_model->delete(TBL_TUITION, ['tution_fee_id' => $tution_fee_id])) {
            notify('success', 'Fee deleted successfully');
        } else {
            notify('error', 'An error occured while deleting fee');
        }

        redirect(site_url('f_setup/tution_fees'));
    }

    public function edit_comm_payable($id) {
        $redirect_url = site_url('/f_setup/comm_payable');
        if (!is_numeric($id) || $id == '') {
            notify('error', 'Invalid parameter passed for Ccommission ID', $redirect_url);
        }

        if (request_is_post()) {
            $data_db = array(
                'commission_payable' => request_post_data()['commission'],
                'description' => request_post_data()['description'],
                'amount' => request_post_data()['amount'],
            );
            if ($this->basic_model->update(TBL_COMM_PAYABLE, $data_db, ['commission_payable_id' => $id])) {
                notify('success', 'Commission updated successfully');
            } else {
                notify('error', 'Unable to update commission payable, pls try again');
            }

            redirect(site_url('/f_setup/comm_payable'));
        }


        $data = array(
            'type' => 'payable',
            'comm_details' => $this->basic_model->fetch_all_records(TBL_COMM_PAYABLE, ['commission_payable_id' => $id])[0],
            'form_action' => site_url('/f_setup/edit_comm_payable/' . $id)
        );

        $this->load->view('setup/edit_comm_payable', $data);
    }

    public function edit_comm_receivable($id) {
        $redirect_url = site_url('/f_setup/comm_receivable');
        if (!is_numeric($id) || $id == '') {
            notify('error', 'Invalid parameter passed for Ccommission ID', $redirect_url);
        }

        if (request_is_post()) {
            $data_db = array(
                'commission_receivable' => request_post_data()['commission'],
                'description' => request_post_data()['description'],
                'amount' => request_post_data()['amount'],
            );
            if ($this->basic_model->update(TBL_COMM_RECEIVABLE, $data_db, ['commission_receivable_id' => $id])) {
                notify('success', 'Commission updated successfully');
            } else {
                notify('error', 'Unable to update commission receivable, pls try again');
            }

            redirect(site_url('/f_setup/comm_receivable'));
        }


        $data = array(
            'type' => 'rec',
            'comm_details' => $this->basic_model->fetch_all_records(TBL_COMM_RECEIVABLE, ['commission_receivable_id' => $id])[0],
            'form_action' => site_url('/f_setup/edit_comm_receivable/' . $id)
        );

        $this->load->view('setup/edit_comm_payable', $data);
    }

    public function edit_visa_docs($visa_doc_id) {
        $redirectUrl = site_url('f_setup/visa_docs');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_VISA_DOCS, request_post_data(), ['visa_doc_id' => $visa_doc_id])) {
                notify('success', 'Visa Document Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update visa document. Pls, try again.', $redirectUrl);
            }
        }

        if (!is_numeric($visa_doc_id) || !$visa_doc_id || $visa_doc_id === '') {
            notify('error', 'Invalid parameter passed for Visa Document', $redirectUrl);
        }

        $data = array(
            'visa_details' => $this->basic_model->fetch_all_records(TBL_VISA_DOCS, ['visa_doc_id' => $visa_doc_id])[0],
        );


        $this->load->view('setup/edit_visa_docs', $data);
    }

    public function edit_tution_fee($tution_fee) {
        $redirectUrl = site_url('f_setup/tution_fees');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_TUITION, request_post_data(), ['tution_fee_id' => $tution_fee])) {
                notify('success', 'Tution Fee Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update tution fee. Pls, try again.', $redirectUrl);
            }
        }

        if (!is_numeric($tution_fee) || !$tution_fee || $tution_fee === '') {
            notify('error', 'Invalid parameter passed for Tution fee', $redirectUrl);
        }

        $data = array(
            'tution_details' => $this->basic_model->fetch_all_records(TBL_TUITION, ['tution_fee_id' => $tution_fee])[0],
        );


        $this->load->view('setup/edit_tution_fee', $data);
    }

    public function course_duration() {

        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->setup_model->addCourseDuration(request_post_data())) {
                notify('success', 'Course Duration added successfully');
            } else {
                notify('error', 'Unable to add course duration at moment, Pls try again');
            }
        }

        $data = [
            'course_durations' => $this->basic_model->fetch_all_records(TBL_COURSE_DURATION)
        ];
        $this->user_nav_lib->run_page('setup/course_duration', $data, 'Course Duration | ' . BUSINESS_NAME);
    }

    public function delete_course_duration($course_duration_id) {

        if ($this->basic_model->delete(TBL_COURSE_DURATION, ['course_duration_id' => $course_duration_id])) {
            notify('success', 'Course Duration deleted successfully');
        } else {
            notify('error', 'An error occured while deleting duration');
        }

        redirect(site_url('f_setup/course_duration'));
    }

    public function edit_course_duration($course_duration_id) {
        $redirectUrl = site_url('f_setup/course_duration');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_COURSE_DURATION, request_post_data(), ['course_duration_id' => $course_duration_id])) {
                notify('success', 'Course Duration Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update course duration fee. Pls, try again.', $redirectUrl);
            }
        }

        if (!is_numeric($course_duration_id) || !$course_duration_id || $course_duration_id === '') {
            notify('error', 'Invalid parameter passed for Course duration', $redirectUrl);
        }

        $data = array(
            'course_details' => $this->basic_model->fetch_all_records(TBL_COURSE_DURATION, ['course_duration_id' => $course_duration_id])[0],
        );


        $this->load->view('setup/edit_course_duration', $data);
    }

    public function edit_fee($fee_id) {

        $redirectUrl = site_url('f_setup/processing_fee');

        if (request_is_post()) {
            if ($this->basic_model->update(TBL_PROCESSING_FEE, request_post_data(), ['processing_fee_id' => $fee_id])) {
                notify('success', 'Processing Fee Updated Successfully', $redirectUrl);
            } else {
                notify('error', 'Unable to update processing fee. Pls, try again.', $redirectUrl);
            }
        }

        $data = array(
            'fee_details' => $this->basic_model->fetch_all_records(TBL_PROCESSING_FEE, ['processing_fee_id' => $fee_id])[0],
        );


        $this->load->view('setup/edit_processing_fee', $data);
    }

}
