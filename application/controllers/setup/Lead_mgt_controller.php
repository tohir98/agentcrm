<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Lead_mgt
 *
 * @author user
 * @property User_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property lead_model $lead_model Description
 * @property Basic_model $basic_model Description
 */
class Lead_mgt_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library(array(
            'user_nav_lib',
            'mailer',
        ));
        $this->load->helper('user_nav_helper');
        $this->load->model('setup/lead_model', 'lead_model');
        $this->load->model('basic_model', 'basic_model');
    }

    public function leads_source() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {

            if ($this->lead_model->getLeadByName(trim(request_post_data()['lead_source']))) {
                notify('error', 'Lead source already exist, Pls specify another lead source');
            } else {
                if ($this->lead_model->saveLead(request_post_data())) {
                    notify('success', 'Lead source added successfully');
                } else {
                    notify('error', 'Unable to add lead source, Pls try again');
                }
            }
        }

        $data = ['sources' => $this->basic_model->fetch_all_records(TBL_LEAD_SOURCE)];
        $this->user_nav_lib->run_page('lead_mgt/leads_sources', $data, 'Lead Sources');
    }

    public function switchStatus() {
        $this->user_auth_lib->check_login();
        $data = request_post_data();

        $status = $data['state'] === 'true' ? 1 : 0;
        if ($this->lead_model->update_lead_status($status, $data['id'])) {
            echo 'OK';
        } else {
            echo 'error';
        }
    }

    public function edit_lead_source() {
        if (request_is_post()) {
            if ($this->lead_model->update_lead_source(request_post_data())) {
                notify('success', 'Lead source updated successfully');
            } else {
                notify('error', 'Unable to update lead source, Pls try again');
            }

            redirect(site_url('lead_mgt/leads_source'));
        }
    }

}
