<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Applicant_model
 *
 * @author user
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_active_record $db 
 */
class Applicant_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveApplicant($data) {
        $ref_id = strtoupper(random_string('alnum', 10));
        $data['dateof_birth'] = date('Y-m-d', strtotime($data['dateof_birth']));
        $data['date_created'] = date('Y-m-d h:i:s');
        $data['agent_id'] = $this->user_auth_lib->get('user_id');
        $data['ref_id'] = $ref_id;

        $this->db->insert(TBL_APPLICANTS, $data);
        return $this->db->insert_id() > 0 ? $ref_id : FALSE;
    }

    public function saveApplicantContact($ref_id, $data) {
        $applicant_id = $this->getApplicantByRef($ref_id);

        $data['applicant_id'] = $this->getApplicantByRef($ref_id);
        if ($this->db->get_where(TBL_APPLICANT_CONTACTS, ['applicant_id' => $applicant_id])->result()) {
            $data['date_updated'] = date('Y-m-d h:i:s');
            $this->db->where('applicant_id', $applicant_id)
                ->update(TBL_APPLICANT_CONTACTS, $data);
        } else {
            $data['date_created'] = date('Y-m-d h:i:s');
            $this->db->insert(TBL_APPLICANT_CONTACTS, $data);
        }
        return TRUE;
    }

    public function fetchApplicants($agent_id = null, $ref_id = NULL) {
        if ($agent_id) {
            $this->db->where('a.agent_id', $agent_id);
        }
        if ($ref_id) {
            $this->db->where('a.ref_id', $ref_id);
        }

        return $this->db->select('a.*, l.lead_source, g.gender, t.title, c.country, c2.country as nationality, m.marital_status')
                ->from(TBL_APPLICANTS . ' as a')
                ->join(TBL_LEAD_SOURCE . ' as l', 'l.lead_source_id=a.lead_source_id', 'left')
                ->join('titles as t', 't.title_id=a.title_id', 'left')
                ->join('gender as g', 'g.gender_id=a.gender_id', 'left')
                ->join('marital_status as m', 'm.marital_status_id=a.marital_status_id', 'left')
                ->join(TBL_COUNTRIES . ' as c', 'c.country_id=a.country_id', 'left')
                ->join(TBL_COUNTRIES . ' as c2', 'c2.country_id=a.nationality_id', 'left')
                ->get()
                ->result();
    }

    public function getApplicantByRef($ref_id) {
        return $this->db->select('applicant_id')
                ->from(TBL_APPLICANTS)
                ->where('ref_id', $ref_id)
                ->get()->row()->applicant_id;
    }

    public function fetchApplicantVisaInfo($applicant_id) {
        return $this->db->select('v.*, t.visa_type')
                ->from(TBL_APPLICANT_VISA_INFO . ' as v')
                ->join(TBL_VISA_TYPES . ' t', 'v.visa_type_id=t.visa_type_id')
                ->where('v.applicant_id', $applicant_id)
                ->get()->result();
    }

    public function fetchApplicantVisaDocs($applicant_id) {
        return $this->db->select('vd.*, d.visa_doc')
                ->from(TBL_APPLICANT_VISA_DOCS . ' as vd')
                ->join(TBL_VISA_DOCS . ' d', 'd.visa_doc_id=vd.visa_doc_id')
                ->where('vd.applicant_id', $applicant_id)
                ->get()->result();
    }

    public function saveVisaInfo($ref_id, $data, $files) {
        $app_visa = [];
        if (!empty($files)) {

            for ($c = 0; $c < count($files['name']); ++$c) {

                $ext = end((explode(".", $files['name'][$c])));

                if ($files['size'][$c] > 102400) {
                    continue;
                }

                if (!in_array($ext, ['jpg', 'jpeg', 'gif', 'png', 'pdf'])) {
                    continue;
                }

                $doc_name = md5(microtime()) . '.' . $ext;

                if (!move_uploaded_file($files['tmp_name'][$c], FILE_PATH_VISA_DOC . $doc_name)) {
                    echo 'cant move fiile';
                    continue;
                }


                $app_visa[] = array(
                    'visa_doc_id' => $data['visa_doc_id'][$c],
                    'doc_image_name' => $doc_name,
                    'doc_image_url' => site_url('files/visa_doc/' . $doc_name),
                    'date_added' => date('Y-m-d h:i:s'),
                    'added_by' => $this->user_auth_lib->get('user_id'),
                    'applicant_id' => $this->getApplicantByRef($ref_id)
                );
            }
            !empty($app_visa) ? $this->db->insert_batch('applicant_visa_docs', $app_visa) : '';
        }

        $v_info = ['visa_type_id' => $data['visa_type_id'],
            'expiry_date' => $data['exp_year'] . '-' . $data['exp_month'] . '-' . $data['exp_day'],
            'passport_no' => $data['passport_no'],
            'medical_exam' => $data['medical_exam'],
            'visa_lodge' => $data['visa_lodge'],
            'date_visa_lodge' => date('Y-m-d', strtotime($data['date_visa_lodge'])),
            'coe_issued' => $data['coe_issued'],
            'pic4020' => $data['pic'],
            'date_coe_rec' => date('Y-m-d', strtotime($data['date_coe_rec'])),
            'applicant_id' => $this->getApplicantByRef($ref_id),
            'agent_id' => $this->user_auth_lib->get('user_id')];


        if (!$this->_has_visa_info($this->getApplicantByRef($ref_id))) {
            $v_info['date_added'] = date('Y-m-d h:i:s');
            return $this->db->insert(TBL_APPLICANT_VISA_INFO, $v_info);
        } else {

            $v_info['date_updated'] = date('Y-m-d h:i:s');
            $this->db->where('applicant_id', $this->getApplicantByRef($ref_id))
                ->update(TBL_APPLICANT_VISA_INFO, $v_info);
            return $this->db->affected_rows() > 0;
        }
    }

    private function _has_visa_info($applicant_id) {
        return $this->db->get_where(TBL_APPLICANT_VISA_INFO, ['applicant_id' => $applicant_id])->result();
    }

    private function _has_fee_detail($applicant_id) {
        return $this->db->get_where(TBL_APPLICANT_FEES, ['applicant_id' => $applicant_id])->result();
    }

    public function fetchApplicantCourseSelection($applicant_id) {
        return $this->db->select('cs.*, c.country, ri.institution_name')
                ->from(TBL_APPLICANT_COURSE_SELECTN . ' as cs')
                ->join(TBL_REP_COUNTRIES . ' as rc', 'rc.rep_country_id=cs.rep_country_id')
                ->join(TBL_COUNTRIES . ' as c', 'c.country_id=rc.country_id')
                ->join(TBL_REP_INSTITUTION . ' as ri', 'ri.rep_institution_id=cs.rep_institution_id')
                ->where('cs.applicant_id', $applicant_id)
                ->get()->result();
    }

    public function fetchApplicantFeeDetails($applicant_id) {
        return $this->db->select('f.*, cd.course_duration')
                ->from(TBL_APPLICANT_FEES . ' as f')
                ->join(TBL_COURSE_DURATION . ' as cd', 'cd.course_duration_id=f.course_duration_id')
                ->where('f.applicant_id', $applicant_id)
                ->get()->row();
    }

    public function saveCourseSelection($ref_id, $data) {
        $applicant_id = $this->getApplicantByRef($ref_id);
        if ($this->db->get_where(TBL_APPLICANT_COURSE_SELECTN, ['applicant_id' => $applicant_id])->result()) {
            $this->db->where('applicant_id', $applicant_id)
                ->update(TBL_APPLICANT_COURSE_SELECTN, $data);
        } else {
            $this->db->insert(TBL_APPLICANT_COURSE_SELECTN, array_merge($data, ['applicant_id' => $applicant_id, 'date_created' => date('Y-m-d h:i:s')]));
        }

        return TRUE;
    }

    public function update_personal_details($ref_id, $data) {
        $data['dateof_birth'] = date('Y-m-d', strtotime($data['dateof_birth']));
        $this->db->where('applicant_id', $this->getApplicantByRef($ref_id))
            ->update(TBL_APPLICANTS, $data);

        return $this->db->affected_rows() > 0;
    }

    public function saveFeeDetails($ref_id, $data) {
        $data['added_by'] = $this->user_auth_lib->get('user_id');
        $data['date_created'] = date('Y-m-d h:i:s');
        if ($data['offer_accepted']) {
            $data['date_offer_accepted'] = date('Y-m-d', strtotime($data['date_offer_accepted']));
        }
        $data['applicant_id'] = $this->getApplicantByRef($ref_id);

        if (!$this->_has_fee_detail($this->getApplicantByRef($ref_id))) {
            return $this->db->insert(TBL_APPLICANT_FEES, $data);
        } else {
            $this->db->where('applicant_id', $this->getApplicantByRef($ref_id))
                ->update(TBL_APPLICANT_FEES, $data);
            return $this->db->affected_rows() > 0;
        }
    }

    public function fetchApplicantPicture($ref_id) {
        return $this->db->get_where(TBL_APPLICANT_PICTURE, ['applicant_id' => $this->getApplicantByRef($ref_id)])->result();
    }

    public function update_applicant_picture($applicant_id, $file) {

        if (!empty($file) && $file['name'] !== '') {

            $ext = end((explode(".", $file['name'])));

            $config = array(
                'upload_path' => FILE_PATH_PROFILE_PIC,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "3000",
                'max_width' => "3000",
            );

            $picture_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $picture_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {

                $error = array('error' => $this->upload->display_errors());

                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data_img['profile_picture'] = $picture_name;
            $data_img['applicant_id'] = $applicant_id;
            $data_img['profile_picture_url'] = site_url('/files/profile_pic/' . $picture_name);
            $data_img['created_by'] = $this->user_auth_lib->get('user_id');


            if (!$this->db->get_where(TBL_APPLICANT_PICTURE, ['applicant_id' => $applicant_id])->row()) {
                $data_img['date_created'] = date('Y-m-d h:i:s');
                return $this->db->insert(TBL_APPLICANT_PICTURE, $data_img);
            } else {
                $data_img['date_updated'] = date('Y-m-d h:i:s');
                $this->db->where('applicant_id', $applicant_id)
                    ->update(TBL_APPLICANT_PICTURE, $data_img);
                return $this->db->affected_rows() > 0;
            }
        }
    }

    public function getApplicantProfilePic($applicant_id) {
        return $this->db->get_where(TBL_APPLICANT_PICTURE, ['applicant_id' => $applicant_id])->row();
    }

    public function removeProfilePicture($applicant_id) {
        return $this->db->where('applicant_id', $applicant_id)->delete(TBL_APPLICANT_PICTURE);
    }

}
