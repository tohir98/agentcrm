<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 */
class Basic_model extends CI_Model {

    private $tuition_per;
    private $oshc;
    private $payment_methods;

    public function __construct() {
        parent::__construct();
        $this->load->database();

        $this->tuition_per = [1 => 'Per term', 2 => 'Per semester'];
        $this->oshc = [1 => 'Education provider', 2 => 'Agent'];
        $this->payment_methods = [1 => 'Direct payment', 2 => 'Agent payment'];
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function saveReg($data) {
        if ($this->_recordExist($data['email'])) {
            return FALSE;
        }

//        var_dump($data); exit;

        $data['dateof_birth'] = date('Y-m-d', strtotime($data['dateof_birth']));
        $data['date_added'] = date('Y-m-d h:i:s');

        $this->db->trans_start();
        $user_id = $this->_createStudentUser($data);

        $this->db->insert('student_event_reg', $data);
        $stat = $this->_sendConfirmationEmail($user_id, array('first_name' => $data['first_name'], 'username' => $data['email']));

        $this->db->trans_complete();

        return TRUE;
    }

    private function _createStudentUser($data) {
        $user_data = array(
            'username' => $data['email'],
            'password' => $this->user_auth_lib->encrypt(DEFAULT_PASSWORD),
            'status' => USER_STATUS_INACTIVE,
            'created_at' => date('Y-m-d h:i:s'),
            'user_type' => USER_TYPE_STUDENT,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
        );

        $this->db->insert(TBL_USERS, $user_data);
        return $this->db->insert_id();
    }

    private function _sendConfirmationEmail($user_id, $params) {
        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $params['first_name'],
            'username' => $params['username'],
            'verify_link' => site_url('verify_email/' . $user_id . '/' . $this->user_auth_lib->encrypt($params['username'] . $user_id) . '/acc'),
            'pdf_guide' => site_url('downloads/OAA_STUDY_guide.pdf')
        );

        $msg = $this->load->view('email_templates/student_reg_confirmation', $mail_data, true);

        return $this->mailer
                ->sendMessage('Welcome | OAAStudy', $msg, $params['username']);
    }

    private function _recordExist($email) {
        return $this->db->get_where('student_event_reg', ['email' => $email])->result();
    }

    public function delete($table, $array) {
        return $this->db->where($array)
                ->delete($table);
    }

    public function update($table, $data, $where) {
        return $this->db->where($where)
                ->update($table, $data);
    }

    public function get_tuition_per() {
        return $this->tuition_per;
    }

    public function get_oshc() {
        return $this->oshc;
    }
    
    public function get_paymemt_methods() {
        return $this->payment_methods;
    }
    
    

}
