<?php

/**
 * Description of Setup_model
 *
 * @author TOHIR
 */
class Setup_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addFee($data) {
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['date_created'] = date('Y-m-d h:i:s');
        return $this->db->insert(TBL_PROCESSING_FEE, $data);
    }
    
    public function addTuition($data) {
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $data['date_created'] = date('Y-m-d h:i:s');
        return $this->db->insert(TBL_TUITION, $data);
    }

    public function addCommission($_surfix, $param) {
        $data_db = array(
            'commission' . $_surfix => $param['commission'],
            'description' => $param['description'],
            'amount' => $param['amount'],
            'date_created' => date('Y-m-d h:i:s'),
            'created_by' => $this->user_auth_lib->get('user_id')
        );

        return $this->db->insert('commission' . $_surfix, $data_db);
    }   
    
    public function addVisaDoc($data) {
        $data['added_by'] = $this->user_auth_lib->get('user_id');
        $data['date_added'] = date('Y-m-d h:i:s');
        $data['status'] = 1;
        return $this->db->insert(TBL_VISA_DOCS, $data);
    }
    
    public function addCourseDuration($data) {
        $data['added_by'] = $this->user_auth_lib->get('user_id');
        $data['date_added'] = date('Y-m-d h:i:s');
        $data['date_updated'] = date('Y-m-d h:i:s');
        return $this->db->insert(TBL_COURSE_DURATION, $data);
    }

}
