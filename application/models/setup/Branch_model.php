<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of branch_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 * @property CI_DB_active_record $db 
 * @property CI_Loader $load loader
 */
class Branch_model extends CI_Model {

    private static $subject = "Access Granted | Branch Office";

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveBranch($data) {
        $this->db->trans_start();

        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        $user_data = array(
            'username' => $data['username'],
            'password' => $this->user_auth_lib->encrypt($data['password']),
            'status' => USER_STATUS_INACTIVE,
            'created_at' => date('Y-m-d h:i:s'),
            'user_type' => USER_TYPE_BRANCH,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['contact_phone'],
        );

        $this->db->insert(TBL_USERS, $user_data);
        $user_id = $this->db->insert_id();

        $branch_data = array(
            'branch_name' => ucfirst($data['branch_name']),
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'country_id' => $data['country_id'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'website' => $data['website'],
            'contact_person' => $data['first_name'] . ' ' . $data['last_name'],
            'designation' => $data['designation'],
            'contact_phone' => $data['contact_phone'],
            'skype_id' => $data['skype_id'],
            'user_id' => $user_id,
            'date_created' => date('Y-m-d h:i:s'),
            'created_by' => $this->user_auth_lib->get('user_id')
        );

        $this->db->insert(TBL_BRANCHES, $branch_data);
        $this->_sendConfirmationEmail($user_id, $data);
        $this->db->trans_complete();

        return TRUE;
    }

    private function _sendConfirmationEmail($user_id, $params) {
        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $params['first_name'],
            'username' => $params['username'],
            'password' => $params['password'],
            'verify_link' => site_url('verify_email/' . $user_id . '/' . $this->user_auth_lib->encrypt($params['username'] . $user_id) . '/acc'),
        );

        $msg = $this->load->view('email_templates/branch_office', $mail_data, true);

        return $this->mailer
                ->sendMessage(static::$subject, $msg, $params['username']);
    }

    public function deleteBranch($id) {
        return $this->db->where('branch_id', $id)->delete(TBL_BRANCHES);
    }

    public function fetch_records() {
        return $this->db->select('b.*, u.status c_status')
                ->from(TBL_BRANCHES . ' as b')
                ->join(TBL_USERS . ' as u', 'b.user_id=u.user_id')
                ->get()->result();
    }

    public function fetchBranchReport() {
        $branch_offices = $this->fetch_records();
        if (empty($branch_offices)) {
            return FALSE;
        }

        $result = [];
        foreach ($branch_offices as $branch) {
            $result[] = (object) array_merge((array) $branch, ['app_count' => $this->_fetchBranchApplications($branch->user_id)]);
        }

        return $result;
    }

    private function _fetchBranchApplications($branch_user) {
        $sql = "select COUNT(*) cnt from applicants WHERE agent_id IN (SELECT user_id FROM agents WHERE created_by = {$branch_user})";
        return $this->db->query($sql)->row()->cnt;
    }

    public function updateBranch($branch_id, $data) {
        if (empty($data)) {
            return FALSE;
        }
        
        $data['contact_person'] = $data['first_name'] . ' ' . $data['last_name'];
        unset($data['first_name']);
        unset($data['last_name']);
        unset($data['username']);
        unset($data['password']);
        
        return $this->db->where('branch_id', $branch_id)
                ->update(TBL_BRANCHES, $data);
    }

}
