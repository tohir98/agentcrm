<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lead_model
 *
 * @author user
 * @property User_auth_lib $user_auth_lib Description
 */
class Lead_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchLead() {
        return $this->db->get(TBL_LEAD_SOURCE)->result();
    }

    public function saveLead($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        $lead_data = array(
            'lead_source' => trim($data['lead_source']),
            'status' => 1,
            'created_at' => date('Y-m-d h:i:s'),
            'created_by' => $this->user_auth_lib->get('user_id'),
        );

        return $this->db->insert(TBL_LEAD_SOURCE, $lead_data);
    }

    public function getLeadById($lead_id) {
        return $this->db->get_where(TBL_LEAD_SOURCE, ['lead_source_id' => $lead_id])->row();
    }

    public function getLeadByName($lead_source) {
        return $this->db->get_where(TBL_LEAD_SOURCE, ['lead_source' => $lead_source])->row();
    }

    public function updateAgent($id, $data) {
        return $this->db->where('lead_source', $id)
                ->update(TBL_LEAD_SOURCE, array_merge($data, array(
                    'date_updated' => date('Y-m-d h:i:s')
                        )
                    )
        );
    }

    public function fetch_records() {
        return $this->db->select('b.*')
                ->from(TBL_BRANCHES . ' as b')
                ->join(TBL_USERS . ' as u', 'b.user_id=u.user_id')
                ->get()->result();
    }

    public function update_lead_status($status, $lead_source_id) {
        return $this->db->where('lead_source_id', $lead_source_id)
                ->update(TBL_LEAD_SOURCE, ['status' => $status]);
    }

    public function update_lead_source($data) {
        $this->db->where('lead_source_id', $data['lead_source_id'])
            ->update(TBL_LEAD_SOURCE, ['lead_source' => $data['lead_source']]);

        return $this->db->affected_rows() > 0;
    }

}
