<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'users/users_controller/login';
//$route['default_controller'] = 'users/Users_controller';
$route['login']  = 'users/users_controller/login';
$route['logout']  = 'users/users_controller/logout';
$route['verify_email']  = 'users/users_controller/verify_email';
$route['verify_email/(:num)']  = 'users/users_controller/verify_email/$1';
$route['verify_email/(:num)/(:any)']  = 'users/users_controller/verify_email/$1/$2';
$route['verify_email/(:num)/(:any)/(:any)']  = 'users/users_controller/verify_email/$1/$2/$3';
$route['admin']  = 'users/users_controller/super_admin';
$route['admin/dashboard']  = 'users/admin_controller/dashboard';
$route['account/dashboard']  = 'users/admin_controller/dashboard';
$route['account/edit_profile']  = 'users/users_controller/edit_profile';

$route['forgot_password']  = 'users/users_controller/forgot_password';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['setup/add_agent']  = 'setup/agent_controller/add_agent';
$route['setup/agents']  = 'setup/agent_controller/agents';
$route['setup/agent']  = 'setup/agent_controller';
$route['setup/agent/(:any)']  = 'setup/agent_controller/$1';
$route['setup/agent/(:any)/(:any)']  = 'setup/agent_controller/$1/$2';
$route['setup/agent/(:any)/(:any)/(:any)']  = 'setup/agent_controller/$1/$2/$3';

$route['f_setup']  = 'setup/setup_controller';
$route['f_setup/(:any)']  = 'setup/setup_controller/$1';
$route['f_setup/(:any)/(:any)']  = 'setup/setup_controller/$1/$2';

// Branch offices
$route['brach_office']  = 'branch/branch_controller';
$route['brach_office/(:any)']  = 'branch/branch_controller/$1';
$route['brach_office/(:any)/(:any)']  = 'branch/branch_controller/$1/$2';

// Representing Countries
$route['rep_countries']  = 'setup/rep_country_controller';
$route['rep_countries/(:any)']  = 'setup/rep_country_controller/$1';
$route['rep_countries/(:any)/(:any)']  = 'setup/rep_country_controller/$1/$2';

// Representing Institutions
$route['rep_institutions']  = 'setup/Rep_institution_controller';
$route['rep_institutions/(:any)']  = 'setup/Rep_institution_controller/$1';
$route['rep_institutions/(:any)/(:any)']  = 'setup/Rep_institution_controller/$1/$2';

// Lead source
$route['lead_mgt']  = 'setup/Lead_mgt_controller';
$route['lead_mgt/(:any)']  = 'setup/Lead_mgt_controller/$1';
$route['lead_mgt/(:any)/(:any)']  = 'setup/Lead_mgt_controller/$1/$2';

// Lead source
$route['applications']  = 'application/Application_controller';
$route['applications/(:any)']  = 'application/Application_controller/$1';
$route['applications/(:any)/(:any)']  = 'application/Application_controller/$1/$2';
$route['applications/(:any)/(:any)/(:any)']  = 'application/Application_controller/$1/$2/$3';

// Reports
$route['reports']  = 'reports/reports_controller';
$route['reports/(:any)']  = 'reports/reports_controller/$1';
$route['reports/(:any)/(:any)']  = 'reports/reports_controller/$1/$2';

// Student Reg
$route['student_reg']  = 'welcome/student_reg';

// Invoices
$route['invoice']  = 'setup/invoice_controller';
$route['invoice/(:any)']  = 'setup/invoice_controller/$1';
$route['invoice/(:any)/(:any)']  = 'setup/invoice_controller/$1/$2';