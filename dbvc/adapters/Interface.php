<?php

interface DBV_Adapter_Interface
{

    /**
     * Connects to the database
     * @throws DBV_Exception
     */
    public function connect(
        $host = false, $port = false, $username = false, $password = false, $database_name = false
    );

    /**
     * Runs an SQL query
     * @throws DBV_Exception
     */
    public function query($sql);
    
}