-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2015 at 12:12 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE IF NOT EXISTS `agents` (
  `agent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `download_csv` int(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `processing_office` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`agent_id`),
  KEY `FK_agents_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`agent_id`, `first_name`, `last_name`, `phone`, `download_csv`, `email`, `created_at`, `created_by`, `processing_office`, `status`, `user_id`, `date_updated`) VALUES
(1, 'yusuf', 'Opeyemi', '05211144', 1, NULL, '2015-11-17 10:27:45', 4, 0, 0, 19, NULL),
(2, 'Raimot', 'Muritala', '08055885588', 1, NULL, '2015-11-22 11:05:16', 20, 0, 0, 21, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `applicant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `middle_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `gender_id` int(10) unsigned DEFAULT NULL,
  `dateof_birth` datetime DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `agent_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `lead_source_id` int(10) unsigned DEFAULT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `ref_id` varchar(10) DEFAULT NULL,
  `marital_status_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`applicant_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`applicant_id`, `title_id`, `first_name`, `middle_name`, `last_name`, `gender_id`, `dateof_birth`, `country_id`, `email`, `agent_id`, `date_created`, `date_updated`, `lead_source_id`, `nationality_id`, `ref_id`, `marital_status_id`) VALUES
(2, 1, 'sade', 'salami', 'adeagbo', 1, '1992-04-15 00:00:00', 14, 'ade@mm.mn', 19, '2015-11-12 05:50:35', NULL, 2, 15, 'JMB26Z8LUO', 2),
(3, 2, 'na', 'na', 'na', 2, '1970-01-01 00:00:00', 17, 'ingo@aol.com', 4, '2015-11-14 02:08:38', NULL, 2, 18, 'HP5TKJXSLO', NULL),
(4, 1, 'Sadiq', 'Issa', 'Usman', 1, '2015-11-26 00:00:00', 18, 'issa.usman@test.com', 21, '2015-11-22 11:14:17', NULL, 1, 18, 'HYKMXB4Q9C', NULL),
(5, 2, 'Taye', 'O', 'Taiwo', 2, '2015-10-26 00:00:00', 12, 'ttaiwo@she.net', 21, '2015-11-23 09:32:19', NULL, 2, 18, 'LSRAUDWQGO', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_contacts`
--

CREATE TABLE IF NOT EXISTS `applicant_contacts` (
  `applicant_contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_1` varchar(150) DEFAULT NULL,
  `address_2` varchar(150) DEFAULT NULL,
  `suburb` varchar(150) DEFAULT NULL,
  `state_id` varchar(150) DEFAULT NULL,
  `postcode_id` int(10) unsigned DEFAULT NULL,
  `telephone` decimal(10,0) DEFAULT NULL,
  `mobile` decimal(10,0) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `lead_source_id` int(10) unsigned DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`applicant_contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `applicant_contacts`
--

INSERT INTO `applicant_contacts` (`applicant_contact_id`, `address_1`, `address_2`, `suburb`, `state_id`, `postcode_id`, `telephone`, `mobile`, `date_created`, `applicant_id`, `date_updated`, `lead_source_id`, `country_id`) VALUES
(1, '10 Bola Shadipe Street, Off Adelabu, Surulere', '', 'Surulere', 'Yaba left', 23401, '9999999999', '9999999999', '2015-11-14 03:11:56', 4, '2015-11-22 11:21:18', NULL, 0),
(2, 'Ikotun ', 'Sab', 'Yaba', 'Lagos', 23401, '902233665', '902233665', '2015-11-18 08:56:55', 2, '2015-11-23 11:40:24', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_course_selection`
--

CREATE TABLE IF NOT EXISTS `applicant_course_selection` (
  `applicant_course_selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_country_id` int(10) unsigned DEFAULT NULL,
  `rep_institution_id` int(10) unsigned DEFAULT NULL,
  `propose_course` varchar(150) DEFAULT NULL,
  `intake_month_id` int(10) unsigned DEFAULT NULL,
  `intake_year` int(10) unsigned DEFAULT NULL,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`applicant_course_selection_id`),
  KEY `FK_applicant_course_selection_rep_country` (`rep_country_id`),
  KEY `FK_applicant_course_selection_rep_institution` (`rep_institution_id`),
  KEY `FK_applicant_course_selection_applicant` (`applicant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `applicant_course_selection`
--

INSERT INTO `applicant_course_selection` (`applicant_course_selection_id`, `rep_country_id`, `rep_institution_id`, `propose_course`, `intake_month_id`, `intake_year`, `applicant_id`, `date_created`) VALUES
(1, 1, 2, 'na', 7, 2017, 2, '2015-11-21 12:21:36'),
(2, 1, 2, 'lkjfjjfhdkjf', 10, 2018, 4, '2015-11-22 11:22:03');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_visa_docs`
--

CREATE TABLE IF NOT EXISTS `applicant_visa_docs` (
  `applicant_visa_doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visa_doc_id` varchar(150) DEFAULT NULL,
  `doc_image_name` varchar(150) DEFAULT NULL,
  `doc_image_url` varchar(150) DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `applicant_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`applicant_visa_doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_visa_info`
--

CREATE TABLE IF NOT EXISTS `applicant_visa_info` (
  `applicant_visa_info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visa_type_id` int(10) unsigned DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `passport_no` varchar(45) DEFAULT NULL,
  `health_cover` tinyint(3) unsigned DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `agent_id` int(10) unsigned DEFAULT NULL,
  `ref_id` varchar(45) DEFAULT NULL,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_visa_info_id`),
  KEY `FK_applicant_visa_info_applicant_id` (`applicant_id`),
  KEY `FK_applicant_visa_info_agent_id` (`agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `applicant_visa_info`
--

INSERT INTO `applicant_visa_info` (`applicant_visa_info_id`, `visa_type_id`, `expiry_date`, `passport_no`, `health_cover`, `date_added`, `agent_id`, `ref_id`, `applicant_id`) VALUES
(4, 2, '2017-05-15', '1222200021', NULL, '2015-11-19 09:34:00', 19, NULL, 2),
(5, 3, '2017-05-18', '0048746734', NULL, '2015-11-22 11:21:38', 21, NULL, 4),
(6, 6, '2018-09-20', '122888888', NULL, '2015-11-23 11:40:54', 19, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(150) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(150) DEFAULT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `skype_id` varchar(45) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `FK_branches_user_id` (`user_id`),
  KEY `FK_branches_created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `address`, `city`, `state`, `country_id`, `email`, `phone`, `website`, `contact_person`, `designation`, `contact_phone`, `skype_id`, `user_id`, `status`, `date_created`, `created_by`) VALUES
(1, 'Ilesa Branch', 'okesa str', 'Ilesa', 'Osun', 154, 'ilesa@oaa.com', '0809999999', 'na', 'Adekunlw Ajasin', 'NA', '09088334477', 'tunde.phil', 20, NULL, '2015-11-22 11:02:21', 4);

-- --------------------------------------------------------

--
-- Table structure for table `commission_payable`
--

CREATE TABLE IF NOT EXISTS `commission_payable` (
  `commission_payable_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commission_payable` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT '0',
  `percentage` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT '1',
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`commission_payable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `commission_payable`
--

INSERT INTO `commission_payable` (`commission_payable_id`, `commission_payable`, `amount`, `percentage`, `date_created`, `created_by`, `status`, `description`) VALUES
(1, 'OSHC', 45220, NULL, '2015-12-11 06:37:13', 4, 1, 'OSHC');

-- --------------------------------------------------------

--
-- Table structure for table `commission_receivable`
--

CREATE TABLE IF NOT EXISTS `commission_receivable` (
  `commission_receivable_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commission_receivable` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT '0',
  `percentage` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT '1',
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`commission_receivable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `flag` varchar(6) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country`, `iso_alpha2`, `iso_alpha3`, `iso_numeric`, `currency_code`, `currency_name`, `currrency_symbol`, `flag`, `code`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 4, 'AFN', 'Afghani', '؋', 'AF.png', 93),
(2, 'Albania', 'AL', 'ALB', 8, 'ALL', 'Lek', 'Lek', 'AL.png', 355),
(3, 'Algeria', 'DZ', 'DZA', 12, 'DZD', 'Dinar', NULL, 'DZ.png', 213),
(4, 'American Samoa', 'AS', 'ASM', 16, 'USD', 'Dollar', '$', 'AS.png', NULL),
(5, 'Andorra', 'AD', 'AND', 20, 'EUR', 'Euro', '€', 'AD.png', NULL),
(6, 'Angola', 'AO', 'AGO', 24, 'AOA', 'Kwanza', 'Kz', 'AO.png', 244),
(7, 'Anguilla', 'AI', 'AIA', 660, 'XCD', 'Dollar', '$', 'AI.png', NULL),
(8, 'Antarctica', 'AQ', 'ATA', 10, '', '', NULL, 'AQ.png', NULL),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 28, 'XCD', 'Dollar', '$', 'AG.png', 1268),
(10, 'Argentina', 'AR', 'ARG', 32, 'ARS', 'Peso', '$', 'AR.png', 54),
(11, 'Armenia', 'AM', 'ARM', 51, 'AMD', 'Dram', NULL, 'AM.png', 374),
(12, 'Aruba', 'AW', 'ABW', 533, 'AWG', 'Guilder', 'ƒ', 'AW.png', NULL),
(13, 'Australia', 'AU', 'AUS', 36, 'AUD', 'Dollar', '$', 'AU.png', NULL),
(14, 'Austria', 'AT', 'AUT', 40, 'EUR', 'Euro', '€', 'AT.png', 43),
(15, 'Azerbaijan', 'AZ', 'AZE', 31, 'AZN', 'Manat', 'ман', 'AZ.png', 994),
(16, 'Bahamas', 'BS', 'BHS', 44, 'BSD', 'Dollar', '$', 'BS.png', NULL),
(17, 'Bahrain', 'BH', 'BHR', 48, 'BHD', 'Dinar', NULL, 'BH.png', 973),
(18, 'Bangladesh', 'BD', 'BGD', 50, 'BDT', 'Taka', NULL, 'BD.png', 880),
(19, 'Barbados', 'BB', 'BRB', 52, 'BBD', 'Dollar', '$', 'BB.png', NULL),
(20, 'Belarus', 'BY', 'BLR', 112, 'BYR', 'Ruble', 'p.', 'BY.png', 1246),
(21, 'Belgium', 'BE', 'BEL', 56, 'EUR', 'Euro', '€', 'BE.png', 32),
(22, 'Belize', 'BZ', 'BLZ', 84, 'BZD', 'Dollar', 'BZ$', 'BZ.png', 501),
(23, 'Benin', 'BJ', 'BEN', 204, 'XOF', 'Franc', NULL, 'BJ.png', 229),
(24, 'Bermuda', 'BM', 'BMU', 60, 'BMD', 'Dollar', '$', 'BM.png', NULL),
(25, 'Bhutan', 'BT', 'BTN', 64, 'BTN', 'Ngultrum', NULL, 'BT.png', 975),
(26, 'Bolivia', 'BO', 'BOL', 68, 'BOB', 'Boliviano', '$b', 'BO.png', 591),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', 70, 'BAM', 'Marka', 'KM', 'BA.png', 387),
(28, 'Botswana', 'BW', 'BWA', 72, 'BWP', 'Pula', 'P', 'BW.png', 267),
(29, 'Bouvet Island', 'BV', 'BVT', 74, 'NOK', 'Krone', 'kr', 'BV.png', NULL),
(30, 'Brazil', 'BR', 'BRA', 76, 'BRL', 'Real', 'R$', 'BR.png', 55),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 86, 'USD', 'Dollar', '$', 'IO.png', NULL),
(32, 'British Virgin Islands', 'VG', 'VGB', 92, 'USD', 'Dollar', '$', 'VG.png', NULL),
(33, 'Brunei', 'BN', 'BRN', 96, 'BND', 'Dollar', '$', 'BN.png', NULL),
(34, 'Bulgaria', 'BG', 'BGR', 100, 'BGN', 'Lev', 'лв', 'BG.png', 359),
(35, 'Burkina Faso', 'BF', 'BFA', 854, 'XOF', 'Franc', NULL, 'BF.png', 226),
(36, 'Burundi', 'BI', 'BDI', 108, 'BIF', 'Franc', NULL, 'BI.png', 257),
(37, 'Cambodia', 'KH', 'KHM', 116, 'KHR', 'Riels', '៛', 'KH.png', 855),
(38, 'Cameroon', 'CM', 'CMR', 120, 'XAF', 'Franc', 'FCF', 'CM.png', 237),
(39, 'Canada', 'CA', 'CAN', 124, 'CAD', 'Dollar', '$', 'CA.png', 1),
(40, 'Cape Verde', 'CV', 'CPV', 132, 'CVE', 'Escudo', NULL, 'CV.png', 238),
(41, 'Cayman Islands', 'KY', 'CYM', 136, 'KYD', 'Dollar', '$', 'KY.png', NULL),
(42, 'Central African Republic', 'CF', 'CAF', 140, 'XAF', 'Franc', 'FCF', 'CF.png', 236),
(43, 'Chad', 'TD', 'TCD', 148, 'XAF', 'Franc', NULL, 'TD.png', 235),
(44, 'Chile', 'CL', 'CHL', 152, 'CLP', 'Peso', NULL, 'CL.png', 56),
(45, 'China', 'CN', 'CHN', 156, 'CNY', 'Yuan Renminbi', '¥', 'CN.png', 86),
(46, 'Christmas Island', 'CX', 'CXR', 162, 'AUD', 'Dollar', '$', 'CX.png', NULL),
(47, 'Cocos Islands', 'CC', 'CCK', 166, 'AUD', 'Dollar', '$', 'CC.png', NULL),
(48, 'Colombia', 'CO', 'COL', 170, 'COP', 'Peso', '$', 'CO.png', 57),
(49, 'Comoros', 'KM', 'COM', 174, 'KMF', 'Franc', NULL, 'KM.png', 269),
(50, 'Cook Islands', 'CK', 'COK', 184, 'NZD', 'Dollar', '$', 'CK.png', NULL),
(51, 'Costa Rica', 'CR', 'CRI', 188, 'CRC', 'Colon', '₡', 'CR.png', 506),
(52, 'Croatia', 'HR', 'HRV', 191, 'HRK', 'Kuna', 'kn', 'HR.png', 385),
(53, 'Cuba', 'CU', 'CUB', 192, 'CUP', 'Peso', '₱', 'CU.png', NULL),
(54, 'Cyprus', 'CY', 'CYP', 196, 'CYP', 'Pound', NULL, 'CY.png', NULL),
(55, 'Czech Republic', 'CZ', 'CZE', 203, 'CZK', 'Koruna', 'Kč', 'CZ.png', 420),
(56, 'Democratic Republic of the Congo', 'CD', 'COD', 180, 'CDF', 'Franc', NULL, 'CD.png', NULL),
(57, 'Denmark', 'DK', 'DNK', 208, 'DKK', 'Krone', 'kr', 'DK.png', 45),
(58, 'Djibouti', 'DJ', 'DJI', 262, 'DJF', 'Franc', NULL, 'DJ.png', 253),
(59, 'Dominica', 'DM', 'DMA', 212, 'XCD', 'Dollar', '$', 'DM.png', 1767),
(60, 'Dominican Republic', 'DO', 'DOM', 214, 'DOP', 'Peso', 'RD$', 'DO.png', 1809),
(61, 'East Timor', 'TL', 'TLS', 626, 'USD', 'Dollar', '$', 'TL.png', NULL),
(62, 'Ecuador', 'EC', 'ECU', 218, 'USD', 'Dollar', '$', 'EC.png', 593),
(63, 'Egypt', 'EG', 'EGY', 818, 'EGP', 'Pound', '£', 'EG.png', 20),
(64, 'El Salvador', 'SV', 'SLV', 222, 'SVC', 'Colone', '$', 'SV.png', 503),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 226, 'XAF', 'Franc', 'FCF', 'GQ.png', 240),
(66, 'Eritrea', 'ER', 'ERI', 232, 'ERN', 'Nakfa', 'Nfk', 'ER.png', 291),
(67, 'Estonia', 'EE', 'EST', 233, 'EEK', 'Kroon', 'kr', 'EE.png', 372),
(68, 'Ethiopia', 'ET', 'ETH', 231, 'ETB', 'Birr', NULL, 'ET.png', 251),
(69, 'Falkland Islands', 'FK', 'FLK', 238, 'FKP', 'Pound', '£', 'FK.png', NULL),
(70, 'Faroe Islands', 'FO', 'FRO', 234, 'DKK', 'Krone', 'kr', 'FO.png', NULL),
(71, 'Fiji', 'FJ', 'FJI', 242, 'FJD', 'Dollar', '$', 'FJ.png', 679),
(72, 'Finland', 'FI', 'FIN', 246, 'EUR', 'Euro', '€', 'FI.png', 358),
(73, 'France', 'FR', 'FRA', 250, 'EUR', 'Euro', '€', 'FR.png', 33),
(74, 'French Guiana', 'GF', 'GUF', 254, 'EUR', 'Euro', '€', 'GF.png', NULL),
(75, 'French Polynesia', 'PF', 'PYF', 258, 'XPF', 'Franc', NULL, 'PF.png', NULL),
(76, 'French Southern Territories', 'TF', 'ATF', 260, 'EUR', 'Euro  ', '€', 'TF.png', NULL),
(77, 'Gabon', 'GA', 'GAB', 266, 'XAF', 'Franc', 'FCF', 'GA.png', 241),
(78, 'Gambia', 'GM', 'GMB', 270, 'GMD', 'Dalasi', 'D', 'GM.png', 220),
(79, 'Georgia', 'GE', 'GEO', 268, 'GEL', 'Lari', NULL, 'GE.png', 995),
(80, 'Germany', 'DE', 'DEU', 276, 'EUR', 'Euro', '€', 'DE.png', 49),
(81, 'Ghana', 'GH', 'GHA', 288, 'GHC', 'Cedi', '¢', 'GH.png', 233),
(82, 'Gibraltar', 'GI', 'GIB', 292, 'GIP', 'Pound', '£', 'GI.png', NULL),
(83, 'Greece', 'GR', 'GRC', 300, 'EUR', 'Euro', '€', 'GR.png', 30),
(84, 'Greenland', 'GL', 'GRL', 304, 'DKK', 'Krone', 'kr', 'GL.png', NULL),
(85, 'Grenada', 'GD', 'GRD', 308, 'XCD', 'Dollar', '$', 'GD.png', 1473),
(86, 'Guadeloupe', 'GP', 'GLP', 312, 'EUR', 'Euro', '€', 'GP.png', NULL),
(87, 'Guam', 'GU', 'GUM', 316, 'USD', 'Dollar', '$', 'GU.png', NULL),
(88, 'Guatemala', 'GT', 'GTM', 320, 'GTQ', 'Quetzal', 'Q', 'GT.png', 502),
(89, 'Guinea', 'GN', 'GIN', 324, 'GNF', 'Franc', NULL, 'GN.png', 224),
(90, 'Guinea-Bissau', 'GW', 'GNB', 624, 'XOF', 'Franc', NULL, 'GW.png', 245),
(91, 'Guyana', 'GY', 'GUY', 328, 'GYD', 'Dollar', '$', 'GY.png', 592),
(92, 'Haiti', 'HT', 'HTI', 332, 'HTG', 'Gourde', 'G', 'HT.png', 509),
(93, 'Heard Island and McDonald Islands', 'HM', 'HMD', 334, 'AUD', 'Dollar', '$', 'HM.png', NULL),
(94, 'Honduras', 'HN', 'HND', 340, 'HNL', 'Lempira', 'L', 'HN.png', 504),
(95, 'Hong Kong', 'HK', 'HKG', 344, 'HKD', 'Dollar', '$', 'HK.png', NULL),
(96, 'Hungary', 'HU', 'HUN', 348, 'HUF', 'Forint', 'Ft', 'HU.png', 36),
(97, 'Iceland', 'IS', 'ISL', 352, 'ISK', 'Krona', 'kr', 'IS.png', 354),
(98, 'India', 'IN', 'IND', 356, 'INR', 'Rupee', '₹', 'IN.png', 91),
(99, 'Indonesia', 'ID', 'IDN', 360, 'IDR', 'Rupiah', 'Rp', 'ID.png', 62),
(100, 'Iran', 'IR', 'IRN', 364, 'IRR', 'Rial', '﷼', 'IR.png', 98),
(101, 'Iraq', 'IQ', 'IRQ', 368, 'IQD', 'Dinar', NULL, 'IQ.png', 964),
(102, 'Ireland', 'IE', 'IRL', 372, 'EUR', 'Euro', '€', 'IE.png', NULL),
(103, 'Israel', 'IL', 'ISR', 376, 'ILS', 'Shekel', '₪', 'IL.png', 972),
(104, 'Italy', 'IT', 'ITA', 380, 'EUR', 'Euro', '€', 'IT.png', 39),
(105, 'Ivory Coast', 'CI', 'CIV', 384, 'XOF', 'Franc', NULL, 'CI.png', NULL),
(106, 'Jamaica', 'JM', 'JAM', 388, 'JMD', 'Dollar', '$', 'JM.png', 1876),
(107, 'Japan', 'JP', 'JPN', 392, 'JPY', 'Yen', '¥', 'JP.png', 81),
(108, 'Jordan', 'JO', 'JOR', 400, 'JOD', 'Dinar', NULL, 'JO.png', 962),
(109, 'Kazakhstan', 'KZ', 'KAZ', 398, 'KZT', 'Tenge', 'лв', 'KZ.png', 7),
(110, 'Kenya', 'KE', 'KEN', 404, 'KES', 'Shilling', NULL, 'KE.png', 254),
(111, 'Kiribati', 'KI', 'KIR', 296, 'AUD', 'Dollar', '$', 'KI.png', 686),
(112, 'Kuwait', 'KW', 'KWT', 414, 'KWD', 'Dinar', NULL, 'KW.png', 965),
(113, 'Kyrgyzstan', 'KG', 'KGZ', 417, 'KGS', 'Som', 'лв', 'KG.png', NULL),
(114, 'Laos', 'LA', 'LAO', 418, 'LAK', 'Kip', '₭', 'LA.png', NULL),
(115, 'Latvia', 'LV', 'LVA', 428, 'LVL', 'Lat', 'Ls', 'LV.png', 371),
(116, 'Lebanon', 'LB', 'LBN', 422, 'LBP', 'Pound', '£', 'LB.png', 961),
(117, 'Lesotho', 'LS', 'LSO', 426, 'LSL', 'Loti', 'L', 'LS.png', 266),
(118, 'Liberia', 'LR', 'LBR', 430, 'LRD', 'Dollar', '$', 'LR.png', 231),
(119, 'Libya', 'LY', 'LBY', 434, 'LYD', 'Dinar', NULL, 'LY.png', 218),
(120, 'Liechtenstein', 'LI', 'LIE', 438, 'CHF', 'Franc', 'CHF', 'LI.png', NULL),
(121, 'Lithuania', 'LT', 'LTU', 440, 'LTL', 'Litas', 'Lt', 'LT.png', 370),
(122, 'Luxembourg', 'LU', 'LUX', 442, 'EUR', 'Euro', '€', 'LU.png', 352),
(123, 'Macao', 'MO', 'MAC', 446, 'MOP', 'Pataca', 'MOP', 'MO.png', NULL),
(124, 'Macedonia', 'MK', 'MKD', 807, 'MKD', 'Denar', 'ден', 'MK.png', NULL),
(125, 'Madagascar', 'MG', 'MDG', 450, 'MGA', 'Ariary', NULL, 'MG.png', 261),
(126, 'Malawi', 'MW', 'MWI', 454, 'MWK', 'Kwacha', 'MK', 'MW.png', 265),
(127, 'Malaysia', 'MY', 'MYS', 458, 'MYR', 'Ringgit', 'RM', 'MY.png', 60),
(128, 'Maldives', 'MV', 'MDV', 462, 'MVR', 'Rufiyaa', 'Rf', 'MV.png', 960),
(129, 'Mali', 'ML', 'MLI', 466, 'XOF', 'Franc', NULL, 'ML.png', 223),
(130, 'Malta', 'MT', 'MLT', 470, 'MTL', 'Lira', NULL, 'MT.png', NULL),
(131, 'Marshall Islands', 'MH', 'MHL', 584, 'USD', 'Dollar', '$', 'MH.png', 692),
(132, 'Martinique', 'MQ', 'MTQ', 474, 'EUR', 'Euro', '€', 'MQ.png', NULL),
(133, 'Mauritania', 'MR', 'MRT', 478, 'MRO', 'Ouguiya', 'UM', 'MR.png', 222),
(134, 'Mauritius', 'MU', 'MUS', 480, 'MUR', 'Rupee', '₨', 'MU.png', 230),
(135, 'Mayotte', 'YT', 'MYT', 175, 'EUR', 'Euro', '€', 'YT.png', NULL),
(136, 'Mexico', 'MX', 'MEX', 484, 'MXN', 'Peso', '$', 'MX.png', 52),
(137, 'Micronesia', 'FM', 'FSM', 583, 'USD', 'Dollar', '$', 'FM.png', NULL),
(138, 'Moldova', 'MD', 'MDA', 498, 'MDL', 'Leu', NULL, 'MD.png', 373),
(139, 'Monaco', 'MC', 'MCO', 492, 'EUR', 'Euro', '€', 'MC.png', NULL),
(140, 'Mongolia', 'MN', 'MNG', 496, 'MNT', 'Tugrik', '₮', 'MN.png', 976),
(141, 'Montserrat', 'MS', 'MSR', 500, 'XCD', 'Dollar', '$', 'MS.png', NULL),
(142, 'Morocco', 'MA', 'MAR', 504, 'MAD', 'Dirham', NULL, 'MA.png', 212),
(143, 'Mozambique', 'MZ', 'MOZ', 508, 'MZN', 'Meticail', 'MT', 'MZ.png', 258),
(144, 'Myanmar', 'MM', 'MMR', 104, 'MMK', 'Kyat', 'K', 'MM.png', 0),
(145, 'Namibia', 'NA', 'NAM', 516, 'NAD', 'Dollar', '$', 'NA.png', 264),
(146, 'Nauru', 'NR', 'NRU', 520, 'AUD', 'Dollar', '$', 'NR.png', NULL),
(147, 'Nepal', 'NP', 'NPL', 524, 'NPR', 'Rupee', '₨', 'NP.png', 977),
(148, 'Netherlands', 'NL', 'NLD', 528, 'EUR', 'Euro', '€', 'NL.png', 31),
(149, 'Netherlands Antilles', 'AN', 'ANT', 530, 'ANG', 'Guilder', 'ƒ', 'AN.png', NULL),
(150, 'New Caledonia', 'NC', 'NCL', 540, 'XPF', 'Franc', NULL, 'NC.png', NULL),
(151, 'New Zealand', 'NZ', 'NZL', 554, 'NZD', 'Dollar', '$', 'NZ.png', NULL),
(152, 'Nicaragua', 'NI', 'NIC', 558, 'NIO', 'Cordoba', 'C$', 'NI.png', 505),
(153, 'Niger', 'NE', 'NER', 562, 'XOF', 'Franc', NULL, 'NE.png', 227),
(154, 'Nigeria', 'NG', 'NGA', 566, 'NGN', 'Naira', '₦', 'NG.png', 234),
(155, 'Niue', 'NU', 'NIU', 570, 'NZD', 'Dollar', '$', 'NU.png', NULL),
(156, 'Norfolk Island', 'NF', 'NFK', 574, 'AUD', 'Dollar', '$', 'NF.png', NULL),
(157, 'North Korea', 'KP', 'PRK', 408, 'KPW', 'Won', '₩', 'KP.png', NULL),
(158, 'Northern Mariana Islands', 'MP', 'MNP', 580, 'USD', 'Dollar', '$', 'MP.png', NULL),
(159, 'Norway', 'NO', 'NOR', 578, 'NOK', 'Krone', 'kr', 'NO.png', 47),
(160, 'Oman', 'OM', 'OMN', 512, 'OMR', 'Rial', '﷼', 'OM.png', 968),
(161, 'Pakistan', 'PK', 'PAK', 586, 'PKR', 'Rupee', '₨', 'PK.png', 92),
(162, 'Palau', 'PW', 'PLW', 585, 'USD', 'Dollar', '$', 'PW.png', 680),
(163, 'Palestinian Territory', 'PS', 'PSE', 275, 'ILS', 'Shekel', '₪', 'PS.png', NULL),
(164, 'Panama', 'PA', 'PAN', 591, 'PAB', 'Balboa', 'B/.', 'PA.png', 507),
(165, 'Papua New Guinea', 'PG', 'PNG', 598, 'PGK', 'Kina', NULL, 'PG.png', 675),
(166, 'Paraguay', 'PY', 'PRY', 600, 'PYG', 'Guarani', 'Gs', 'PY.png', 595),
(167, 'Peru', 'PE', 'PER', 604, 'PEN', 'Sol', 'S/.', 'PE.png', 51),
(168, 'Philippines', 'PH', 'PHL', 608, 'PHP', 'Peso', 'Php', 'PH.png', 63),
(169, 'Pitcairn', 'PN', 'PCN', 612, 'NZD', 'Dollar', '$', 'PN.png', NULL),
(170, 'Poland', 'PL', 'POL', 616, 'PLN', 'Zloty', 'zł', 'PL.png', 48),
(171, 'Portugal', 'PT', 'PRT', 620, 'EUR', 'Euro', '€', 'PT.png', 351),
(172, 'Puerto Rico', 'PR', 'PRI', 630, 'USD', 'Dollar', '$', 'PR.png', NULL),
(173, 'Qatar', 'QA', 'QAT', 634, 'QAR', 'Rial', '﷼', 'QA.png', 974),
(174, 'Republic of the Congo', 'CG', 'COG', 178, 'XAF', 'Franc', 'FCF', 'CG.png', NULL),
(175, 'Reunion', 'RE', 'REU', 638, 'EUR', 'Euro', '€', 'RE.png', NULL),
(176, 'Romania', 'RO', 'ROU', 642, 'RON', 'Leu', 'lei', 'RO.png', 40),
(177, 'Russia', 'RU', 'RUS', 643, 'RUB', 'Ruble', 'руб', 'RU.png', 7),
(178, 'Rwanda', 'RW', 'RWA', 646, 'RWF', 'Franc', NULL, 'RW.png', 250),
(179, 'Saint Helena', 'SH', 'SHN', 654, 'SHP', 'Pound', '£', 'SH.png', NULL),
(180, 'Saint Kitts and Nevis', 'KN', 'KNA', 659, 'XCD', 'Dollar', '$', 'KN.png', NULL),
(181, 'Saint Lucia', 'LC', 'LCA', 662, 'XCD', 'Dollar', '$', 'LC.png', NULL),
(182, 'Saint Pierre and Miquelon', 'PM', 'SPM', 666, 'EUR', 'Euro', '€', 'PM.png', NULL),
(183, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 670, 'XCD', 'Dollar', '$', 'VC.png', NULL),
(184, 'Samoa', 'WS', 'WSM', 882, 'WST', 'Tala', 'WS$', 'WS.png', 685),
(185, 'San Marino', 'SM', 'SMR', 674, 'EUR', 'Euro', '€', 'SM.png', NULL),
(186, 'Sao Tome and Principe', 'ST', 'STP', 678, 'STD', 'Dobra', 'Db', 'ST.png', 239),
(187, 'Saudi Arabia', 'SA', 'SAU', 682, 'SAR', 'Rial', '﷼', 'SA.png', NULL),
(188, 'Senegal', 'SN', 'SEN', 686, 'XOF', 'Franc', NULL, 'SN.png', 221),
(189, 'Serbia and Montenegro', 'CS', 'SCG', 891, 'RSD', 'Dinar', 'Дин', 'CS.png', NULL),
(190, 'Seychelles', 'SC', 'SYC', 690, 'SCR', 'Rupee', '₨', 'SC.png', 248),
(191, 'Sierra Leone', 'SL', 'SLE', 694, 'SLL', 'Leone', 'Le', 'SL.png', 232),
(192, 'Singapore', 'SG', 'SGP', 702, 'SGD', 'Dollar', '$', 'SG.png', 65),
(193, 'Slovakia', 'SK', 'SVK', 703, 'SKK', 'Koruna', 'Sk', 'SK.png', NULL),
(194, 'Slovenia', 'SI', 'SVN', 705, 'EUR', 'Euro', '€', 'SI.png', 386),
(195, 'Solomon Islands', 'SB', 'SLB', 90, 'SBD', 'Dollar', '$', 'SB.png', 677),
(196, 'Somalia', 'SO', 'SOM', 706, 'SOS', 'Shilling', 'S', 'SO.png', 252),
(197, 'South Africa', 'ZA', 'ZAF', 710, 'ZAR', 'Rand', 'R', 'ZA.png', 27),
(198, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 239, 'GBP', 'Pound', '£', 'GS.png', NULL),
(199, 'South Korea', 'KR', 'KOR', 410, 'KRW', 'Won', '₩', 'KR.png', NULL),
(200, 'Spain', 'ES', 'ESP', 724, 'EUR', 'Euro', '€', 'ES.png', 34),
(201, 'Sri Lanka', 'LK', 'LKA', 144, 'LKR', 'Rupee', '₨', 'LK.png', 94),
(202, 'Sudan', 'SD', 'SDN', 736, 'SDD', 'Dinar', NULL, 'SD.png', 249),
(203, 'Suriname', 'SR', 'SUR', 740, 'SRD', 'Dollar', '$', 'SR.png', 597),
(204, 'Svalbard and Jan Mayen', 'SJ', 'SJM', 744, 'NOK', 'Krone', 'kr', 'SJ.png', NULL),
(205, 'Swaziland', 'SZ', 'SWZ', 748, 'SZL', 'Lilangeni', NULL, 'SZ.png', 268),
(206, 'Sweden', 'SE', 'SWE', 752, 'SEK', 'Krona', 'kr', 'SE.png', 46),
(207, 'Switzerland', 'CH', 'CHE', 756, 'CHF', 'Franc', 'CHF', 'CH.png', 41),
(208, 'Syria', 'SY', 'SYR', 760, 'SYP', 'Pound', '£', 'SY.png', 963),
(209, 'Taiwan', 'TW', 'TWN', 158, 'TWD', 'Dollar', 'NT$', 'TW.png', NULL),
(210, 'Tajikistan', 'TJ', 'TJK', 762, 'TJS', 'Somoni', NULL, 'TJ.png', 992),
(211, 'Tanzania', 'TZ', 'TZA', 834, 'TZS', 'Shilling', NULL, 'TZ.png', 255),
(212, 'Thailand', 'TH', 'THA', 764, 'THB', 'Baht', '฿', 'TH.png', 66),
(213, 'Togo', 'TG', 'TGO', 768, 'XOF', 'Franc', NULL, 'TG.png', 228),
(214, 'Tokelau', 'TK', 'TKL', 772, 'NZD', 'Dollar', '$', 'TK.png', NULL),
(215, 'Tonga', 'TO', 'TON', 776, 'TOP', 'Pa''anga', 'T$', 'TO.png', 676),
(216, 'Trinidad and Tobago', 'TT', 'TTO', 780, 'TTD', 'Dollar', 'TT$', 'TT.png', 1868),
(217, 'Tunisia', 'TN', 'TUN', 788, 'TND', 'Dinar', NULL, 'TN.png', 216),
(218, 'Turkey', 'TR', 'TUR', 792, 'TRY', 'Lira', 'YTL', 'TR.png', NULL),
(219, 'Turkmenistan', 'TM', 'TKM', 795, 'TMM', 'Manat', 'm', 'TM.png', 993),
(220, 'Turks and Caicos Islands', 'TC', 'TCA', 796, 'USD', 'Dollar', '$', 'TC.png', NULL),
(221, 'Tuvalu', 'TV', 'TUV', 798, 'AUD', 'Dollar', '$', 'TV.png', 688),
(222, 'U.S. Virgin Islands', 'VI', 'VIR', 850, 'USD', 'Dollar', '$', 'VI.png', NULL),
(223, 'Uganda', 'UG', 'UGA', 800, 'UGX', 'Shilling', NULL, 'UG.png', 256),
(224, 'Ukraine', 'UA', 'UKR', 804, 'UAH', 'Hryvnia', '₴', 'UA.png', 380),
(225, 'United Arab Emirates', 'AE', 'ARE', 784, 'AED', 'Dirham', NULL, 'AE.png', 971),
(226, 'United Kingdom', 'GB', 'GBR', 826, 'GBP', 'Pound', '£', 'GB.png', 44),
(227, 'United States', 'US', 'USA', 840, 'USD', 'Dollar', '$', 'US.png', NULL),
(228, 'United States Minor Outlying Islands', 'UM', 'UMI', 581, 'USD', 'Dollar ', '$', 'UM.png', NULL),
(229, 'Uruguay', 'UY', 'URY', 858, 'UYU', 'Peso', '$U', 'UY.png', 598),
(230, 'Uzbekistan', 'UZ', 'UZB', 860, 'UZS', 'Som', 'лв', 'UZ.png', 998),
(231, 'Vanuatu', 'VU', 'VUT', 548, 'VUV', 'Vatu', 'Vt', 'VU.png', 678),
(232, 'Vatican', 'VA', 'VAT', 336, 'EUR', 'Euro', '€', 'VA.png', NULL),
(233, 'Venezuela', 'VE', 'VEN', 862, 'VEF', 'Bolivar', 'Bs', 'VE.png', 58),
(234, 'Vietnam', 'VN', 'VNM', 704, 'VND', 'Dong', '₫', 'VN.png', 84),
(235, 'Wallis and Futuna', 'WF', 'WLF', 876, 'XPF', 'Franc', NULL, 'WF.png', NULL),
(236, 'Western Sahara', 'EH', 'ESH', 732, 'MAD', 'Dirham', NULL, 'EH.png', NULL),
(237, 'Yemen', 'YE', 'YEM', 887, 'YER', 'Rial', '﷼', 'YE.png', 967),
(238, 'Zambia', 'ZM', 'ZMB', 894, 'ZMK', 'Kwacha', 'ZK', 'ZM.png', 260),
(239, 'Zimbabwe', 'ZW', 'ZWE', 716, 'ZWD', 'Dollar', 'Z$', 'ZW.png', 263);

-- --------------------------------------------------------

--
-- Table structure for table `dvbc__schema_version`
--

CREATE TABLE IF NOT EXISTS `dvbc__schema_version` (
  `version_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `checksum` varchar(42) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dvbc__schema_version`
--

INSERT INTO `dvbc__schema_version` (`version_id`, `file`, `checksum`, `title`, `created_on`) VALUES
(500, '500_payout_modification.sql', 'f498bb6b2ce5c994d7d450922b2aeb2e', 'payout modification', '2015-02-10 12:09:49'),
(501, '501_payout_batch_add.sql', '04cb03aed74b35db7bff0e630f348b7d', 'payout batch add', '2015-02-10 12:09:52'),
(502, '502_payout_type_added.sql', '8d979ccb5b4c82f3e7abe2d2ae68d82f', 'payout type added', '2015-02-10 12:09:53'),
(503, '503_processing_fee_check.sql', 'eef2142a2ecbe4d7f11d5fb146c2ef98', 'processing fee check', '2015-02-10 12:09:54'),
(504, '504_drop_paystatus_and_improve_payout.sql', 'a762bdd1a04ceb17d723c44decb4433b', 'drop paystatus and improve payout', '2015-02-10 15:30:54'),
(505, '505_add_single_account_occurence_per_payout.sql', '43f7e3ccfa56deea1201612a2e400a51', 'add single account occurence per payout', '2015-02-12 17:38:27'),
(506, '506_company_goals_restrictions_tbl.sql', 'b8abab8e11cf0954a6a50260864a62e9', 'company goals restrictions tbl', '2015-02-12 17:38:27'),
(507, '507_payout_confirmation.sql', '1ab086ec7a931800df3c252b712cfdaa', 'payout confirmation', '2015-02-12 17:38:32'),
(508, '508_update_payroll_contribution.sql', '301acccdeb6bdd838e76a2e73279620f', 'update payroll contribution', '2015-02-12 17:38:32'),
(509, '509_add_retries_count_to_table.sql', '53befea1c121907a41ad0b5fef08fadc', 'add retries count to table', '2015-02-12 17:38:34'),
(510, '510_new_customized_pdf_template.sql', '6873cc2e9adf7d71bc99189d97a1a547', 'new customized pdf template', '2015-02-12 17:38:35'),
(511, '511_add_send_retries_to_payout.sql', '9222f13df2f1e7608f5becf4a7c0bb9c', 'add send retries to payout', '2015-02-12 17:38:35'),
(512, '512_add_attempt_to_batch.sql', 'c9d10522fda464110526dc304d45968a', 'add attempt to batch', '2015-02-12 17:38:37'),
(513, '513_payroll_contribution_default_edited.sql', 'bf202f4102947cf5e46e42b940145434', 'payroll contribution default edited', '2015-02-12 17:38:38'),
(514, '514_payout_add_currency.sql', '788209b82d7a1fc4d726eef4377911da', 'payout add currency', '2015-02-13 14:17:06'),
(515, '515_default_added_to_payroll_allowance_default.sql', 'a2ebdd7c289d5c3d3125f345a7ab9c92', 'default added to payroll allowance default', '2015-02-13 14:23:46'),
(516, '516_update_goals_table.sql', 'dad86009543b6b260bd2d894cb3bf270', 'update goals table', '2015-02-18 08:00:14'),
(517, '517_changed_menu_status_in_module_perms.sql', '5349f2049ce3585bcef817e40e132dba', 'changed menu status in module perms', '2015-02-18 08:00:14'),
(518, '518_payroll_updated_with_paygrade.sql', 'c351229a8af4a5448aeb00d31940385a', 'payroll updated with paygrade', '2015-02-20 19:50:46'),
(519, '519_payroll_updated_with_paygrade.sql', 'ef4a4e4b095fdd33887cfdb32234f03e', 'payroll updated with paygrade', '2015-02-21 11:17:34'),
(520, '520_payroll_updated_with_paygrade.sql', '964e1c5cf09602920aa835f4740fa488', 'payroll updated with paygrade', '2015-02-21 11:17:35'),
(521, '521_module_update.sql', '1212ae42f7622afb52aa0b0f6cfde16e', 'module update', '2015-02-23 13:40:11'),
(522, '522_update_existing_paygrade_with_ref_code.sql', 'b59284f9a825f42aa47482a05d7de62d', 'update existing paygrade with ref code', '2015-02-23 13:40:11'),
(523, '523_module_update.sql', '08a7eeac67d613c6743dfca99e8de27d', 'module update', '2015-02-23 13:40:11'),
(524, '524_module_perm_update.sql', '588e5a8047c582ef4a7d41c0cd4902ab', 'module perm update', '2015-02-23 13:40:11'),
(525, '525_alter_appraisal_settings_templates.sql', '1f1b8362ddac77e7f5967d0da14911c5', 'alter appraisal settings templates', '2015-02-23 13:40:12'),
(526, '526_alter_appraisal_settings_templates.sql', '8ef175e144ab28f676e75cf0afc1284c', 'alter appraisal settings templates', '2015-02-24 13:57:44'),
(527, '527_employee_credential_files.sql', 'a47e8ac6360a036db52ad25bf7e8e16a', 'employee credential files', '2015-02-24 13:57:45'),
(528, '528_menu_ordering.sql', '216a948563357792c7b1a473c60305cf', 'menu ordering', '2015-02-24 18:16:36'),
(529, '529_remove_module_compensation.sql', 'b9816fe533d8b32ec8121a5ace06a7e7', 'remove module compensation', '2015-02-24 18:16:37'),
(530, '530_menu_order_for_staffrecord_and_payroll.sql', 'c0e2930d0b64aa3f04e9efadc9a95955', 'menu order for staffrecord and payroll', '2015-02-25 11:48:53'),
(531, '531_appraisal_menu_order_updates.sql', '2c6a7f9e48759b5e77673ddaa6a0f81c', 'appraisal menu order updates', '2015-02-25 11:48:53'),
(532, '532_goals_menu_order_update.sql', '91756ce8f98c4c528bcb60b0e3d7e143', 'goals menu order update', '2015-02-25 11:48:54'),
(533, '533_training_menu_order_updates.sql', '9badfdf4c263c1a866593988670b6857', 'training menu order updates', '2015-02-25 11:48:54'),
(534, '534_goals_menu_order_update_fix.sql', '267d5aff3144407feb4982fc38662df6', 'goals menu order update fix', '2015-02-25 11:48:55'),
(535, '535_goals_menu_order_fix.sql', 'f7a128564ea99e61960d6a0180bacb1c', 'goals menu order fix', '2015-02-25 11:48:55'),
(536, '536_modules_table_update.sql', 'd6794d5161254b7388dfff6dc57c2771', 'modules table update', '2015-02-27 16:31:02'),
(537, '537_add_onpremise_release.sql', 'a89bb69a20d0ec3fa59dce66dafe1ada', 'add onpremise release', '2015-02-27 16:31:03'),
(538, '538_add_min_version-to_release.sql', 'f2913ffff0c3696f9664e54b1df729a7', 'add min version to release', '2015-02-27 16:31:04'),
(539, '539_add_release_download_table.sql', 'c90ffd616f227ed39b136f8704d76dec', 'add release download table', '2015-02-27 16:31:05'),
(540, '540_new_record_for_documents.sql', 'd4293af971881ec57ecda156fe49cfa8', 'new record for documents', '2015-02-27 16:31:05'),
(541, '541_downloads_template_docs.sql', 'f0431f0b6f841f99e2537384fa246733', 'downloads template docs', '2015-02-27 16:31:07'),
(542, '542_goal_table_update.sql', '5644d65b29f2d7d434d76c9b448553e4', 'goal table update', '2015-02-27 16:31:08'),
(543, '543_downloads_generated_doc_users.sql', 'd55d97259b3ce04e7816542e08fa4198', 'downloads generated doc users', '2015-02-27 16:31:09'),
(544, '544_alter_downloads_doc_tables.sql', 'b27f835a863f06c2c3247ed57dc3e529', 'alter downloads doc tables', '2015-02-27 16:31:12'),
(545, '545_alter_employee_temp_task.sql', 'f4b9c5c269696d444a1843545ce8722e', 'alter employee temp task', '2015-02-27 16:31:13'),
(546, '546_downloads_doc_variables.sql', '2aa8ee87726048498bc2891fd80d78d3', 'downloads doc variables', '2015-03-05 14:26:21'),
(547, '547_downloads_alter_table_names_and_columns.sql', '4d26e7482e9fa9b7aefbe3a54b5fc525', 'downloads alter table names and columns', '2015-03-05 14:26:22'),
(548, '548_modules_sync.sql', '85512971610fc7ee33420bde06ed2fbd', 'modules sync', '2015-03-05 14:26:22'),
(549, '549_vacation_menu_order.sql', '88812817f8c535b4ac115e01af3e5575', 'vacation menu order', '2015-03-05 14:26:23'),
(551, '551_document_center_updates_1.sql', 'c73872f68a6a0341d9215109bb21b808', 'document center updates 1', '2015-03-05 14:26:23'),
(552, '552_alter_downloads_generated_docs.sql', '020f92c7bdfab8a4256f9492f0baaed4', 'alter downloads generated docs', '2015-03-05 14:26:24'),
(553, '553_payment_config_new.sql', '878b30b407d87ff3846bd3b573244cee', 'payment config new', '2015-03-05 14:26:25'),
(554, '554_payment_receipt_table.sql', '86c790e3006103506b3b330ee8c33e02', 'payment receipt table', '2015-03-05 18:05:53'),
(555, '555_add_receipt_ref.sql', '127de177f6a6231ba011c05146b0db0f', 'add receipt ref', '2015-03-05 18:05:54'),
(556, '556_modify_vacation_allowance_table.sql', '786d323f5c982ffd3ea245f00df2dcb6', 'modify vacation allowance table', '2015-03-06 14:07:03'),
(557, '557_job_credit_table.sql', 'c6a0cd2ab494dc79218a38c529d33c83', 'job credit table', '2015-03-06 14:07:04'),
(558, '558_job_credit_indexes.sql', 'e7385c668d6605ab7b7269f3ac75449e', 'job credit indexes', '2015-03-06 14:07:05'),
(559, '559_changed_job_credit_to_int.sql', '81c0da7c495e883f540d11a97e5d576c', 'changed job credit to int', '2015-03-06 14:07:05'),
(560, '560_update_downloads_variables_doc.sql', 'afd7a6595804267b365f11cc4c84c9a4', 'update downloads variables doc', '2015-03-06 14:07:06'),
(561, '561_changed_txn_and_receipt_item.sql', 'f1d9df7c1167423355eadbc563a7ecc5', 'changed txn and receipt item', '2015-03-06 14:07:06'),
(562, '562_vacation_allowance_setting.sql', '577457a2e2400187da16376fee0f5da5', 'vacation allowance setting', '2015-03-06 14:16:02'),
(563, '563_module_perms_update.sql', 'ece16eaf710d14d4a9e90b07b4011667', 'module perms update', '2015-03-06 14:16:02'),
(564, '564_vacation_allowance_setting_update.sql', '8d5e7468dbd4dfea6b106aedb30b0c06', 'vacation allowance setting update', '2015-03-09 12:34:58'),
(565, '565_vacation_allowance_setting_changed.sql', '6d55c846022bc03c6c3d2d7e4276b5bd', 'vacation allowance setting changed', '2015-03-09 12:34:58'),
(566, '566_vacation_allowance_setting_modified.sql', 'd49a49b4f404469ca16ce281d113e75d', 'vacation allowance setting modified', '2015-03-09 12:34:59'),
(567, '567_vacation_allowance_setting_modified2.sql', 'cae58057156086c582c82685cfdbc898', 'vacation allowance setting modified2', '2015-03-09 12:34:59'),
(568, '568_sms_module_setting_table.sql', 'a664ac940bb2c2d8c494fc23e62eb8d0', 'sms module setting table', '2015-03-09 12:34:59'),
(569, '569_add_job_action_credit.sql', '45c3edc66f1421060f191ec63008a6f0', 'add job action credit', '2015-03-09 15:03:58'),
(570, '570_add_job_action_table.sql', '4fe61ddc0a532be600fb559035c81977', 'add job action table', '2015-03-09 15:03:59'),
(571, '571_reward_table_relationship.sql', 'ec87c52bf60a840caeaa2a0bc1fbc4b1', 'reward table relationship', '2015-03-09 15:04:00'),
(572, '572_reward_menu_order.sql', 'af8b31e004bca30ee2c01406d5aa7255', 'reward menu order', '2015-03-09 15:04:00'),
(573, '573_reward_menu_item_removed.sql', 'f9ce18d8201e5f525390f800d0bf197e', 'reward menu item removed', '2015-03-09 15:29:55'),
(574, '574_appraisal_downloads.sql', '81e25ab16f1a1c6be2655cbc702211d6', 'appraisal downloads', '2015-03-10 17:52:13'),
(575, '575_altered_module_perms.sql', 'eda3d24b48a4991349db827ad935ace6', 'altered module perms', '2015-03-10 17:52:13'),
(576, '576_module_notification_settings.sql', '5e41175f6a0f16d55b19fdde3714d385', 'module notification settings', '2015-03-10 17:52:14'),
(577, '577_module_perms_updated.sql', 'da42fc7b07c04a29fe71f7b96f4d8338', 'module perms updated', '2015-03-11 01:01:45'),
(578, '578_employee_queries_issuers.sql', 'd0bf030f830dead4da8aeb85e19eba51', 'employee queries issuers', '2015-03-11 10:57:09'),
(579, '579_employee_request_menu_reorder.sql', '4c842921af6b1abe9b613500797971a5', 'employee request menu reorder', '2015-03-11 10:57:10'),
(580, '580_module_perm_update.sql', '31ca1bc8ceb90ef0e099be1394d94956', 'module perm update', '2015-03-11 10:57:10'),
(581, '581_payroll_comp_temp_modified.sql', '73b55da9d71329e98fcac4ffaec542eb', 'payroll comp temp modified', '2015-03-11 10:58:10'),
(582, '582_start_date_now_date.sql', 'e6c47aa76b06571adca4db05c1709ad6', 'start date now date', '2015-03-12 17:50:32'),
(583, '583_required_login_disabled.sql', '5a5d13b3181f186e909646f0f7f17f73', 'required login disabled', '2015-03-12 17:50:32'),
(584, '584_job_credit_package.sql', '8dfdb47dd181c1fe551205d1c0b38c13', 'job credit package', '2015-03-12 17:50:33'),
(585, '585_job_credit_perms.sql', 'd6f2745346567992e075f73a9b8036de', 'job credit perms', '2015-03-12 17:50:33'),
(586, '586_job_credit_package_status.sql', 'fba2579c089f03286dab5980a997bf20', 'job credit package status', '2015-03-13 16:47:16'),
(587, '587_fixes_on_receipt_tables.sql', '708be451dfd15210bbd771f7c5dee284', 'fixes on receipt tables', '2015-03-13 16:47:17'),
(588, '588_payroll_period_updated.sql', '5e8447bd2d7e51062b13cca4a516d2d7', 'payroll period updated', '2015-03-13 16:47:17'),
(589, '589_payment_txn_mod.sql', 'ac34ed903a2c3482e27434fab3c28efb', 'payment txn mod', '2015-03-13 16:47:18'),
(590, '590_payment_receipt_url_added.sql', 'e8408142ed38a73b41ea7c52bce6fe52', 'payment receipt url added', '2015-03-13 16:47:19'),
(591, '591_rewards_types.sql', 'a6f1dbcefaca9e96302f6525d9daabc2', 'rewards types', '2015-03-13 16:47:19'),
(592, '592_rewards_status.sql', '87d74c5c7238cc11a556e167d7b30c54', 'rewards status', '2015-03-13 16:47:20'),
(593, '593_vat_to_receipt.sql', '79bed28af8bec9b83b0f570ab6818ff2', 'vat to receipt', '2015-03-13 16:47:20'),
(594, '594_insert_questions.sql', '5cefda49aa916210f14bb94b45f365a9', 'insert questions', '2015-03-13 16:47:20'),
(595, '595_employee_workflow_approver.sql', '38c109dbcbf66895475c91fd0cf94382', 'employee workflow approver', '2015-03-13 16:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE IF NOT EXISTS `fees` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fee_name` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`fee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `institution_level`
--

CREATE TABLE IF NOT EXISTS `institution_level` (
  `institution_level_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`institution_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `institution_level`
--

INSERT INTO `institution_level` (`institution_level_id`, `level`) VALUES
(1, '1st Level'),
(2, '2nd Level');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source`
--

CREATE TABLE IF NOT EXISTS `lead_source` (
  `lead_source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_source` varchar(150) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`lead_source_id`),
  KEY `FK_lead_source_created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lead_source`
--

INSERT INTO `lead_source` (`lead_source_id`, `lead_source`, `status`, `created_at`, `created_by`) VALUES
(1, 'Social Media', 0, '2015-10-25 06:30:39', 4),
(2, 'Print Media', 1, '2015-10-25 06:30:51', 4),
(3, 'source 3f86757', 1, '2015-11-04 06:57:28', 4);

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE IF NOT EXISTS `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `marital_status`
--

INSERT INTO `marital_status` (`marital_status_id`, `marital_status`) VALUES
(1, 'Single'),
(2, 'Married'),
(3, 'De-Facto'),
(4, 'Engaged');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `subject`, `id_string`, `status`, `requires_login`, `menu_order`) VALUES
(1, 'My Account', 'account', 1, 1, 100),
(2, 'Agents', 'setup', 1, 1, 200),
(3, 'Branch Office', 'brach_office', 1, 1, 120),
(4, 'Representing Countries', 'rep_countries', 1, 1, 400),
(5, 'Representing Institutions', 'rep_institutions', 1, 1, 500),
(6, 'Reports', 'reports', 1, 1, 600),
(7, 'Lead Management', 'lead_mgt', 1, 1, 700),
(8, 'Applications', 'applications', 1, 1, 800),
(9, 'Setup', 'f_setup', 1, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `module_perms`
--

CREATE TABLE IF NOT EXISTS `module_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  PRIMARY KEY (`perm_id`),
  KEY `FK_module_perms_module_id` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `module_perms`
--

INSERT INTO `module_perms` (`perm_id`, `module_id`, `subject`, `id_string`, `in_menu`, `status`, `menu_order`) VALUES
(1, 1, 'Dashboard', 'dashboard', 1, 1, 100),
(2, 1, 'Edit Profile', 'edit_profile', 1, 1, 100),
(3, 2, 'View', 'agents', 1, 1, 100),
(4, 2, 'Add Agent', 'add_agent', 1, 1, 100),
(5, 3, 'Add', 'add', 1, 1, 100),
(6, 3, 'View', 'view', 1, 1, 100),
(7, 4, 'Add', 'add', 1, 1, 100),
(8, 4, 'View', 'view', 1, 1, 100),
(9, 5, 'Add', 'add', 1, 1, 100),
(10, 5, 'View', 'view', 1, 1, 100),
(11, 6, 'View Applications', 'view_applications', 1, 1, 99),
(12, 6, 'Branch Office Reports', 'branch_office_report', 1, 1, 100),
(13, 6, 'Agent Report', 'agent_report', 1, 1, 100),
(14, 7, 'Leads Source', 'leads_source', 1, 1, 100),
(15, 7, 'Add Leads Source', 'add_leads_source', 0, 1, 100),
(16, 8, 'New Application', 'new_application', 1, 1, 100),
(17, 8, 'Applications', 'app_directory', 1, 1, 100),
(18, 9, 'Processing Fee', 'processing_fee', 1, 1, 100),
(20, 9, 'Commission Receivable', 'comm_receivable', 1, 1, 100),
(21, 9, 'Commission Payable', 'comm_payable', 1, 1, 100),
(22, 9, 'Tution Fees', 'tution_fees', 1, 1, 100),
(23, 9, 'Visa Documents', 'visa_docs', 1, 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `partner_intitutions`
--

CREATE TABLE IF NOT EXISTS `partner_intitutions` (
  `institution_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institution_name` varchar(200) DEFAULT NULL,
  `institution_level_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`institution_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `partner_intitutions`
--

INSERT INTO `partner_intitutions` (`institution_id`, `institution_name`, `institution_level_id`) VALUES
(1, 'Academy of English', 1),
(2, 'Discover English', 1),
(3, 'Academies Australasia Polytechnic (AAPoly)', 1),
(4, 'Federation University Australia @ AAPoly Melbourne', 1),
(5, 'Federation University Australia @ IIBIT Sydney', 1),
(6, 'Federation University Australia @ IIBIT Adelaide', 1),
(7, 'Federation University Australia Main Campus', 1),
(8, 'International Institute of Business and Information Technology (IIBIT)', 1),
(9, 'Careers Australia', 1),
(10, 'Sydney Business and Travel Academy (SBTA)', 1),
(11, 'Bridge Business College (BBC)', 1),
(12, 'Queensland International Business  Academies (QIBA) Brisbane', 1),
(13, 'Queensland International Business  Academies (QIBA) Sydney', 1),
(14, 'Queensland International Business  Academies (QIBA) Melbourne', 1),
(15, 'Sterling College', 1),
(16, 'Perth International College of English (PICE)', 1),
(17, 'Pacific College of Technology (PCT)', 1),
(18, 'TAFE NSW', 1),
(19, 'Australian Pacific College', 1),
(20, 'University of Technology Sydney (UTS)', 2),
(21, 'University of Western Sydney (UWS)', 2),
(22, 'University of New South Wales (UNSW)', 2),
(23, 'University of Wollongong  (UOW)', 2),
(24, 'University of Newcastle', 2),
(25, 'Flinders University', 2),
(26, 'Federation University Australia (fEDuNI)', 2),
(27, 'Curtin University of Technology (Curtin)', 2),
(28, 'University of Western Australia (UWA)', 2),
(29, 'Edith Cowan University (ECU)', 2),
(30, 'Murdoch University ', 2),
(31, 'James Cooks University', 2),
(32, 'Charles Darwin University (CDU)', 2),
(33, 'University of Canberra @ @ AAPoly', 2),
(34, 'Holmes Institute', 1);

-- --------------------------------------------------------

--
-- Table structure for table `processing_fee`
--

CREATE TABLE IF NOT EXISTS `processing_fee` (
  `processing_fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fee_name` varchar(150) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`processing_fee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `processing_fee`
--

INSERT INTO `processing_fee` (`processing_fee_id`, `fee_name`, `amount`, `status`, `date_created`, `created_by`, `description`) VALUES
(1, 'Processing', 2500, 1, '2015-12-10 08:29:59', 4, 'App processing');

-- --------------------------------------------------------

--
-- Table structure for table `rep_countries`
--

CREATE TABLE IF NOT EXISTS `rep_countries` (
  `rep_country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rep_country_id`),
  KEY `FK_rep_countries_created_by` (`created_by`),
  KEY `FK_rep_countries_country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rep_countries`
--

INSERT INTO `rep_countries` (`rep_country_id`, `country_id`, `created_at`, `created_by`, `status`) VALUES
(1, 9, NULL, 4, 1),
(2, 226, '2015-10-16 04:50:47', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rep_country_statuses`
--

CREATE TABLE IF NOT EXISTS `rep_country_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_country_id` int(10) unsigned DEFAULT NULL,
  `status_name` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_rep_country_statuses_rep_country` (`rep_country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `rep_country_statuses`
--

INSERT INTO `rep_country_statuses` (`id`, `rep_country_id`, `status_name`, `status`) VALUES
(1, 1, 'mgnbxmch', 0),
(2, 1, 'ffdgfg', 1),
(3, 1, 'dfdgehjf', 1),
(4, 1, 'dsdlkgiugs', 1),
(5, 2, 'New', 1),
(6, 2, 'Conditional Offer', 1),
(7, 2, 'Unconditional Offer', 1),
(8, 2, 'Deposit Paid', 1),
(9, 2, 'CAS Issued', 0),
(10, 2, 'Visa Applied', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rep_institutions`
--

CREATE TABLE IF NOT EXISTS `rep_institutions` (
  `rep_institution_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_country_id` int(10) unsigned DEFAULT NULL,
  `institution_name` varchar(150) DEFAULT NULL,
  `campus` varchar(150) DEFAULT NULL,
  `intl_contact_person` varchar(150) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`rep_institution_id`),
  KEY `FK_rep_institutions_rep_country` (`rep_country_id`),
  KEY `FK_rep_institutions_created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rep_institutions`
--

INSERT INTO `rep_institutions` (`rep_institution_id`, `rep_country_id`, `institution_name`, `campus`, `intl_contact_person`, `email`, `phone`, `website`, `logo`, `status`, `created_at`, `created_by`, `updated_at`) VALUES
(1, 2, 'cambridge', 'Sokto', 'Conta', 'mdjd.jhdhjs@dh.vjn', '0800999', 'www.sql.com', NULL, 1, '2015-10-18 07:12:31', 4, '0000-00-00 00:00:00'),
(2, 2, 'UK Versity', 'Alabama', 'Chuks', 'na@gmil.com', '08037816000', 'na', '37ebbc604a338db06372b42e5667e4e8.jpg', 1, '2015-10-18 07:32:49', 4, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_event_reg`
--

CREATE TABLE IF NOT EXISTS `student_event_reg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `dateof_birth` datetime DEFAULT NULL,
  `education_fair` varchar(45) DEFAULT NULL,
  `passport` tinyint(3) unsigned DEFAULT NULL,
  `interest` varchar(45) DEFAULT NULL,
  `english_test` varchar(45) DEFAULT NULL,
  `financial_capacity` varchar(45) DEFAULT NULL,
  `course_choice_1` varchar(150) DEFAULT NULL,
  `course_choice_2` varchar(150) DEFAULT NULL,
  `course_choice_3` varchar(150) DEFAULT NULL,
  `attendee` varchar(150) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `highest_qual` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `student_event_reg`
--

INSERT INTO `student_event_reg` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `dateof_birth`, `education_fair`, `passport`, `interest`, `english_test`, `financial_capacity`, `course_choice_1`, `course_choice_2`, `course_choice_3`, `attendee`, `date_added`, `highest_qual`) VALUES
(7, 'tst', 'hjkjfh', '7403909', 'otcleantech@gmail.com', '10 Bola Shadipe Street, Off Adelabu, Surulere', '1970-01-01 00:00:00', 'Lagos, Nigeria', 0, 'Diploma & Advance Diploma', 'None', '$3000', '', '', '', '', '2015-12-12 12:20:03', 'Bachelor');

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE IF NOT EXISTS `super_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`id`, `email`, `password`, `first_name`, `last_name`, `date_created`, `date_updated`, `status`) VALUES
(1, 'admin@crm.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Super', 'Admin', '2015-10-10 11:08:05', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE IF NOT EXISTS `titles` (
  `title_id` int(25) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`title_id`, `title`) VALUES
(1, 'Mr.'),
(2, 'Mrs.'),
(3, 'Miss.');

-- --------------------------------------------------------

--
-- Table structure for table `tution_fees`
--

CREATE TABLE IF NOT EXISTS `tution_fees` (
  `tution_fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fee` varchar(150) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned DEFAULT '1',
  PRIMARY KEY (`tution_fee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tution_fees`
--

INSERT INTO `tution_fees` (`tution_fee_id`, `fee`, `description`, `date_created`, `created_by`, `status`) VALUES
(1, 'initial tuition fees', 'initial tuition fees', '2015-12-11 07:31:46', 4, 1),
(2, 'Per semester', 'Per semester', '2015-12-11 07:32:03', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `user_type` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `created_at`, `last_visit`, `status`, `user_type`, `first_name`, `last_name`, `phone`) VALUES
(4, 'admin@crm.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-05 11:02:26', NULL, 1, 1, 'Super', 'Admin', NULL),
(10, 'aasim@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-10-23 12:33:37', NULL, 1, 3, 'Omoloye', 'johin', '07011221122'),
(13, 'taiwo@gtmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-24 04:31:58', NULL, 1, 3, 'Tohir', 'Omoloye', '0212545455'),
(15, 'salami@yahoo.com', '7dfa5910647b9b85e184c274692bbe8dadf084d1', '2015-10-25 12:48:53', NULL, 0, 3, 'nana', 'fuch', '08000'),
(16, 'soliu@afp.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-10-25 01:18:30', NULL, 1, 3, 'soliu', 'inbo', '0800'),
(18, 'taiwo@tel.net', 'e38ad214943daad1d64c102faec29de4afe9da3d', '2015-10-25 12:34:20', NULL, 1, 2, NULL, NULL, NULL),
(19, 'yusuf@aol.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-11-17 10:27:45', NULL, 1, 2, NULL, NULL, NULL),
(20, 'tundephil@oaa.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-11-22 11:02:21', NULL, 1, 3, 'tunde', 'philips', '09088334477'),
(21, 'mmuritalaraimot@yahoo.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-11-22 11:05:16', NULL, 1, 2, NULL, NULL, NULL),
(23, 'info@tunde.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-11-24 11:51:55', NULL, 0, 4, 'dupe', 'Raheemah', '08037816500'),
(26, 'tttt@ttt.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-12-12 12:05:07', NULL, 0, 4, 'tt', 'ttt', 'ttttt'),
(27, 'otcleantech@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-12-12 12:20:03', NULL, 0, 4, 'tst', 'hjkjfh', '7403909');

-- --------------------------------------------------------

--
-- Table structure for table `user_perms`
--

CREATE TABLE IF NOT EXISTS `user_perms` (
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_perms`
--

INSERT INTO `user_perms` (`user_id`, `perm_id`, `module_id`) VALUES
(4, 1, 1),
(4, 2, 1),
(4, 3, 2),
(4, 4, 2),
(4, 5, 3),
(4, 6, 3),
(4, 7, 4),
(4, 8, 4),
(4, 9, 5),
(4, 10, 5),
(4, 11, 6),
(4, 12, 6),
(4, 13, 6),
(12, 1, 1),
(12, 2, 1),
(12, 3, 2),
(12, 4, 2),
(12, 6, 3),
(12, 8, 4),
(12, 10, 5),
(18, 1, 1),
(18, 2, 1),
(18, 3, 2),
(18, 6, 3),
(18, 8, 4),
(18, 10, 5),
(4, 14, 7),
(18, 14, 7),
(4, 15, 7),
(13, 1, 1),
(13, 2, 1),
(13, 3, 2),
(13, 4, 2),
(13, 6, 3),
(13, 8, 4),
(13, 10, 5),
(13, 14, 7),
(19, 1, 1),
(19, 2, 1),
(19, 3, 2),
(19, 6, 3),
(19, 8, 4),
(19, 10, 5),
(19, 14, 7),
(19, 16, 8),
(19, 17, 8),
(20, 1, 1),
(20, 2, 1),
(20, 3, 2),
(20, 4, 2),
(20, 6, 3),
(20, 8, 4),
(20, 10, 5),
(20, 14, 7),
(21, 1, 1),
(21, 2, 1),
(21, 3, 2),
(21, 6, 3),
(21, 8, 4),
(21, 10, 5),
(21, 14, 7),
(21, 17, 8),
(4, 18, 9),
(4, 20, 9),
(4, 21, 9),
(4, 22, 9),
(4, 23, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id_user_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id_user_type`, `user_type`) VALUES
(1, 'Super Admin'),
(2, 'Branch Manager'),
(3, 'Agent');

-- --------------------------------------------------------

--
-- Table structure for table `visa_docs`
--

CREATE TABLE IF NOT EXISTS `visa_docs` (
  `visa_doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visa_doc` varchar(150) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `added_by` int(10) unsigned DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`visa_doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `visa_types`
--

CREATE TABLE IF NOT EXISTS `visa_types` (
  `visa_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visa_type` varchar(45) NOT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`visa_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `visa_types`
--

INSERT INTO `visa_types` (`visa_type_id`, `visa_type`, `status`, `date_created`) VALUES
(1, 'E-Visa', 1, NULL),
(2, 'College ( 572 )', 1, NULL),
(3, 'University ( 573 )', 1, NULL),
(4, 'ELICOS ( 570 )', 1, NULL),
(5, 'Working Holiday', 1, NULL),
(6, 'Dependent', 1, NULL),
(7, 'Other', 1, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `agents`
--
ALTER TABLE `agents`
  ADD CONSTRAINT `FK_agents_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `applicant_course_selection`
--
ALTER TABLE `applicant_course_selection`
  ADD CONSTRAINT `FK_applicant_course_selection_applicant` FOREIGN KEY (`applicant_id`) REFERENCES `applicants` (`applicant_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_applicant_course_selection_rep_country` FOREIGN KEY (`rep_country_id`) REFERENCES `rep_countries` (`rep_country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_applicant_course_selection_rep_institution` FOREIGN KEY (`rep_institution_id`) REFERENCES `rep_institutions` (`rep_institution_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `applicant_visa_info`
--
ALTER TABLE `applicant_visa_info`
  ADD CONSTRAINT `FK_applicant_visa_info_applicant_id` FOREIGN KEY (`applicant_id`) REFERENCES `applicants` (`applicant_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `FK_branches_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_branches_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lead_source`
--
ALTER TABLE `lead_source`
  ADD CONSTRAINT `FK_lead_source_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `module_perms`
--
ALTER TABLE `module_perms`
  ADD CONSTRAINT `FK_module_perms_module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rep_countries`
--
ALTER TABLE `rep_countries`
  ADD CONSTRAINT `FK_rep_countries_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rep_countries_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rep_country_statuses`
--
ALTER TABLE `rep_country_statuses`
  ADD CONSTRAINT `FK_rep_country_statuses_rep_country` FOREIGN KEY (`rep_country_id`) REFERENCES `rep_countries` (`rep_country_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rep_institutions`
--
ALTER TABLE `rep_institutions`
  ADD CONSTRAINT `FK_rep_institutions_created_by` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rep_institutions_rep_country` FOREIGN KEY (`rep_country_id`) REFERENCES `rep_countries` (`rep_country_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
