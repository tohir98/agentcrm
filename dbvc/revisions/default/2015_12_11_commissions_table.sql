CREATE TABLE `commission_payable` (
  `commission_payable_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `commission_payable` VARCHAR(45),
  `amount` DOUBLE DEFAULT 0,
  `percentage` VARCHAR(45),
  `date_created` DATETIME,
  `created_by` INT UNSIGNED,
  `status` TINYINT UNSIGNED DEFAULT 1,
  PRIMARY KEY (`commission_payable_id`)
)
ENGINE = InnoDB;

ALTER TABLE `commission_payable` ADD COLUMN `description` VARCHAR(150) AFTER `status`;



CREATE TABLE IF NOT EXISTS `commission_receivable` (
  `commission_receivable_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `commission_receivable` VARCHAR(45),
  `amount` DOUBLE DEFAULT 0,
  `percentage` VARCHAR(45),
  `date_created` DATETIME,
  `created_by` INT UNSIGNED,
  `status` TINYINT UNSIGNED DEFAULT 1,
  PRIMARY KEY (`commission_receivable_id`)
)
ENGINE = InnoDB;

ALTER TABLE `commission_receivable` ADD COLUMN `description` VARCHAR(150) AFTER `status`;

CREATE TABLE `tution_fees` (
  `tution_fee_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `fee` VARCHAR(150) NOT NULL,
  `description` VARCHAR(150),
  `date_created` DATETIME NOT NULL,
  `created_by` INT UNSIGNED NOT NULL,
  `status` TINYINT UNSIGNED DEFAULT 1,
  PRIMARY KEY (`tution_fee_id`)
)
ENGINE = InnoDB;





