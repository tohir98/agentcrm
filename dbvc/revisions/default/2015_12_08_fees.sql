CREATE TABLE `fees` (
  `fee_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `fee_name` VARCHAR(45),
  `amount` DOUBLE,
  `date_created` DATETIME,
  `created_by` INT UNSIGNED,
  `date_updated` DATETIME,
  PRIMARY KEY (`fee_id`)
)
ENGINE = InnoDB;
