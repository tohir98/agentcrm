CREATE TABLE IF NOT EXISTS `visa_types` (
  `visa_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `visa_type` VARCHAR(45) NOT NULL,
  `status` TINYINT UNSIGNED,
  `date_created` DATETIME,
  PRIMARY KEY (`visa_type_id`)
)
ENGINE = InnoDB;