CREATE TABLE `applicant_profile_picture` (
  `applicant_profile_picture_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `applicant_id` INT UNSIGNED NOT NULL,
  `profile_picture` VARCHAR(150) NOT NULL,
  `profile_picture_url` VARCHAR(150) NOT NULL,
  `date_created` DATETIME NOT NULL,
  `created_by` INT UNSIGNED NOT NULL,
  `date_updated` DATETIME,
  PRIMARY KEY (`applicant_profile_picture_id`)
)
ENGINE = InnoDB;