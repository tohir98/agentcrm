CREATE TABLE IF NOT EXISTS `course_duration` (
  `course_duration_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_duration` varchar(150) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `added_by` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`course_duration_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;