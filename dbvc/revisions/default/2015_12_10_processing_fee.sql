CREATE TABLE  `processing_fee` (
  `processing_fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fee_name` varchar(150) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`processing_fee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;