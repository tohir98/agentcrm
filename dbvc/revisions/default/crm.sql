/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : crm

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2015-10-25 11:57:20
*/



-- ----------------------------
-- Table structure for `agents`
-- ----------------------------
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `agent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `download_csv` int(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `processing_office` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agents
-- ----------------------------
INSERT INTO `agents` VALUES ('1', 'olu', 'famous', 'info@olu.com', null, null, '2015-10-21 12:12:47', '4', '0', '1', '8');
INSERT INTO `agents` VALUES ('2', 'Olu', 'famous', '123211', '1', null, '2015-10-21 12:19:56', '4', '0', '1', '9');

-- ----------------------------
-- Table structure for `branches`
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(150) NOT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(150) DEFAULT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `skype_id` varchar(45) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'Lagos', '10 Bola Shadipe Street, Off Adelabu, Surulere', 'Surulere', 'Lagos', '154', 'otcleantech@gmail.com', '+2348037816587', 'www.lagos.com', 'Ibe kachi', 'Cordinator', '020', 'ccc', '6', null, null, null);
INSERT INTO `branches` VALUES ('7', 'Abuja Office', 'Utako shopping plaza', 'Utako', 'FCT', '154', 'mktmgr9@aol.com', '+2348037816587', 'info@aol.com', 'Raheemah Muritala', 'Cordinator', '08055', '', '14', null, '2015-10-24 04:38:08', '4');
INSERT INTO `branches` VALUES ('8', 'Sala', 'na', 'na', 'na', '146', 'na@na.cj', '99', 'nn', 'nana fuch', 'Supervisor', '08000', 'muri.tala', '15', null, '2015-10-25 12:48:53', '4');
INSERT INTO `branches` VALUES ('9', 'Sagamu', 'sagamu', 'sagamu', 'ogun', '148', 'sag@mu.m', '0909', 'na', 'soliu inbo', 'na', '0800', 'na', '16', null, '2015-10-25 01:18:30', '4');

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `country_id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `flag` varchar(6) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'Afghanistan', 'AF', 'AFG', '4', 'AFN', 'Afghani', '؋', 'AF.png', '93');
INSERT INTO `countries` VALUES ('2', 'Albania', 'AL', 'ALB', '8', 'ALL', 'Lek', 'Lek', 'AL.png', '355');
INSERT INTO `countries` VALUES ('3', 'Algeria', 'DZ', 'DZA', '12', 'DZD', 'Dinar', null, 'DZ.png', '213');
INSERT INTO `countries` VALUES ('4', 'American Samoa', 'AS', 'ASM', '16', 'USD', 'Dollar', '$', 'AS.png', null);
INSERT INTO `countries` VALUES ('5', 'Andorra', 'AD', 'AND', '20', 'EUR', 'Euro', '€', 'AD.png', null);
INSERT INTO `countries` VALUES ('6', 'Angola', 'AO', 'AGO', '24', 'AOA', 'Kwanza', 'Kz', 'AO.png', '244');
INSERT INTO `countries` VALUES ('7', 'Anguilla', 'AI', 'AIA', '660', 'XCD', 'Dollar', '$', 'AI.png', null);
INSERT INTO `countries` VALUES ('8', 'Antarctica', 'AQ', 'ATA', '10', '', '', null, 'AQ.png', null);
INSERT INTO `countries` VALUES ('9', 'Antigua and Barbuda', 'AG', 'ATG', '28', 'XCD', 'Dollar', '$', 'AG.png', '1268');
INSERT INTO `countries` VALUES ('10', 'Argentina', 'AR', 'ARG', '32', 'ARS', 'Peso', '$', 'AR.png', '54');
INSERT INTO `countries` VALUES ('11', 'Armenia', 'AM', 'ARM', '51', 'AMD', 'Dram', null, 'AM.png', '374');
INSERT INTO `countries` VALUES ('12', 'Aruba', 'AW', 'ABW', '533', 'AWG', 'Guilder', 'ƒ', 'AW.png', null);
INSERT INTO `countries` VALUES ('13', 'Australia', 'AU', 'AUS', '36', 'AUD', 'Dollar', '$', 'AU.png', null);
INSERT INTO `countries` VALUES ('14', 'Austria', 'AT', 'AUT', '40', 'EUR', 'Euro', '€', 'AT.png', '43');
INSERT INTO `countries` VALUES ('15', 'Azerbaijan', 'AZ', 'AZE', '31', 'AZN', 'Manat', 'ман', 'AZ.png', '994');
INSERT INTO `countries` VALUES ('16', 'Bahamas', 'BS', 'BHS', '44', 'BSD', 'Dollar', '$', 'BS.png', null);
INSERT INTO `countries` VALUES ('17', 'Bahrain', 'BH', 'BHR', '48', 'BHD', 'Dinar', null, 'BH.png', '973');
INSERT INTO `countries` VALUES ('18', 'Bangladesh', 'BD', 'BGD', '50', 'BDT', 'Taka', null, 'BD.png', '880');
INSERT INTO `countries` VALUES ('19', 'Barbados', 'BB', 'BRB', '52', 'BBD', 'Dollar', '$', 'BB.png', null);
INSERT INTO `countries` VALUES ('20', 'Belarus', 'BY', 'BLR', '112', 'BYR', 'Ruble', 'p.', 'BY.png', '1246');
INSERT INTO `countries` VALUES ('21', 'Belgium', 'BE', 'BEL', '56', 'EUR', 'Euro', '€', 'BE.png', '32');
INSERT INTO `countries` VALUES ('22', 'Belize', 'BZ', 'BLZ', '84', 'BZD', 'Dollar', 'BZ$', 'BZ.png', '501');
INSERT INTO `countries` VALUES ('23', 'Benin', 'BJ', 'BEN', '204', 'XOF', 'Franc', null, 'BJ.png', '229');
INSERT INTO `countries` VALUES ('24', 'Bermuda', 'BM', 'BMU', '60', 'BMD', 'Dollar', '$', 'BM.png', null);
INSERT INTO `countries` VALUES ('25', 'Bhutan', 'BT', 'BTN', '64', 'BTN', 'Ngultrum', null, 'BT.png', '975');
INSERT INTO `countries` VALUES ('26', 'Bolivia', 'BO', 'BOL', '68', 'BOB', 'Boliviano', '$b', 'BO.png', '591');
INSERT INTO `countries` VALUES ('27', 'Bosnia and Herzegovina', 'BA', 'BIH', '70', 'BAM', 'Marka', 'KM', 'BA.png', '387');
INSERT INTO `countries` VALUES ('28', 'Botswana', 'BW', 'BWA', '72', 'BWP', 'Pula', 'P', 'BW.png', '267');
INSERT INTO `countries` VALUES ('29', 'Bouvet Island', 'BV', 'BVT', '74', 'NOK', 'Krone', 'kr', 'BV.png', null);
INSERT INTO `countries` VALUES ('30', 'Brazil', 'BR', 'BRA', '76', 'BRL', 'Real', 'R$', 'BR.png', '55');
INSERT INTO `countries` VALUES ('31', 'British Indian Ocean Territory', 'IO', 'IOT', '86', 'USD', 'Dollar', '$', 'IO.png', null);
INSERT INTO `countries` VALUES ('32', 'British Virgin Islands', 'VG', 'VGB', '92', 'USD', 'Dollar', '$', 'VG.png', null);
INSERT INTO `countries` VALUES ('33', 'Brunei', 'BN', 'BRN', '96', 'BND', 'Dollar', '$', 'BN.png', null);
INSERT INTO `countries` VALUES ('34', 'Bulgaria', 'BG', 'BGR', '100', 'BGN', 'Lev', 'лв', 'BG.png', '359');
INSERT INTO `countries` VALUES ('35', 'Burkina Faso', 'BF', 'BFA', '854', 'XOF', 'Franc', null, 'BF.png', '226');
INSERT INTO `countries` VALUES ('36', 'Burundi', 'BI', 'BDI', '108', 'BIF', 'Franc', null, 'BI.png', '257');
INSERT INTO `countries` VALUES ('37', 'Cambodia', 'KH', 'KHM', '116', 'KHR', 'Riels', '៛', 'KH.png', '855');
INSERT INTO `countries` VALUES ('38', 'Cameroon', 'CM', 'CMR', '120', 'XAF', 'Franc', 'FCF', 'CM.png', '237');
INSERT INTO `countries` VALUES ('39', 'Canada', 'CA', 'CAN', '124', 'CAD', 'Dollar', '$', 'CA.png', '1');
INSERT INTO `countries` VALUES ('40', 'Cape Verde', 'CV', 'CPV', '132', 'CVE', 'Escudo', null, 'CV.png', '238');
INSERT INTO `countries` VALUES ('41', 'Cayman Islands', 'KY', 'CYM', '136', 'KYD', 'Dollar', '$', 'KY.png', null);
INSERT INTO `countries` VALUES ('42', 'Central African Republic', 'CF', 'CAF', '140', 'XAF', 'Franc', 'FCF', 'CF.png', '236');
INSERT INTO `countries` VALUES ('43', 'Chad', 'TD', 'TCD', '148', 'XAF', 'Franc', null, 'TD.png', '235');
INSERT INTO `countries` VALUES ('44', 'Chile', 'CL', 'CHL', '152', 'CLP', 'Peso', null, 'CL.png', '56');
INSERT INTO `countries` VALUES ('45', 'China', 'CN', 'CHN', '156', 'CNY', 'Yuan Renminbi', '¥', 'CN.png', '86');
INSERT INTO `countries` VALUES ('46', 'Christmas Island', 'CX', 'CXR', '162', 'AUD', 'Dollar', '$', 'CX.png', null);
INSERT INTO `countries` VALUES ('47', 'Cocos Islands', 'CC', 'CCK', '166', 'AUD', 'Dollar', '$', 'CC.png', null);
INSERT INTO `countries` VALUES ('48', 'Colombia', 'CO', 'COL', '170', 'COP', 'Peso', '$', 'CO.png', '57');
INSERT INTO `countries` VALUES ('49', 'Comoros', 'KM', 'COM', '174', 'KMF', 'Franc', null, 'KM.png', '269');
INSERT INTO `countries` VALUES ('50', 'Cook Islands', 'CK', 'COK', '184', 'NZD', 'Dollar', '$', 'CK.png', null);
INSERT INTO `countries` VALUES ('51', 'Costa Rica', 'CR', 'CRI', '188', 'CRC', 'Colon', '₡', 'CR.png', '506');
INSERT INTO `countries` VALUES ('52', 'Croatia', 'HR', 'HRV', '191', 'HRK', 'Kuna', 'kn', 'HR.png', '385');
INSERT INTO `countries` VALUES ('53', 'Cuba', 'CU', 'CUB', '192', 'CUP', 'Peso', '₱', 'CU.png', null);
INSERT INTO `countries` VALUES ('54', 'Cyprus', 'CY', 'CYP', '196', 'CYP', 'Pound', null, 'CY.png', null);
INSERT INTO `countries` VALUES ('55', 'Czech Republic', 'CZ', 'CZE', '203', 'CZK', 'Koruna', 'Kč', 'CZ.png', '420');
INSERT INTO `countries` VALUES ('56', 'Democratic Republic of the Congo', 'CD', 'COD', '180', 'CDF', 'Franc', null, 'CD.png', null);
INSERT INTO `countries` VALUES ('57', 'Denmark', 'DK', 'DNK', '208', 'DKK', 'Krone', 'kr', 'DK.png', '45');
INSERT INTO `countries` VALUES ('58', 'Djibouti', 'DJ', 'DJI', '262', 'DJF', 'Franc', null, 'DJ.png', '253');
INSERT INTO `countries` VALUES ('59', 'Dominica', 'DM', 'DMA', '212', 'XCD', 'Dollar', '$', 'DM.png', '1767');
INSERT INTO `countries` VALUES ('60', 'Dominican Republic', 'DO', 'DOM', '214', 'DOP', 'Peso', 'RD$', 'DO.png', '1809');
INSERT INTO `countries` VALUES ('61', 'East Timor', 'TL', 'TLS', '626', 'USD', 'Dollar', '$', 'TL.png', null);
INSERT INTO `countries` VALUES ('62', 'Ecuador', 'EC', 'ECU', '218', 'USD', 'Dollar', '$', 'EC.png', '593');
INSERT INTO `countries` VALUES ('63', 'Egypt', 'EG', 'EGY', '818', 'EGP', 'Pound', '£', 'EG.png', '20');
INSERT INTO `countries` VALUES ('64', 'El Salvador', 'SV', 'SLV', '222', 'SVC', 'Colone', '$', 'SV.png', '503');
INSERT INTO `countries` VALUES ('65', 'Equatorial Guinea', 'GQ', 'GNQ', '226', 'XAF', 'Franc', 'FCF', 'GQ.png', '240');
INSERT INTO `countries` VALUES ('66', 'Eritrea', 'ER', 'ERI', '232', 'ERN', 'Nakfa', 'Nfk', 'ER.png', '291');
INSERT INTO `countries` VALUES ('67', 'Estonia', 'EE', 'EST', '233', 'EEK', 'Kroon', 'kr', 'EE.png', '372');
INSERT INTO `countries` VALUES ('68', 'Ethiopia', 'ET', 'ETH', '231', 'ETB', 'Birr', null, 'ET.png', '251');
INSERT INTO `countries` VALUES ('69', 'Falkland Islands', 'FK', 'FLK', '238', 'FKP', 'Pound', '£', 'FK.png', null);
INSERT INTO `countries` VALUES ('70', 'Faroe Islands', 'FO', 'FRO', '234', 'DKK', 'Krone', 'kr', 'FO.png', null);
INSERT INTO `countries` VALUES ('71', 'Fiji', 'FJ', 'FJI', '242', 'FJD', 'Dollar', '$', 'FJ.png', '679');
INSERT INTO `countries` VALUES ('72', 'Finland', 'FI', 'FIN', '246', 'EUR', 'Euro', '€', 'FI.png', '358');
INSERT INTO `countries` VALUES ('73', 'France', 'FR', 'FRA', '250', 'EUR', 'Euro', '€', 'FR.png', '33');
INSERT INTO `countries` VALUES ('74', 'French Guiana', 'GF', 'GUF', '254', 'EUR', 'Euro', '€', 'GF.png', null);
INSERT INTO `countries` VALUES ('75', 'French Polynesia', 'PF', 'PYF', '258', 'XPF', 'Franc', null, 'PF.png', null);
INSERT INTO `countries` VALUES ('76', 'French Southern Territories', 'TF', 'ATF', '260', 'EUR', 'Euro  ', '€', 'TF.png', null);
INSERT INTO `countries` VALUES ('77', 'Gabon', 'GA', 'GAB', '266', 'XAF', 'Franc', 'FCF', 'GA.png', '241');
INSERT INTO `countries` VALUES ('78', 'Gambia', 'GM', 'GMB', '270', 'GMD', 'Dalasi', 'D', 'GM.png', '220');
INSERT INTO `countries` VALUES ('79', 'Georgia', 'GE', 'GEO', '268', 'GEL', 'Lari', null, 'GE.png', '995');
INSERT INTO `countries` VALUES ('80', 'Germany', 'DE', 'DEU', '276', 'EUR', 'Euro', '€', 'DE.png', '49');
INSERT INTO `countries` VALUES ('81', 'Ghana', 'GH', 'GHA', '288', 'GHC', 'Cedi', '¢', 'GH.png', '233');
INSERT INTO `countries` VALUES ('82', 'Gibraltar', 'GI', 'GIB', '292', 'GIP', 'Pound', '£', 'GI.png', null);
INSERT INTO `countries` VALUES ('83', 'Greece', 'GR', 'GRC', '300', 'EUR', 'Euro', '€', 'GR.png', '30');
INSERT INTO `countries` VALUES ('84', 'Greenland', 'GL', 'GRL', '304', 'DKK', 'Krone', 'kr', 'GL.png', null);
INSERT INTO `countries` VALUES ('85', 'Grenada', 'GD', 'GRD', '308', 'XCD', 'Dollar', '$', 'GD.png', '1473');
INSERT INTO `countries` VALUES ('86', 'Guadeloupe', 'GP', 'GLP', '312', 'EUR', 'Euro', '€', 'GP.png', null);
INSERT INTO `countries` VALUES ('87', 'Guam', 'GU', 'GUM', '316', 'USD', 'Dollar', '$', 'GU.png', null);
INSERT INTO `countries` VALUES ('88', 'Guatemala', 'GT', 'GTM', '320', 'GTQ', 'Quetzal', 'Q', 'GT.png', '502');
INSERT INTO `countries` VALUES ('89', 'Guinea', 'GN', 'GIN', '324', 'GNF', 'Franc', null, 'GN.png', '224');
INSERT INTO `countries` VALUES ('90', 'Guinea-Bissau', 'GW', 'GNB', '624', 'XOF', 'Franc', null, 'GW.png', '245');
INSERT INTO `countries` VALUES ('91', 'Guyana', 'GY', 'GUY', '328', 'GYD', 'Dollar', '$', 'GY.png', '592');
INSERT INTO `countries` VALUES ('92', 'Haiti', 'HT', 'HTI', '332', 'HTG', 'Gourde', 'G', 'HT.png', '509');
INSERT INTO `countries` VALUES ('93', 'Heard Island and McDonald Islands', 'HM', 'HMD', '334', 'AUD', 'Dollar', '$', 'HM.png', null);
INSERT INTO `countries` VALUES ('94', 'Honduras', 'HN', 'HND', '340', 'HNL', 'Lempira', 'L', 'HN.png', '504');
INSERT INTO `countries` VALUES ('95', 'Hong Kong', 'HK', 'HKG', '344', 'HKD', 'Dollar', '$', 'HK.png', null);
INSERT INTO `countries` VALUES ('96', 'Hungary', 'HU', 'HUN', '348', 'HUF', 'Forint', 'Ft', 'HU.png', '36');
INSERT INTO `countries` VALUES ('97', 'Iceland', 'IS', 'ISL', '352', 'ISK', 'Krona', 'kr', 'IS.png', '354');
INSERT INTO `countries` VALUES ('98', 'India', 'IN', 'IND', '356', 'INR', 'Rupee', '₹', 'IN.png', '91');
INSERT INTO `countries` VALUES ('99', 'Indonesia', 'ID', 'IDN', '360', 'IDR', 'Rupiah', 'Rp', 'ID.png', '62');
INSERT INTO `countries` VALUES ('100', 'Iran', 'IR', 'IRN', '364', 'IRR', 'Rial', '﷼', 'IR.png', '98');
INSERT INTO `countries` VALUES ('101', 'Iraq', 'IQ', 'IRQ', '368', 'IQD', 'Dinar', null, 'IQ.png', '964');
INSERT INTO `countries` VALUES ('102', 'Ireland', 'IE', 'IRL', '372', 'EUR', 'Euro', '€', 'IE.png', null);
INSERT INTO `countries` VALUES ('103', 'Israel', 'IL', 'ISR', '376', 'ILS', 'Shekel', '₪', 'IL.png', '972');
INSERT INTO `countries` VALUES ('104', 'Italy', 'IT', 'ITA', '380', 'EUR', 'Euro', '€', 'IT.png', '39');
INSERT INTO `countries` VALUES ('105', 'Ivory Coast', 'CI', 'CIV', '384', 'XOF', 'Franc', null, 'CI.png', null);
INSERT INTO `countries` VALUES ('106', 'Jamaica', 'JM', 'JAM', '388', 'JMD', 'Dollar', '$', 'JM.png', '1876');
INSERT INTO `countries` VALUES ('107', 'Japan', 'JP', 'JPN', '392', 'JPY', 'Yen', '¥', 'JP.png', '81');
INSERT INTO `countries` VALUES ('108', 'Jordan', 'JO', 'JOR', '400', 'JOD', 'Dinar', null, 'JO.png', '962');
INSERT INTO `countries` VALUES ('109', 'Kazakhstan', 'KZ', 'KAZ', '398', 'KZT', 'Tenge', 'лв', 'KZ.png', '7');
INSERT INTO `countries` VALUES ('110', 'Kenya', 'KE', 'KEN', '404', 'KES', 'Shilling', null, 'KE.png', '254');
INSERT INTO `countries` VALUES ('111', 'Kiribati', 'KI', 'KIR', '296', 'AUD', 'Dollar', '$', 'KI.png', '686');
INSERT INTO `countries` VALUES ('112', 'Kuwait', 'KW', 'KWT', '414', 'KWD', 'Dinar', null, 'KW.png', '965');
INSERT INTO `countries` VALUES ('113', 'Kyrgyzstan', 'KG', 'KGZ', '417', 'KGS', 'Som', 'лв', 'KG.png', null);
INSERT INTO `countries` VALUES ('114', 'Laos', 'LA', 'LAO', '418', 'LAK', 'Kip', '₭', 'LA.png', null);
INSERT INTO `countries` VALUES ('115', 'Latvia', 'LV', 'LVA', '428', 'LVL', 'Lat', 'Ls', 'LV.png', '371');
INSERT INTO `countries` VALUES ('116', 'Lebanon', 'LB', 'LBN', '422', 'LBP', 'Pound', '£', 'LB.png', '961');
INSERT INTO `countries` VALUES ('117', 'Lesotho', 'LS', 'LSO', '426', 'LSL', 'Loti', 'L', 'LS.png', '266');
INSERT INTO `countries` VALUES ('118', 'Liberia', 'LR', 'LBR', '430', 'LRD', 'Dollar', '$', 'LR.png', '231');
INSERT INTO `countries` VALUES ('119', 'Libya', 'LY', 'LBY', '434', 'LYD', 'Dinar', null, 'LY.png', '218');
INSERT INTO `countries` VALUES ('120', 'Liechtenstein', 'LI', 'LIE', '438', 'CHF', 'Franc', 'CHF', 'LI.png', null);
INSERT INTO `countries` VALUES ('121', 'Lithuania', 'LT', 'LTU', '440', 'LTL', 'Litas', 'Lt', 'LT.png', '370');
INSERT INTO `countries` VALUES ('122', 'Luxembourg', 'LU', 'LUX', '442', 'EUR', 'Euro', '€', 'LU.png', '352');
INSERT INTO `countries` VALUES ('123', 'Macao', 'MO', 'MAC', '446', 'MOP', 'Pataca', 'MOP', 'MO.png', null);
INSERT INTO `countries` VALUES ('124', 'Macedonia', 'MK', 'MKD', '807', 'MKD', 'Denar', 'ден', 'MK.png', null);
INSERT INTO `countries` VALUES ('125', 'Madagascar', 'MG', 'MDG', '450', 'MGA', 'Ariary', null, 'MG.png', '261');
INSERT INTO `countries` VALUES ('126', 'Malawi', 'MW', 'MWI', '454', 'MWK', 'Kwacha', 'MK', 'MW.png', '265');
INSERT INTO `countries` VALUES ('127', 'Malaysia', 'MY', 'MYS', '458', 'MYR', 'Ringgit', 'RM', 'MY.png', '60');
INSERT INTO `countries` VALUES ('128', 'Maldives', 'MV', 'MDV', '462', 'MVR', 'Rufiyaa', 'Rf', 'MV.png', '960');
INSERT INTO `countries` VALUES ('129', 'Mali', 'ML', 'MLI', '466', 'XOF', 'Franc', null, 'ML.png', '223');
INSERT INTO `countries` VALUES ('130', 'Malta', 'MT', 'MLT', '470', 'MTL', 'Lira', null, 'MT.png', null);
INSERT INTO `countries` VALUES ('131', 'Marshall Islands', 'MH', 'MHL', '584', 'USD', 'Dollar', '$', 'MH.png', '692');
INSERT INTO `countries` VALUES ('132', 'Martinique', 'MQ', 'MTQ', '474', 'EUR', 'Euro', '€', 'MQ.png', null);
INSERT INTO `countries` VALUES ('133', 'Mauritania', 'MR', 'MRT', '478', 'MRO', 'Ouguiya', 'UM', 'MR.png', '222');
INSERT INTO `countries` VALUES ('134', 'Mauritius', 'MU', 'MUS', '480', 'MUR', 'Rupee', '₨', 'MU.png', '230');
INSERT INTO `countries` VALUES ('135', 'Mayotte', 'YT', 'MYT', '175', 'EUR', 'Euro', '€', 'YT.png', null);
INSERT INTO `countries` VALUES ('136', 'Mexico', 'MX', 'MEX', '484', 'MXN', 'Peso', '$', 'MX.png', '52');
INSERT INTO `countries` VALUES ('137', 'Micronesia', 'FM', 'FSM', '583', 'USD', 'Dollar', '$', 'FM.png', null);
INSERT INTO `countries` VALUES ('138', 'Moldova', 'MD', 'MDA', '498', 'MDL', 'Leu', null, 'MD.png', '373');
INSERT INTO `countries` VALUES ('139', 'Monaco', 'MC', 'MCO', '492', 'EUR', 'Euro', '€', 'MC.png', null);
INSERT INTO `countries` VALUES ('140', 'Mongolia', 'MN', 'MNG', '496', 'MNT', 'Tugrik', '₮', 'MN.png', '976');
INSERT INTO `countries` VALUES ('141', 'Montserrat', 'MS', 'MSR', '500', 'XCD', 'Dollar', '$', 'MS.png', null);
INSERT INTO `countries` VALUES ('142', 'Morocco', 'MA', 'MAR', '504', 'MAD', 'Dirham', null, 'MA.png', '212');
INSERT INTO `countries` VALUES ('143', 'Mozambique', 'MZ', 'MOZ', '508', 'MZN', 'Meticail', 'MT', 'MZ.png', '258');
INSERT INTO `countries` VALUES ('144', 'Myanmar', 'MM', 'MMR', '104', 'MMK', 'Kyat', 'K', 'MM.png', '0');
INSERT INTO `countries` VALUES ('145', 'Namibia', 'NA', 'NAM', '516', 'NAD', 'Dollar', '$', 'NA.png', '264');
INSERT INTO `countries` VALUES ('146', 'Nauru', 'NR', 'NRU', '520', 'AUD', 'Dollar', '$', 'NR.png', null);
INSERT INTO `countries` VALUES ('147', 'Nepal', 'NP', 'NPL', '524', 'NPR', 'Rupee', '₨', 'NP.png', '977');
INSERT INTO `countries` VALUES ('148', 'Netherlands', 'NL', 'NLD', '528', 'EUR', 'Euro', '€', 'NL.png', '31');
INSERT INTO `countries` VALUES ('149', 'Netherlands Antilles', 'AN', 'ANT', '530', 'ANG', 'Guilder', 'ƒ', 'AN.png', null);
INSERT INTO `countries` VALUES ('150', 'New Caledonia', 'NC', 'NCL', '540', 'XPF', 'Franc', null, 'NC.png', null);
INSERT INTO `countries` VALUES ('151', 'New Zealand', 'NZ', 'NZL', '554', 'NZD', 'Dollar', '$', 'NZ.png', null);
INSERT INTO `countries` VALUES ('152', 'Nicaragua', 'NI', 'NIC', '558', 'NIO', 'Cordoba', 'C$', 'NI.png', '505');
INSERT INTO `countries` VALUES ('153', 'Niger', 'NE', 'NER', '562', 'XOF', 'Franc', null, 'NE.png', '227');
INSERT INTO `countries` VALUES ('154', 'Nigeria', 'NG', 'NGA', '566', 'NGN', 'Naira', '₦', 'NG.png', '234');
INSERT INTO `countries` VALUES ('155', 'Niue', 'NU', 'NIU', '570', 'NZD', 'Dollar', '$', 'NU.png', null);
INSERT INTO `countries` VALUES ('156', 'Norfolk Island', 'NF', 'NFK', '574', 'AUD', 'Dollar', '$', 'NF.png', null);
INSERT INTO `countries` VALUES ('157', 'North Korea', 'KP', 'PRK', '408', 'KPW', 'Won', '₩', 'KP.png', null);
INSERT INTO `countries` VALUES ('158', 'Northern Mariana Islands', 'MP', 'MNP', '580', 'USD', 'Dollar', '$', 'MP.png', null);
INSERT INTO `countries` VALUES ('159', 'Norway', 'NO', 'NOR', '578', 'NOK', 'Krone', 'kr', 'NO.png', '47');
INSERT INTO `countries` VALUES ('160', 'Oman', 'OM', 'OMN', '512', 'OMR', 'Rial', '﷼', 'OM.png', '968');
INSERT INTO `countries` VALUES ('161', 'Pakistan', 'PK', 'PAK', '586', 'PKR', 'Rupee', '₨', 'PK.png', '92');
INSERT INTO `countries` VALUES ('162', 'Palau', 'PW', 'PLW', '585', 'USD', 'Dollar', '$', 'PW.png', '680');
INSERT INTO `countries` VALUES ('163', 'Palestinian Territory', 'PS', 'PSE', '275', 'ILS', 'Shekel', '₪', 'PS.png', null);
INSERT INTO `countries` VALUES ('164', 'Panama', 'PA', 'PAN', '591', 'PAB', 'Balboa', 'B/.', 'PA.png', '507');
INSERT INTO `countries` VALUES ('165', 'Papua New Guinea', 'PG', 'PNG', '598', 'PGK', 'Kina', null, 'PG.png', '675');
INSERT INTO `countries` VALUES ('166', 'Paraguay', 'PY', 'PRY', '600', 'PYG', 'Guarani', 'Gs', 'PY.png', '595');
INSERT INTO `countries` VALUES ('167', 'Peru', 'PE', 'PER', '604', 'PEN', 'Sol', 'S/.', 'PE.png', '51');
INSERT INTO `countries` VALUES ('168', 'Philippines', 'PH', 'PHL', '608', 'PHP', 'Peso', 'Php', 'PH.png', '63');
INSERT INTO `countries` VALUES ('169', 'Pitcairn', 'PN', 'PCN', '612', 'NZD', 'Dollar', '$', 'PN.png', null);
INSERT INTO `countries` VALUES ('170', 'Poland', 'PL', 'POL', '616', 'PLN', 'Zloty', 'zł', 'PL.png', '48');
INSERT INTO `countries` VALUES ('171', 'Portugal', 'PT', 'PRT', '620', 'EUR', 'Euro', '€', 'PT.png', '351');
INSERT INTO `countries` VALUES ('172', 'Puerto Rico', 'PR', 'PRI', '630', 'USD', 'Dollar', '$', 'PR.png', null);
INSERT INTO `countries` VALUES ('173', 'Qatar', 'QA', 'QAT', '634', 'QAR', 'Rial', '﷼', 'QA.png', '974');
INSERT INTO `countries` VALUES ('174', 'Republic of the Congo', 'CG', 'COG', '178', 'XAF', 'Franc', 'FCF', 'CG.png', null);
INSERT INTO `countries` VALUES ('175', 'Reunion', 'RE', 'REU', '638', 'EUR', 'Euro', '€', 'RE.png', null);
INSERT INTO `countries` VALUES ('176', 'Romania', 'RO', 'ROU', '642', 'RON', 'Leu', 'lei', 'RO.png', '40');
INSERT INTO `countries` VALUES ('177', 'Russia', 'RU', 'RUS', '643', 'RUB', 'Ruble', 'руб', 'RU.png', '7');
INSERT INTO `countries` VALUES ('178', 'Rwanda', 'RW', 'RWA', '646', 'RWF', 'Franc', null, 'RW.png', '250');
INSERT INTO `countries` VALUES ('179', 'Saint Helena', 'SH', 'SHN', '654', 'SHP', 'Pound', '£', 'SH.png', null);
INSERT INTO `countries` VALUES ('180', 'Saint Kitts and Nevis', 'KN', 'KNA', '659', 'XCD', 'Dollar', '$', 'KN.png', null);
INSERT INTO `countries` VALUES ('181', 'Saint Lucia', 'LC', 'LCA', '662', 'XCD', 'Dollar', '$', 'LC.png', null);
INSERT INTO `countries` VALUES ('182', 'Saint Pierre and Miquelon', 'PM', 'SPM', '666', 'EUR', 'Euro', '€', 'PM.png', null);
INSERT INTO `countries` VALUES ('183', 'Saint Vincent and the Grenadines', 'VC', 'VCT', '670', 'XCD', 'Dollar', '$', 'VC.png', null);
INSERT INTO `countries` VALUES ('184', 'Samoa', 'WS', 'WSM', '882', 'WST', 'Tala', 'WS$', 'WS.png', '685');
INSERT INTO `countries` VALUES ('185', 'San Marino', 'SM', 'SMR', '674', 'EUR', 'Euro', '€', 'SM.png', null);
INSERT INTO `countries` VALUES ('186', 'Sao Tome and Principe', 'ST', 'STP', '678', 'STD', 'Dobra', 'Db', 'ST.png', '239');
INSERT INTO `countries` VALUES ('187', 'Saudi Arabia', 'SA', 'SAU', '682', 'SAR', 'Rial', '﷼', 'SA.png', null);
INSERT INTO `countries` VALUES ('188', 'Senegal', 'SN', 'SEN', '686', 'XOF', 'Franc', null, 'SN.png', '221');
INSERT INTO `countries` VALUES ('189', 'Serbia and Montenegro', 'CS', 'SCG', '891', 'RSD', 'Dinar', 'Дин', 'CS.png', null);
INSERT INTO `countries` VALUES ('190', 'Seychelles', 'SC', 'SYC', '690', 'SCR', 'Rupee', '₨', 'SC.png', '248');
INSERT INTO `countries` VALUES ('191', 'Sierra Leone', 'SL', 'SLE', '694', 'SLL', 'Leone', 'Le', 'SL.png', '232');
INSERT INTO `countries` VALUES ('192', 'Singapore', 'SG', 'SGP', '702', 'SGD', 'Dollar', '$', 'SG.png', '65');
INSERT INTO `countries` VALUES ('193', 'Slovakia', 'SK', 'SVK', '703', 'SKK', 'Koruna', 'Sk', 'SK.png', null);
INSERT INTO `countries` VALUES ('194', 'Slovenia', 'SI', 'SVN', '705', 'EUR', 'Euro', '€', 'SI.png', '386');
INSERT INTO `countries` VALUES ('195', 'Solomon Islands', 'SB', 'SLB', '90', 'SBD', 'Dollar', '$', 'SB.png', '677');
INSERT INTO `countries` VALUES ('196', 'Somalia', 'SO', 'SOM', '706', 'SOS', 'Shilling', 'S', 'SO.png', '252');
INSERT INTO `countries` VALUES ('197', 'South Africa', 'ZA', 'ZAF', '710', 'ZAR', 'Rand', 'R', 'ZA.png', '27');
INSERT INTO `countries` VALUES ('198', 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '239', 'GBP', 'Pound', '£', 'GS.png', null);
INSERT INTO `countries` VALUES ('199', 'South Korea', 'KR', 'KOR', '410', 'KRW', 'Won', '₩', 'KR.png', null);
INSERT INTO `countries` VALUES ('200', 'Spain', 'ES', 'ESP', '724', 'EUR', 'Euro', '€', 'ES.png', '34');
INSERT INTO `countries` VALUES ('201', 'Sri Lanka', 'LK', 'LKA', '144', 'LKR', 'Rupee', '₨', 'LK.png', '94');
INSERT INTO `countries` VALUES ('202', 'Sudan', 'SD', 'SDN', '736', 'SDD', 'Dinar', null, 'SD.png', '249');
INSERT INTO `countries` VALUES ('203', 'Suriname', 'SR', 'SUR', '740', 'SRD', 'Dollar', '$', 'SR.png', '597');
INSERT INTO `countries` VALUES ('204', 'Svalbard and Jan Mayen', 'SJ', 'SJM', '744', 'NOK', 'Krone', 'kr', 'SJ.png', null);
INSERT INTO `countries` VALUES ('205', 'Swaziland', 'SZ', 'SWZ', '748', 'SZL', 'Lilangeni', null, 'SZ.png', '268');
INSERT INTO `countries` VALUES ('206', 'Sweden', 'SE', 'SWE', '752', 'SEK', 'Krona', 'kr', 'SE.png', '46');
INSERT INTO `countries` VALUES ('207', 'Switzerland', 'CH', 'CHE', '756', 'CHF', 'Franc', 'CHF', 'CH.png', '41');
INSERT INTO `countries` VALUES ('208', 'Syria', 'SY', 'SYR', '760', 'SYP', 'Pound', '£', 'SY.png', '963');
INSERT INTO `countries` VALUES ('209', 'Taiwan', 'TW', 'TWN', '158', 'TWD', 'Dollar', 'NT$', 'TW.png', null);
INSERT INTO `countries` VALUES ('210', 'Tajikistan', 'TJ', 'TJK', '762', 'TJS', 'Somoni', null, 'TJ.png', '992');
INSERT INTO `countries` VALUES ('211', 'Tanzania', 'TZ', 'TZA', '834', 'TZS', 'Shilling', null, 'TZ.png', '255');
INSERT INTO `countries` VALUES ('212', 'Thailand', 'TH', 'THA', '764', 'THB', 'Baht', '฿', 'TH.png', '66');
INSERT INTO `countries` VALUES ('213', 'Togo', 'TG', 'TGO', '768', 'XOF', 'Franc', null, 'TG.png', '228');
INSERT INTO `countries` VALUES ('214', 'Tokelau', 'TK', 'TKL', '772', 'NZD', 'Dollar', '$', 'TK.png', null);
INSERT INTO `countries` VALUES ('215', 'Tonga', 'TO', 'TON', '776', 'TOP', 'Pa\'anga', 'T$', 'TO.png', '676');
INSERT INTO `countries` VALUES ('216', 'Trinidad and Tobago', 'TT', 'TTO', '780', 'TTD', 'Dollar', 'TT$', 'TT.png', '1868');
INSERT INTO `countries` VALUES ('217', 'Tunisia', 'TN', 'TUN', '788', 'TND', 'Dinar', null, 'TN.png', '216');
INSERT INTO `countries` VALUES ('218', 'Turkey', 'TR', 'TUR', '792', 'TRY', 'Lira', 'YTL', 'TR.png', null);
INSERT INTO `countries` VALUES ('219', 'Turkmenistan', 'TM', 'TKM', '795', 'TMM', 'Manat', 'm', 'TM.png', '993');
INSERT INTO `countries` VALUES ('220', 'Turks and Caicos Islands', 'TC', 'TCA', '796', 'USD', 'Dollar', '$', 'TC.png', null);
INSERT INTO `countries` VALUES ('221', 'Tuvalu', 'TV', 'TUV', '798', 'AUD', 'Dollar', '$', 'TV.png', '688');
INSERT INTO `countries` VALUES ('222', 'U.S. Virgin Islands', 'VI', 'VIR', '850', 'USD', 'Dollar', '$', 'VI.png', null);
INSERT INTO `countries` VALUES ('223', 'Uganda', 'UG', 'UGA', '800', 'UGX', 'Shilling', null, 'UG.png', '256');
INSERT INTO `countries` VALUES ('224', 'Ukraine', 'UA', 'UKR', '804', 'UAH', 'Hryvnia', '₴', 'UA.png', '380');
INSERT INTO `countries` VALUES ('225', 'United Arab Emirates', 'AE', 'ARE', '784', 'AED', 'Dirham', null, 'AE.png', '971');
INSERT INTO `countries` VALUES ('226', 'United Kingdom', 'GB', 'GBR', '826', 'GBP', 'Pound', '£', 'GB.png', '44');
INSERT INTO `countries` VALUES ('227', 'United States', 'US', 'USA', '840', 'USD', 'Dollar', '$', 'US.png', null);
INSERT INTO `countries` VALUES ('228', 'United States Minor Outlying Islands', 'UM', 'UMI', '581', 'USD', 'Dollar ', '$', 'UM.png', null);
INSERT INTO `countries` VALUES ('229', 'Uruguay', 'UY', 'URY', '858', 'UYU', 'Peso', '$U', 'UY.png', '598');
INSERT INTO `countries` VALUES ('230', 'Uzbekistan', 'UZ', 'UZB', '860', 'UZS', 'Som', 'лв', 'UZ.png', '998');
INSERT INTO `countries` VALUES ('231', 'Vanuatu', 'VU', 'VUT', '548', 'VUV', 'Vatu', 'Vt', 'VU.png', '678');
INSERT INTO `countries` VALUES ('232', 'Vatican', 'VA', 'VAT', '336', 'EUR', 'Euro', '€', 'VA.png', null);
INSERT INTO `countries` VALUES ('233', 'Venezuela', 'VE', 'VEN', '862', 'VEF', 'Bolivar', 'Bs', 'VE.png', '58');
INSERT INTO `countries` VALUES ('234', 'Vietnam', 'VN', 'VNM', '704', 'VND', 'Dong', '₫', 'VN.png', '84');
INSERT INTO `countries` VALUES ('235', 'Wallis and Futuna', 'WF', 'WLF', '876', 'XPF', 'Franc', null, 'WF.png', null);
INSERT INTO `countries` VALUES ('236', 'Western Sahara', 'EH', 'ESH', '732', 'MAD', 'Dirham', null, 'EH.png', null);
INSERT INTO `countries` VALUES ('237', 'Yemen', 'YE', 'YEM', '887', 'YER', 'Rial', '﷼', 'YE.png', '967');
INSERT INTO `countries` VALUES ('238', 'Zambia', 'ZM', 'ZMB', '894', 'ZMK', 'Kwacha', 'ZK', 'ZM.png', '260');
INSERT INTO `countries` VALUES ('239', 'Zimbabwe', 'ZW', 'ZWE', '716', 'ZWD', 'Dollar', 'Z$', 'ZW.png', '263');

-- ----------------------------
-- Table structure for `dvbc__schema_version`
-- ----------------------------
DROP TABLE IF EXISTS `dvbc__schema_version`;
CREATE TABLE `dvbc__schema_version` (
  `version_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `checksum` varchar(42) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dvbc__schema_version
-- ----------------------------
INSERT INTO `dvbc__schema_version` VALUES ('500', '500_payout_modification.sql', 'f498bb6b2ce5c994d7d450922b2aeb2e', 'payout modification', '2015-02-10 13:09:49');
INSERT INTO `dvbc__schema_version` VALUES ('501', '501_payout_batch_add.sql', '04cb03aed74b35db7bff0e630f348b7d', 'payout batch add', '2015-02-10 13:09:52');
INSERT INTO `dvbc__schema_version` VALUES ('502', '502_payout_type_added.sql', '8d979ccb5b4c82f3e7abe2d2ae68d82f', 'payout type added', '2015-02-10 13:09:53');
INSERT INTO `dvbc__schema_version` VALUES ('503', '503_processing_fee_check.sql', 'eef2142a2ecbe4d7f11d5fb146c2ef98', 'processing fee check', '2015-02-10 13:09:54');
INSERT INTO `dvbc__schema_version` VALUES ('504', '504_drop_paystatus_and_improve_payout.sql', 'a762bdd1a04ceb17d723c44decb4433b', 'drop paystatus and improve payout', '2015-02-10 16:30:54');
INSERT INTO `dvbc__schema_version` VALUES ('505', '505_add_single_account_occurence_per_payout.sql', '43f7e3ccfa56deea1201612a2e400a51', 'add single account occurence per payout', '2015-02-12 18:38:27');
INSERT INTO `dvbc__schema_version` VALUES ('506', '506_company_goals_restrictions_tbl.sql', 'b8abab8e11cf0954a6a50260864a62e9', 'company goals restrictions tbl', '2015-02-12 18:38:27');
INSERT INTO `dvbc__schema_version` VALUES ('507', '507_payout_confirmation.sql', '1ab086ec7a931800df3c252b712cfdaa', 'payout confirmation', '2015-02-12 18:38:32');
INSERT INTO `dvbc__schema_version` VALUES ('508', '508_update_payroll_contribution.sql', '301acccdeb6bdd838e76a2e73279620f', 'update payroll contribution', '2015-02-12 18:38:32');
INSERT INTO `dvbc__schema_version` VALUES ('509', '509_add_retries_count_to_table.sql', '53befea1c121907a41ad0b5fef08fadc', 'add retries count to table', '2015-02-12 18:38:34');
INSERT INTO `dvbc__schema_version` VALUES ('510', '510_new_customized_pdf_template.sql', '6873cc2e9adf7d71bc99189d97a1a547', 'new customized pdf template', '2015-02-12 18:38:35');
INSERT INTO `dvbc__schema_version` VALUES ('511', '511_add_send_retries_to_payout.sql', '9222f13df2f1e7608f5becf4a7c0bb9c', 'add send retries to payout', '2015-02-12 18:38:35');
INSERT INTO `dvbc__schema_version` VALUES ('512', '512_add_attempt_to_batch.sql', 'c9d10522fda464110526dc304d45968a', 'add attempt to batch', '2015-02-12 18:38:37');
INSERT INTO `dvbc__schema_version` VALUES ('513', '513_payroll_contribution_default_edited.sql', 'bf202f4102947cf5e46e42b940145434', 'payroll contribution default edited', '2015-02-12 18:38:38');
INSERT INTO `dvbc__schema_version` VALUES ('514', '514_payout_add_currency.sql', '788209b82d7a1fc4d726eef4377911da', 'payout add currency', '2015-02-13 15:17:06');
INSERT INTO `dvbc__schema_version` VALUES ('515', '515_default_added_to_payroll_allowance_default.sql', 'a2ebdd7c289d5c3d3125f345a7ab9c92', 'default added to payroll allowance default', '2015-02-13 15:23:46');
INSERT INTO `dvbc__schema_version` VALUES ('516', '516_update_goals_table.sql', 'dad86009543b6b260bd2d894cb3bf270', 'update goals table', '2015-02-18 09:00:14');
INSERT INTO `dvbc__schema_version` VALUES ('517', '517_changed_menu_status_in_module_perms.sql', '5349f2049ce3585bcef817e40e132dba', 'changed menu status in module perms', '2015-02-18 09:00:14');
INSERT INTO `dvbc__schema_version` VALUES ('518', '518_payroll_updated_with_paygrade.sql', 'c351229a8af4a5448aeb00d31940385a', 'payroll updated with paygrade', '2015-02-20 20:50:46');
INSERT INTO `dvbc__schema_version` VALUES ('519', '519_payroll_updated_with_paygrade.sql', 'ef4a4e4b095fdd33887cfdb32234f03e', 'payroll updated with paygrade', '2015-02-21 12:17:34');
INSERT INTO `dvbc__schema_version` VALUES ('520', '520_payroll_updated_with_paygrade.sql', '964e1c5cf09602920aa835f4740fa488', 'payroll updated with paygrade', '2015-02-21 12:17:35');
INSERT INTO `dvbc__schema_version` VALUES ('521', '521_module_update.sql', '1212ae42f7622afb52aa0b0f6cfde16e', 'module update', '2015-02-23 14:40:11');
INSERT INTO `dvbc__schema_version` VALUES ('522', '522_update_existing_paygrade_with_ref_code.sql', 'b59284f9a825f42aa47482a05d7de62d', 'update existing paygrade with ref code', '2015-02-23 14:40:11');
INSERT INTO `dvbc__schema_version` VALUES ('523', '523_module_update.sql', '08a7eeac67d613c6743dfca99e8de27d', 'module update', '2015-02-23 14:40:11');
INSERT INTO `dvbc__schema_version` VALUES ('524', '524_module_perm_update.sql', '588e5a8047c582ef4a7d41c0cd4902ab', 'module perm update', '2015-02-23 14:40:11');
INSERT INTO `dvbc__schema_version` VALUES ('525', '525_alter_appraisal_settings_templates.sql', '1f1b8362ddac77e7f5967d0da14911c5', 'alter appraisal settings templates', '2015-02-23 14:40:12');
INSERT INTO `dvbc__schema_version` VALUES ('526', '526_alter_appraisal_settings_templates.sql', '8ef175e144ab28f676e75cf0afc1284c', 'alter appraisal settings templates', '2015-02-24 14:57:44');
INSERT INTO `dvbc__schema_version` VALUES ('527', '527_employee_credential_files.sql', 'a47e8ac6360a036db52ad25bf7e8e16a', 'employee credential files', '2015-02-24 14:57:45');
INSERT INTO `dvbc__schema_version` VALUES ('528', '528_menu_ordering.sql', '216a948563357792c7b1a473c60305cf', 'menu ordering', '2015-02-24 19:16:36');
INSERT INTO `dvbc__schema_version` VALUES ('529', '529_remove_module_compensation.sql', 'b9816fe533d8b32ec8121a5ace06a7e7', 'remove module compensation', '2015-02-24 19:16:37');
INSERT INTO `dvbc__schema_version` VALUES ('530', '530_menu_order_for_staffrecord_and_payroll.sql', 'c0e2930d0b64aa3f04e9efadc9a95955', 'menu order for staffrecord and payroll', '2015-02-25 12:48:53');
INSERT INTO `dvbc__schema_version` VALUES ('531', '531_appraisal_menu_order_updates.sql', '2c6a7f9e48759b5e77673ddaa6a0f81c', 'appraisal menu order updates', '2015-02-25 12:48:53');
INSERT INTO `dvbc__schema_version` VALUES ('532', '532_goals_menu_order_update.sql', '91756ce8f98c4c528bcb60b0e3d7e143', 'goals menu order update', '2015-02-25 12:48:54');
INSERT INTO `dvbc__schema_version` VALUES ('533', '533_training_menu_order_updates.sql', '9badfdf4c263c1a866593988670b6857', 'training menu order updates', '2015-02-25 12:48:54');
INSERT INTO `dvbc__schema_version` VALUES ('534', '534_goals_menu_order_update_fix.sql', '267d5aff3144407feb4982fc38662df6', 'goals menu order update fix', '2015-02-25 12:48:55');
INSERT INTO `dvbc__schema_version` VALUES ('535', '535_goals_menu_order_fix.sql', 'f7a128564ea99e61960d6a0180bacb1c', 'goals menu order fix', '2015-02-25 12:48:55');
INSERT INTO `dvbc__schema_version` VALUES ('536', '536_modules_table_update.sql', 'd6794d5161254b7388dfff6dc57c2771', 'modules table update', '2015-02-27 17:31:02');
INSERT INTO `dvbc__schema_version` VALUES ('537', '537_add_onpremise_release.sql', 'a89bb69a20d0ec3fa59dce66dafe1ada', 'add onpremise release', '2015-02-27 17:31:03');
INSERT INTO `dvbc__schema_version` VALUES ('538', '538_add_min_version-to_release.sql', 'f2913ffff0c3696f9664e54b1df729a7', 'add min version to release', '2015-02-27 17:31:04');
INSERT INTO `dvbc__schema_version` VALUES ('539', '539_add_release_download_table.sql', 'c90ffd616f227ed39b136f8704d76dec', 'add release download table', '2015-02-27 17:31:05');
INSERT INTO `dvbc__schema_version` VALUES ('540', '540_new_record_for_documents.sql', 'd4293af971881ec57ecda156fe49cfa8', 'new record for documents', '2015-02-27 17:31:05');
INSERT INTO `dvbc__schema_version` VALUES ('541', '541_downloads_template_docs.sql', 'f0431f0b6f841f99e2537384fa246733', 'downloads template docs', '2015-02-27 17:31:07');
INSERT INTO `dvbc__schema_version` VALUES ('542', '542_goal_table_update.sql', '5644d65b29f2d7d434d76c9b448553e4', 'goal table update', '2015-02-27 17:31:08');
INSERT INTO `dvbc__schema_version` VALUES ('543', '543_downloads_generated_doc_users.sql', 'd55d97259b3ce04e7816542e08fa4198', 'downloads generated doc users', '2015-02-27 17:31:09');
INSERT INTO `dvbc__schema_version` VALUES ('544', '544_alter_downloads_doc_tables.sql', 'b27f835a863f06c2c3247ed57dc3e529', 'alter downloads doc tables', '2015-02-27 17:31:12');
INSERT INTO `dvbc__schema_version` VALUES ('545', '545_alter_employee_temp_task.sql', 'f4b9c5c269696d444a1843545ce8722e', 'alter employee temp task', '2015-02-27 17:31:13');
INSERT INTO `dvbc__schema_version` VALUES ('546', '546_downloads_doc_variables.sql', '2aa8ee87726048498bc2891fd80d78d3', 'downloads doc variables', '2015-03-05 15:26:21');
INSERT INTO `dvbc__schema_version` VALUES ('547', '547_downloads_alter_table_names_and_columns.sql', '4d26e7482e9fa9b7aefbe3a54b5fc525', 'downloads alter table names and columns', '2015-03-05 15:26:22');
INSERT INTO `dvbc__schema_version` VALUES ('548', '548_modules_sync.sql', '85512971610fc7ee33420bde06ed2fbd', 'modules sync', '2015-03-05 15:26:22');
INSERT INTO `dvbc__schema_version` VALUES ('549', '549_vacation_menu_order.sql', '88812817f8c535b4ac115e01af3e5575', 'vacation menu order', '2015-03-05 15:26:23');
INSERT INTO `dvbc__schema_version` VALUES ('551', '551_document_center_updates_1.sql', 'c73872f68a6a0341d9215109bb21b808', 'document center updates 1', '2015-03-05 15:26:23');
INSERT INTO `dvbc__schema_version` VALUES ('552', '552_alter_downloads_generated_docs.sql', '020f92c7bdfab8a4256f9492f0baaed4', 'alter downloads generated docs', '2015-03-05 15:26:24');
INSERT INTO `dvbc__schema_version` VALUES ('553', '553_payment_config_new.sql', '878b30b407d87ff3846bd3b573244cee', 'payment config new', '2015-03-05 15:26:25');
INSERT INTO `dvbc__schema_version` VALUES ('554', '554_payment_receipt_table.sql', '86c790e3006103506b3b330ee8c33e02', 'payment receipt table', '2015-03-05 19:05:53');
INSERT INTO `dvbc__schema_version` VALUES ('555', '555_add_receipt_ref.sql', '127de177f6a6231ba011c05146b0db0f', 'add receipt ref', '2015-03-05 19:05:54');
INSERT INTO `dvbc__schema_version` VALUES ('556', '556_modify_vacation_allowance_table.sql', '786d323f5c982ffd3ea245f00df2dcb6', 'modify vacation allowance table', '2015-03-06 15:07:03');
INSERT INTO `dvbc__schema_version` VALUES ('557', '557_job_credit_table.sql', 'c6a0cd2ab494dc79218a38c529d33c83', 'job credit table', '2015-03-06 15:07:04');
INSERT INTO `dvbc__schema_version` VALUES ('558', '558_job_credit_indexes.sql', 'e7385c668d6605ab7b7269f3ac75449e', 'job credit indexes', '2015-03-06 15:07:05');
INSERT INTO `dvbc__schema_version` VALUES ('559', '559_changed_job_credit_to_int.sql', '81c0da7c495e883f540d11a97e5d576c', 'changed job credit to int', '2015-03-06 15:07:05');
INSERT INTO `dvbc__schema_version` VALUES ('560', '560_update_downloads_variables_doc.sql', 'afd7a6595804267b365f11cc4c84c9a4', 'update downloads variables doc', '2015-03-06 15:07:06');
INSERT INTO `dvbc__schema_version` VALUES ('561', '561_changed_txn_and_receipt_item.sql', 'f1d9df7c1167423355eadbc563a7ecc5', 'changed txn and receipt item', '2015-03-06 15:07:06');
INSERT INTO `dvbc__schema_version` VALUES ('562', '562_vacation_allowance_setting.sql', '577457a2e2400187da16376fee0f5da5', 'vacation allowance setting', '2015-03-06 15:16:02');
INSERT INTO `dvbc__schema_version` VALUES ('563', '563_module_perms_update.sql', 'ece16eaf710d14d4a9e90b07b4011667', 'module perms update', '2015-03-06 15:16:02');
INSERT INTO `dvbc__schema_version` VALUES ('564', '564_vacation_allowance_setting_update.sql', '8d5e7468dbd4dfea6b106aedb30b0c06', 'vacation allowance setting update', '2015-03-09 13:34:58');
INSERT INTO `dvbc__schema_version` VALUES ('565', '565_vacation_allowance_setting_changed.sql', '6d55c846022bc03c6c3d2d7e4276b5bd', 'vacation allowance setting changed', '2015-03-09 13:34:58');
INSERT INTO `dvbc__schema_version` VALUES ('566', '566_vacation_allowance_setting_modified.sql', 'd49a49b4f404469ca16ce281d113e75d', 'vacation allowance setting modified', '2015-03-09 13:34:59');
INSERT INTO `dvbc__schema_version` VALUES ('567', '567_vacation_allowance_setting_modified2.sql', 'cae58057156086c582c82685cfdbc898', 'vacation allowance setting modified2', '2015-03-09 13:34:59');
INSERT INTO `dvbc__schema_version` VALUES ('568', '568_sms_module_setting_table.sql', 'a664ac940bb2c2d8c494fc23e62eb8d0', 'sms module setting table', '2015-03-09 13:34:59');
INSERT INTO `dvbc__schema_version` VALUES ('569', '569_add_job_action_credit.sql', '45c3edc66f1421060f191ec63008a6f0', 'add job action credit', '2015-03-09 16:03:58');
INSERT INTO `dvbc__schema_version` VALUES ('570', '570_add_job_action_table.sql', '4fe61ddc0a532be600fb559035c81977', 'add job action table', '2015-03-09 16:03:59');
INSERT INTO `dvbc__schema_version` VALUES ('571', '571_reward_table_relationship.sql', 'ec87c52bf60a840caeaa2a0bc1fbc4b1', 'reward table relationship', '2015-03-09 16:04:00');
INSERT INTO `dvbc__schema_version` VALUES ('572', '572_reward_menu_order.sql', 'af8b31e004bca30ee2c01406d5aa7255', 'reward menu order', '2015-03-09 16:04:00');
INSERT INTO `dvbc__schema_version` VALUES ('573', '573_reward_menu_item_removed.sql', 'f9ce18d8201e5f525390f800d0bf197e', 'reward menu item removed', '2015-03-09 16:29:55');
INSERT INTO `dvbc__schema_version` VALUES ('574', '574_appraisal_downloads.sql', '81e25ab16f1a1c6be2655cbc702211d6', 'appraisal downloads', '2015-03-10 18:52:13');
INSERT INTO `dvbc__schema_version` VALUES ('575', '575_altered_module_perms.sql', 'eda3d24b48a4991349db827ad935ace6', 'altered module perms', '2015-03-10 18:52:13');
INSERT INTO `dvbc__schema_version` VALUES ('576', '576_module_notification_settings.sql', '5e41175f6a0f16d55b19fdde3714d385', 'module notification settings', '2015-03-10 18:52:14');
INSERT INTO `dvbc__schema_version` VALUES ('577', '577_module_perms_updated.sql', 'da42fc7b07c04a29fe71f7b96f4d8338', 'module perms updated', '2015-03-11 02:01:45');
INSERT INTO `dvbc__schema_version` VALUES ('578', '578_employee_queries_issuers.sql', 'd0bf030f830dead4da8aeb85e19eba51', 'employee queries issuers', '2015-03-11 11:57:09');
INSERT INTO `dvbc__schema_version` VALUES ('579', '579_employee_request_menu_reorder.sql', '4c842921af6b1abe9b613500797971a5', 'employee request menu reorder', '2015-03-11 11:57:10');
INSERT INTO `dvbc__schema_version` VALUES ('580', '580_module_perm_update.sql', '31ca1bc8ceb90ef0e099be1394d94956', 'module perm update', '2015-03-11 11:57:10');
INSERT INTO `dvbc__schema_version` VALUES ('581', '581_payroll_comp_temp_modified.sql', '73b55da9d71329e98fcac4ffaec542eb', 'payroll comp temp modified', '2015-03-11 11:58:10');
INSERT INTO `dvbc__schema_version` VALUES ('582', '582_start_date_now_date.sql', 'e6c47aa76b06571adca4db05c1709ad6', 'start date now date', '2015-03-12 18:50:32');
INSERT INTO `dvbc__schema_version` VALUES ('583', '583_required_login_disabled.sql', '5a5d13b3181f186e909646f0f7f17f73', 'required login disabled', '2015-03-12 18:50:32');
INSERT INTO `dvbc__schema_version` VALUES ('584', '584_job_credit_package.sql', '8dfdb47dd181c1fe551205d1c0b38c13', 'job credit package', '2015-03-12 18:50:33');
INSERT INTO `dvbc__schema_version` VALUES ('585', '585_job_credit_perms.sql', 'd6f2745346567992e075f73a9b8036de', 'job credit perms', '2015-03-12 18:50:33');
INSERT INTO `dvbc__schema_version` VALUES ('586', '586_job_credit_package_status.sql', 'fba2579c089f03286dab5980a997bf20', 'job credit package status', '2015-03-13 17:47:16');
INSERT INTO `dvbc__schema_version` VALUES ('587', '587_fixes_on_receipt_tables.sql', '708be451dfd15210bbd771f7c5dee284', 'fixes on receipt tables', '2015-03-13 17:47:17');
INSERT INTO `dvbc__schema_version` VALUES ('588', '588_payroll_period_updated.sql', '5e8447bd2d7e51062b13cca4a516d2d7', 'payroll period updated', '2015-03-13 17:47:17');
INSERT INTO `dvbc__schema_version` VALUES ('589', '589_payment_txn_mod.sql', 'ac34ed903a2c3482e27434fab3c28efb', 'payment txn mod', '2015-03-13 17:47:18');
INSERT INTO `dvbc__schema_version` VALUES ('590', '590_payment_receipt_url_added.sql', 'e8408142ed38a73b41ea7c52bce6fe52', 'payment receipt url added', '2015-03-13 17:47:19');
INSERT INTO `dvbc__schema_version` VALUES ('591', '591_rewards_types.sql', 'a6f1dbcefaca9e96302f6525d9daabc2', 'rewards types', '2015-03-13 17:47:19');
INSERT INTO `dvbc__schema_version` VALUES ('592', '592_rewards_status.sql', '87d74c5c7238cc11a556e167d7b30c54', 'rewards status', '2015-03-13 17:47:20');
INSERT INTO `dvbc__schema_version` VALUES ('593', '593_vat_to_receipt.sql', '79bed28af8bec9b83b0f570ab6818ff2', 'vat to receipt', '2015-03-13 17:47:20');
INSERT INTO `dvbc__schema_version` VALUES ('594', '594_insert_questions.sql', '5cefda49aa916210f14bb94b45f365a9', 'insert questions', '2015-03-13 17:47:20');
INSERT INTO `dvbc__schema_version` VALUES ('595', '595_employee_workflow_approver.sql', '38c109dbcbf66895475c91fd0cf94382', 'employee workflow approver', '2015-03-13 17:48:31');

-- ----------------------------
-- Table structure for `gender`
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gender
-- ----------------------------

-- ----------------------------
-- Table structure for `institution_level`
-- ----------------------------
DROP TABLE IF EXISTS `institution_level`;
CREATE TABLE `institution_level` (
  `institution_level_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`institution_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of institution_level
-- ----------------------------
INSERT INTO `institution_level` VALUES ('1', '1st Level');
INSERT INTO `institution_level` VALUES ('2', '2nd Level');

-- ----------------------------
-- Table structure for `lead_source`
-- ----------------------------
DROP TABLE IF EXISTS `lead_source`;
CREATE TABLE `lead_source` (
  `lead_source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_source` varchar(150) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`lead_source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lead_source
-- ----------------------------

-- ----------------------------
-- Table structure for `marital_status`
-- ----------------------------
DROP TABLE IF EXISTS `marital_status`;
CREATE TABLE `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of marital_status
-- ----------------------------

-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'My Account', 'account', '1', '1', '100');
INSERT INTO `modules` VALUES ('2', 'Agents', 'setup', '1', '1', '200');
INSERT INTO `modules` VALUES ('3', 'Branch Office', 'brach_office', '1', '1', '120');
INSERT INTO `modules` VALUES ('4', 'Representing Countries', 'rep_countries', '1', '1', '400');
INSERT INTO `modules` VALUES ('5', 'Representing Institutions', 'rep_institutions', '1', '1', '500');
INSERT INTO `modules` VALUES ('6', 'Reports', 'reports', '1', '1', '600');
INSERT INTO `modules` VALUES ('7', 'Lead Management', 'lead_mgt', '1', '1', '700');

-- ----------------------------
-- Table structure for `module_perms`
-- ----------------------------
DROP TABLE IF EXISTS `module_perms`;
CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  PRIMARY KEY (`perm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module_perms
-- ----------------------------
INSERT INTO `module_perms` VALUES ('1', '1', 'Dashboard', 'dashboard', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('2', '1', 'Edit Profile', 'edit_profile', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('3', '2', 'View', 'agents', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('4', '2', 'Add Agent', 'add_agent', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('5', '3', 'Add', 'add', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('6', '3', 'View', 'view', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('7', '4', 'Add', 'add', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('8', '4', 'View', 'view', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('9', '5', 'Add', 'add', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('10', '5', 'View', 'view', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('11', '6', 'View Applications', 'view_applications', '1', '1', '99');
INSERT INTO `module_perms` VALUES ('12', '6', 'Branch Office Reports', 'branch_office_report', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('13', '6', 'Agent Report', 'agent_report', '1', '1', '100');

-- ----------------------------
-- Table structure for `partner_intitutions`
-- ----------------------------
DROP TABLE IF EXISTS `partner_intitutions`;
CREATE TABLE `partner_intitutions` (
  `institution_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institution_name` varchar(200) DEFAULT NULL,
  `institution_level_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`institution_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of partner_intitutions
-- ----------------------------
INSERT INTO `partner_intitutions` VALUES ('1', 'Academy of English', '1');
INSERT INTO `partner_intitutions` VALUES ('2', 'Discover English', '1');
INSERT INTO `partner_intitutions` VALUES ('3', 'Academies Australasia Polytechnic (AAPoly)', '1');
INSERT INTO `partner_intitutions` VALUES ('4', 'Federation University Australia @ AAPoly Melbourne', '1');
INSERT INTO `partner_intitutions` VALUES ('5', 'Federation University Australia @ IIBIT Sydney', '1');
INSERT INTO `partner_intitutions` VALUES ('6', 'Federation University Australia @ IIBIT Adelaide', '1');
INSERT INTO `partner_intitutions` VALUES ('7', 'Federation University Australia Main Campus', '1');
INSERT INTO `partner_intitutions` VALUES ('8', 'International Institute of Business and Information Technology (IIBIT)', '1');
INSERT INTO `partner_intitutions` VALUES ('9', 'Careers Australia', '1');
INSERT INTO `partner_intitutions` VALUES ('10', 'Sydney Business and Travel Academy (SBTA)', '1');
INSERT INTO `partner_intitutions` VALUES ('11', 'Bridge Business College (BBC)', '1');
INSERT INTO `partner_intitutions` VALUES ('12', 'Queensland International Business  Academies (QIBA) Brisbane', '1');
INSERT INTO `partner_intitutions` VALUES ('13', 'Queensland International Business  Academies (QIBA) Sydney', '1');
INSERT INTO `partner_intitutions` VALUES ('14', 'Queensland International Business  Academies (QIBA) Melbourne', '1');
INSERT INTO `partner_intitutions` VALUES ('15', 'Sterling College', '1');
INSERT INTO `partner_intitutions` VALUES ('16', 'Perth International College of English (PICE)', '1');
INSERT INTO `partner_intitutions` VALUES ('17', 'Pacific College of Technology (PCT)', '1');
INSERT INTO `partner_intitutions` VALUES ('18', 'TAFE NSW', '1');
INSERT INTO `partner_intitutions` VALUES ('19', 'Australian Pacific College', '1');
INSERT INTO `partner_intitutions` VALUES ('20', 'University of Technology Sydney (UTS)', '2');
INSERT INTO `partner_intitutions` VALUES ('21', 'University of Western Sydney (UWS)', '2');
INSERT INTO `partner_intitutions` VALUES ('22', 'University of New South Wales (UNSW)', '2');
INSERT INTO `partner_intitutions` VALUES ('23', 'University of Wollongong  (UOW)', '2');
INSERT INTO `partner_intitutions` VALUES ('24', 'University of Newcastle', '2');
INSERT INTO `partner_intitutions` VALUES ('25', 'Flinders University', '2');
INSERT INTO `partner_intitutions` VALUES ('26', 'Federation University Australia (fEDuNI)', '2');
INSERT INTO `partner_intitutions` VALUES ('27', 'Curtin University of Technology (Curtin)', '2');
INSERT INTO `partner_intitutions` VALUES ('28', 'University of Western Australia (UWA)', '2');
INSERT INTO `partner_intitutions` VALUES ('29', 'Edith Cowan University (ECU)', '2');
INSERT INTO `partner_intitutions` VALUES ('30', 'Murdoch University ', '2');
INSERT INTO `partner_intitutions` VALUES ('31', 'James Cooks University', '2');
INSERT INTO `partner_intitutions` VALUES ('32', 'Charles Darwin University (CDU)', '2');
INSERT INTO `partner_intitutions` VALUES ('33', 'University of Canberra @ @ AAPoly', '2');
INSERT INTO `partner_intitutions` VALUES ('34', 'Holmes Institute', '1');

-- ----------------------------
-- Table structure for `rep_countries`
-- ----------------------------
DROP TABLE IF EXISTS `rep_countries`;
CREATE TABLE `rep_countries` (
  `rep_country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rep_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rep_countries
-- ----------------------------
INSERT INTO `rep_countries` VALUES ('1', '9', null, '4', '1');
INSERT INTO `rep_countries` VALUES ('2', '226', '2015-10-16 05:50:47', '4', '1');

-- ----------------------------
-- Table structure for `rep_country_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `rep_country_statuses`;
CREATE TABLE `rep_country_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_country_id` int(10) unsigned DEFAULT NULL,
  `status_name` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rep_country_statuses
-- ----------------------------
INSERT INTO `rep_country_statuses` VALUES ('1', '1', 'mgnbxmch', '1');
INSERT INTO `rep_country_statuses` VALUES ('2', '1', 'ffdgfg', '1');
INSERT INTO `rep_country_statuses` VALUES ('3', '1', 'dfdgehjf', '1');
INSERT INTO `rep_country_statuses` VALUES ('4', '1', 'dsdlkgiugs', '1');
INSERT INTO `rep_country_statuses` VALUES ('5', '2', 'New', '1');
INSERT INTO `rep_country_statuses` VALUES ('6', '2', 'Conditional Offer', '1');
INSERT INTO `rep_country_statuses` VALUES ('7', '2', 'Unconditional Offer', '0');
INSERT INTO `rep_country_statuses` VALUES ('8', '2', 'Deposit Paid', '1');
INSERT INTO `rep_country_statuses` VALUES ('9', '2', 'CAS Issued', '1');
INSERT INTO `rep_country_statuses` VALUES ('10', '2', 'Visa Applied', '1');

-- ----------------------------
-- Table structure for `rep_institutions`
-- ----------------------------
DROP TABLE IF EXISTS `rep_institutions`;
CREATE TABLE `rep_institutions` (
  `institution_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_country_id` int(10) unsigned DEFAULT NULL,
  `institution_name` varchar(150) DEFAULT NULL,
  `campus` varchar(150) DEFAULT NULL,
  `intl_contact_person` varchar(150) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`institution_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rep_institutions
-- ----------------------------
INSERT INTO `rep_institutions` VALUES ('1', '2', 'cambridge', 'jhfjhd', 'Conta', 'mdjd.jhdhjs@dh.vjn', '0800999', 'www.sql.com', null, '1', '2015-10-18 07:12:31', '4');
INSERT INTO `rep_institutions` VALUES ('2', '2', 'UK Versity', 'na', 'na', 'na@gmil.com', '08037816587', 'na', '37ebbc604a338db06372b42e5667e4e8.jpg', '1', '2015-10-18 07:32:49', '4');

-- ----------------------------
-- Table structure for `super_admin`
-- ----------------------------
DROP TABLE IF EXISTS `super_admin`;
CREATE TABLE `super_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of super_admin
-- ----------------------------
INSERT INTO `super_admin` VALUES ('1', 'admin@crm.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Super', 'Admin', '2015-10-10 11:08:05', null, '1');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `user_type` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', 'admin@crm.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-05 11:02:26', null, '1', '1', null, null, null);
INSERT INTO `users` VALUES ('5', 'sys@info.net', '96c98f1469295420ce4661bca997c5e97e66f448', '2015-10-06 12:33:06', null, '1', '2', null, null, null);
INSERT INTO `users` VALUES ('6', 'ibe@cdl.net', '76940ab40759f4118a7cf955ec26c1d19d5110c8', '2015-10-16 11:53:39', null, '1', '3', null, null, null);
INSERT INTO `users` VALUES ('7', 'sam.agoro@oaa.com', 'bbc95bd2829ce3714569ca3f9fdae92e9d2a7857', '2015-10-18 04:33:20', null, '1', '3', null, null, null);
INSERT INTO `users` VALUES ('9', 'olu@famous.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-10-21 12:19:55', null, '1', '2', null, null, null);
INSERT INTO `users` VALUES ('10', 'aasim@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-10-23 12:33:37', null, '1', '3', 'Omoloye', 'johin', '07011221122');
INSERT INTO `users` VALUES ('12', 'taiwo@gtmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-10-23 12:41:20', null, '1', '3', 'sade', 'taiwo', '080009998');
INSERT INTO `users` VALUES ('13', 'taiwo@gtmail.com', 'd0f3cf01a77cdf0761ca8fe8fee9e46da2596614', '2015-10-24 04:31:58', null, '1', '3', 'Tohir', 'Omoloye', '0212545455');
INSERT INTO `users` VALUES ('15', 'salami@yahoo.com', '7dfa5910647b9b85e184c274692bbe8dadf084d1', '2015-10-25 12:48:53', null, '0', '3', 'nana', 'fuch', '08000');
INSERT INTO `users` VALUES ('16', 'soliu@afp.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2015-10-25 01:18:30', null, '1', '3', 'soliu', 'inbo', '0800');

-- ----------------------------
-- Table structure for `user_perms`
-- ----------------------------
DROP TABLE IF EXISTS `user_perms`;
CREATE TABLE `user_perms` (
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_perms
-- ----------------------------
INSERT INTO `user_perms` VALUES ('4', '1', '1');
INSERT INTO `user_perms` VALUES ('4', '2', '1');
INSERT INTO `user_perms` VALUES ('4', '3', '2');
INSERT INTO `user_perms` VALUES ('4', '4', '2');
INSERT INTO `user_perms` VALUES ('4', '5', '3');
INSERT INTO `user_perms` VALUES ('4', '6', '3');
INSERT INTO `user_perms` VALUES ('4', '7', '4');
INSERT INTO `user_perms` VALUES ('4', '8', '4');
INSERT INTO `user_perms` VALUES ('4', '9', '5');
INSERT INTO `user_perms` VALUES ('4', '10', '5');
INSERT INTO `user_perms` VALUES ('4', '11', '6');
INSERT INTO `user_perms` VALUES ('4', '12', '6');
INSERT INTO `user_perms` VALUES ('4', '13', '6');
INSERT INTO `user_perms` VALUES ('12', '1', '1');
INSERT INTO `user_perms` VALUES ('12', '2', '1');
INSERT INTO `user_perms` VALUES ('12', '3', '2');
INSERT INTO `user_perms` VALUES ('12', '4', '2');
INSERT INTO `user_perms` VALUES ('12', '6', '3');
INSERT INTO `user_perms` VALUES ('12', '8', '4');
INSERT INTO `user_perms` VALUES ('12', '10', '5');

-- ----------------------------
-- Table structure for `user_types`
-- ----------------------------
DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `id_user_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_types
-- ----------------------------
INSERT INTO `user_types` VALUES ('1', 'Super Admin');
INSERT INTO `user_types` VALUES ('2', 'Branch Manager');
INSERT INTO `user_types` VALUES ('3', 'Agent');
