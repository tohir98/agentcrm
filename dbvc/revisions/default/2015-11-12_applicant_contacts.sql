CREATE TABLE IF NOT EXISTS `applicant_contacts` (
  `applicant_contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_1` varchar(150) DEFAULT NULL,
  `address_2` varchar(150) DEFAULT NULL,
  `suburb` varchar(150) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `postcode_id` int(10) unsigned DEFAULT NULL,
  `telephone` decimal(10,0) DEFAULT NULL,
  `mobile` decimal(10,0) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `applicant_id` int(10) unsigned DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `lead_source_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`applicant_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `applicants` (
  `applicant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `middle_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `gender_id` int(10) unsigned DEFAULT NULL,
  `dateof_birth` datetime DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `agent_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `lead_source_id` int(10) unsigned DEFAULT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `ref_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`applicant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `titles` (
  `title_id` int(25) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of titles
-- ----------------------------
INSERT INTO `titles` VALUES ('1', 'Mr.');
INSERT INTO `titles` VALUES ('2', 'Mrs.');
INSERT INTO `titles` VALUES ('3', 'Miss.');
